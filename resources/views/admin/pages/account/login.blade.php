<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{route("admin.login")}}">
    <meta charset="utf-8" />
    <title>Modabu | Admin Panel</title>
    <meta name="description" content="Login page example" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{{asset("assets/css/pages/login/classic/login-4.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/plugins/global/plugins.bundle.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/plugins/custom/prismjs/prismjs.bundle.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/css/style.bundle.css")}}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{asset('/assets/media/fvcn.svg')}}" />
</head>
<body id="kt_body" style="background-image: url('{{asset("assets/media/bg/bg-10.jpg")}}')"
    class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
                style="background-image: url('{{asset("assets/media/bg/bg-3.jpg")}}');">
                <div class="login-form text-center p-7 position-relative overflow-hidden">
                    <!--begin::Login Header-->
                    <div class="d-flex flex-center mb-15">
                        <a href="#">
                            <img src="{{asset("assets/media/logo2.svg")}}" class="max-h-100px" width="200" alt="Modabu" />
                        </a>
                    </div>
                    <div class="login-signin">
                        <div class="mb-20">
                            <div class="text-muted font-weight-bold">Məlumatları aşağadakı xanalara daxil edin!</div>
                        </div>
                        <form class="form" method="post" action="{{route("admin.login")}}">
                            @csrf
                            <div class="form-group mb-5">
                                <input class="form-control h-auto form-control-solid py-4 px-8" type="text"
                                    placeholder="İstifadəci adı" name="username" autocomplete="off" />
                            </div>
                            <div class="form-group mb-5">
                                <input class="form-control h-auto form-control-solid py-4 px-8" type="password"
                                    placeholder="Şifrə" name="password" />
                            </div>
                            <button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Daxil ol</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset("assets/plugins/global/plugins.bundle.js")}}"></script>
    <script src="{{asset("assets/plugins/custom/prismjs/prismjs.bundle.js")}}"></script>
    <script src="{{asset("assets/js/scripts.bundle.js")}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if ($errors->any())
    <script>
        Swal.fire({
            title: 'Xəta baş verdi!',
            html: '@foreach($errors->all() as $error) <p class="text-danger">{{$error}}</p> @endforeach',
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    </script>
    @endif
    <script src="{{asset("assets/js/pages/custom/login/login-general.js")}}"></script>
</body>
</html>