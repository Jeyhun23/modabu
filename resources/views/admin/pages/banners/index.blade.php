@extends("layouts.admin")

@section("content")
<div class="container">
    <!--begin::Card-->
    <!-- Modal-->
    <div class="modal fade" id="newBanner" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="newBanner" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.banners.store') }}" method="post" id="createBannerForm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal Title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Başlıq:</label>
                                    <div class="col-lg-9">
                                        <input type="title" name="title" class="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Link:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="url" class="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Şəkil:</label>
                                    <div class="col-lg-9">
                                        <input type="file" name="image" class="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Sütun:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="position" class="form-control"
                                            placeholder="Sol => 1, Sağ => 2" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Sıra:</label>
                                    <div class="col-lg-9">
                                        <input type="number" name="rank" class="form-control" placeholder="Tam əd." />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editBanner" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="newBanner" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.banners.update') }}" method="post" id="updateBannerForm">
                    <input type="hidden" name="id" id="bannerId">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal Title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Başlıq:</label>
                                    <div class="col-lg-9">
                                        <input type="title" name="title" class="form-control" id="bannerTitle" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Link:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="url" class="form-control" id="bannerUrl" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Sütun:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="position" class="form-control"
                                            placeholder="Sol => 1, Sağ => 2" id="bannerPosition" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Sıra:</label>
                                    <div class="col-lg-9">
                                        <input type="number" name="rank" class="form-control" placeholder="Tam əd."
                                            id="bannerRank" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Şəkil:</label>
                                    <div class="col-lg-9">
                                        <input type="file" name="image" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    {{$page_title}}
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="javascript;" data-toggle="modal" data-target="#newBanner"
                    class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                            height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path
                                    d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                    fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                    Yeni Banner əlavə et
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable" data-url="/banners">
                <thead>
                    <tr>
                        <th title="Field #Tarix">Tarix</th>
                        <th title="Field #Şəkil">Şəkil</th>
                        <th title="Field #Başlıq">Başlıq</th>
                        <th title="Field #Sütun">Sütun</th>
                        <th title="Field #Sıra">Sıra</th>
                        <th title="Field #Status">Status</th>
                        <th title="Field #Əməliyyatlar">Əməliyyatlar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($banners as $item)
                    <tr>
                        <td>
                            <span>{{ date("d/m/Y", strtotime($item->created_at)) }}</span>
                        </td>
                        <td>
                            <img src="{{ asset('/storage'.$item->image) }}" width="70">
                        </td>
                        <td>
                            <a href="{{ $item->url }}" target="_blank">{{ truncate($item->title, 100) }}</a>
                        </td>
                        <td>
                            <span>{{ ($item->position == 1)?"Sol":"Sağ" }}</span>
                        </td>
                        <td>
                            <span>{{ $item->rank }}</span>
                        </td>
                        <td>
                            <span class="switch switch-sm switch-outline switch-icon switch-success">
                                <label>
                                    <input type="checkbox" @if($item->published) checked="checked"
                                    @endif onchange="toggleStatus($(this))"
                                    data-url="{{route("admin.banners.status", $item->id)}}"/>
                                    <span></span>
                                </label>
                            </span>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editBanner"
                                data-banner="{{ strip_tags(json_encode($item)) }}">Redaktə et</button>
                            <button type="button" class="btn btn-danger"
                                onclick="deleteBanner('{{ route('admin.banners.delete', $item->id) }}')">Sil
                            </button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/actions/banners.js')}}"></script>
<script>
    let bannersUrl = "{{ route('admin.banners') }}";
    async function toggleStatus(element) {
        var url = element.data('url');
        await fetch(url, {
            method: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    };

    function deleteBanner(url) {
        Swal.fire({
            title: "Are you sure?",
            text: "If you save, Service will be deleted!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            reverseButtons: true
        }).then(async function(result) {
            if (result.value) {
                await fetch(url, {
                    method: 'delete',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                }).then(function(response) {
                    if (response.status == 200) {
                        Swal.fire("Deleted","","success")
                        location.reload();
                    } else {
                        Swal.fire("System error","","error")
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire("Cancelled","","error")
            }
        });


    }

    $('#editBanner').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var banner = button.data('banner');
        var modal = $(this);
        modal.find('#bannerId').val(banner.id);
        modal.find('#bannerTitle').val(banner.title);
        modal.find('#bannerUrl').val(banner.url);
        modal.find('#bannerPosition').val(banner.position);
        modal.find('#bannerRank').val(banner.rank);
    });

    $("#createBannerForm").submit(async function(e){
        e.preventDefault();
        let form = $(this);
        let text = "";
        var formData = new FormData(e.target);
        const fileField = document.querySelector('input[type="file"]');
        formData.append('image', fileField.files[0]);
        await fetch(form.attr('action'), {
            method:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'accept': 'application/json'
            },
            body: formData,
        }).then(function(res) {
            if(res.status != 201)
                return res.json();
            else
                Swal.fire({
                    title: 'Əlavə edildi!',
                    html: text,
                    icon: 'success',
                });
                window.location.href = bannersUrl;
        }).then(function(res) {
            if(res.errors){
                return res.errors;
            }
        }).then(function(errors) {
            for(x in errors){
                text += errors[x][0] + '<br>';
            }
            Swal.fire({
                title: 'Something went to wrong!',
                html: text,
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        }).catch(function(e) {
            console.log('Error', e);
        });
    });

    $("#updateBannerForm").submit(async function(e){
        e.preventDefault();
        let form = $(this);
        let text = "";
        var formData = new FormData(form[0]);
        await fetch(form.attr('action'), {
            method:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'accept': 'application/json'
            },
            body: formData,
        }).then(function(res) {
            if(res.status != 200)
                return res.json();
            else
                Swal.fire({
                    title: 'Yadda saxlandı!',
                    html: text,
                    icon: 'success',
                });
                location.reload();
        }).then(function(res) {
            if(res.errors){
                return res.errors;
            }
        }).then(function(errors) {
            for(x in errors){
                text += errors[x][0] + '<br>';
            }
            Swal.fire({
                title: 'Səhv baş verdi!',
                html: text,
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        }).catch(function(e) {
            console.log('Error', e);
        });
    });
</script>
@endsection
