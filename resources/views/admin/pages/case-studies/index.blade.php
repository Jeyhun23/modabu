@extends("layouts.admin")

@section("content")
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    {{$page_title}}
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Dropdown-->
                <!--end::Dropdown-->

                <!--begin::Button-->
                <a href="{{route("admin.case-studies.create")}}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                            height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path
                                    d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                    fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                    Add new
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"
                data-url="{{route('admin.case-studies')}}">
                <thead>
                    <tr>
                        <th title="Field #Date">Date</th>
                        <th title="Field #Title">Title</th>
                        <th title="Field #Location">Location</th>
                        <th title="Field #Court">Court</th>
                        <th title="Field #Status">Status</th>
                        <th title="Field #Options">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($caseStudies as $item)
                    <tr>
                        <td>{{date("d.m.Y", strtotime($item->created_at))}}</td>
                        <td><a href="">{{$item->translation->title}}</a></td>
                        <td>{{$item->translation->location}}</td>
                        <td>{{$item->translation->court}}</td>
                        <td>
                            <div class="form-group row">
                                <div class="col-3">
                                    <span class="switch switch-sm switch-outline switch-icon switch-success">
                                        <label>
                                            <input type="checkbox" @if($item->status) checked="checked"
                                            @endif onchange="toggleStatus($(this))"
                                            data-url="{{route("admin.case-studies.status", ['id' => $item->id])}}"/>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="{{route('admin.case-studies.edit', $item->id)}}" type="button"
                                class="btn btn-primary">Edit</a>
                            <a href="{{route('admin.case-studies.processes', $item->id)}}" type="button"
                                class="btn btn-primary">Processes</a>
                            <button type="button" class="btn btn-danger"
                                onclick="deleteService('{{ route('admin.case-studies.delete', $item->id) }}')">Delete
                            </button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/actions/case-study.js')}}"></script>
<script>
    async function toggleStatus(element) {
            var url = element.data('url');
            await fetch(url, {
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
        };
    function deleteService(url) {
        Swal.fire({
            title: "Are you sure?",
            text: "If you save, Service will be deleted!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            reverseButtons: true
        }).then(async function(result) {
            if (result.value) {
                await fetch(url, {
                    method: 'delete',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                }).then(function(response) {
                    if (response.status == 200) {
                        Swal.fire("Deleted","","success")
                        location.reload();
                    } else {
                        Swal.fire("System error","","error")
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire("Cancelled","","error")
            }
        });
    }
</script>
@endsection