@extends("layouts.admin")

@section("content")
<div class="container">

    <div class="card card-custom">
        <div class="card-header card-header-tabs-line">
            <div class="card-title">
                <h3 class="card-label">Enter new Case Studies` detail</h3>
            </div>
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-tabs-line">
                    @foreach($languages as $language)
                    <li class="nav-item">
                        <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                            href="#kt_tab_pane_{{$language->id}}_2">{{$language->title}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>


        <div class="card-body">
            <form action="{{route('admin.case-studies.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="tab-content">
                    <div class="form-group">
                        <label style="display:block">Image upload</label>
                        <input type="file" name="photo" />
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="ct">Service<span class="text-danger">*</span></label>
                        <select name="service" class="form-control" id="ct">
                            @foreach ($services as $item)
                            <option value="{{ $item->id }}">{{$item->translation->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    @foreach($languages as $language)
                    <input type="hidden" name="casestudy[{{$language->code}}][language_code]" value="{{$language->code}}">
                    <div class="tab-pane fade show @if($loop->first) active @endif" id="kt_tab_pane_{{$language->id}}_2"
                        role="tabpanel">
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <label>Location<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="casestudy[{{$language->code}}][location]"
                                    value="{{old("casestudy.{$language->code}.location")}}" />
                            </div>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <label>Court<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="casestudy[{{$language->code}}][court]"
                                    value="{{old("casestudy.{$language->code}.court")}}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Title<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="casestudy[{{$language->code}}][title]"
                                value="{{old("casestudy.{$language->code}.title")}}" />
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-left col-lg-12 col-sm-12">Summary</label>
                            <div class="col-lg-12 col-md-9 col-sm-12">
                                <textarea name="casestudy[{{$language->code}}][summary]"
                                    id="kt_summernote_{{$language->id}}"
                                    class="summernote">{!! old("casestudy.{$language->code}.summary") !!}</textarea>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-12"></div>
                        <div class="col-lg-9">
                            <button type="submit" class="btn btn-primary mr-2">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/pages/crud/forms/editors/summernote.js')}}"></script>
@endsection