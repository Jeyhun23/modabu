@extends("layouts.admin")

@section("content")
<div class="container">
    <!-- Status Modal -->
    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModal"
        aria-hidden="true" style="z-index:1110;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Çatdırılma statusu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="{{ route('admin.orders.status') }}" id="orderStatusForm" method="POST">
                    @csrf
                    <input type="hidden" name="type" value="" id="orderType">
                    <input type="hidden" name="item" value="" id="itemID">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-lg-5 col-md-9 col-sm-12">
                                <label>Status:</label>
                                <select class="form-control selectpicker" title="Status seç.." name="status"
                                    id="statusSelect">
                                    @foreach ($statuses as $item)
                                    <option value="{{$item->id}}" @if($loop->first) selected="selected"
                                        @endif>{{$item->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">Bağla</button>
                        <button type="submit" class="btn btn-primary font-weight-bold"
                            id="submitStatusFormBtn">Təsdiqlə</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Status Modal -->

    <!-- Modal-->
    <div class="modal fade" id="orderDetail" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="orderDetail" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sifariş məlumatı</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" id="result">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    {{$page_title}}
                </h3>
            </div>
            <div class="card-toolbar">
            </div>
        </div>
        <div class="card-body">
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="row align-items-center">
                            <div class="col-md-3 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Axtarış..." id="searchQuery" />
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>
                            <div class="col-md-3 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-md-block">Sifariş növü</label>
                                    <select class="form-control" id="orderTypeQuery">
                                        <option value="form">Birbaşa</option>
                                        <option value="cart" selected>Səbətdən</option>
                                        <option value="cart-no">Səbətdən (Birbaşa)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->

            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/actions/orders.js?v=2.0.0')}}"></script>
@endsection
