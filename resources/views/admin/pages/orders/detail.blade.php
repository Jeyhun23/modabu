<div class="row">
    <div class="form-group col-md-12 d-flex">
        <div class="col-md-6">
            <p><label class="mr-2">Ad: </label>
                @if ($type == 'order')
                    <span>{{ $order->user ? $order->user->name : '-' }}</span>
                @else
                    <span>{{ $order->first_name }}</span>
                @endif
            </p>
            <p><label class="mr-2">Telefon: </label>
                @if ($type == 'order')
                    <span>{{ $order->user ? $order->user->phone : '-' }}</span>
                @else
                    <span>{{ $order->phone }}</span>
                @endif
            </p>
            <p><label class="mr-2">Email: </label>
                @if ($type == 'order')
                    <span>{{ $order->user ? $order->user->email : '-' }}</span>
                @else
                    <span>{{ $order->email }}</span>
                @endif
            </p>
        </div>
        <div class="col-md-6">
            @if ($type == 'order')
                <p><label class="mr-2">Ad: </label> <span>{{ $order->address['fullname'] }}</span></p>
                <p><label class="mr-2">Telefon: </label> <span>{{ $order->address['phone'] }}</span></p>
                <p><label class="mr-2">Şəhər: </label> <span>{{ $order->address['city'] }}</span></p>
                <p><label class="mr-2">Ünvan: </label> <span>{{ $order->address['address'] }}</span></p>
            @else
                <p><label class="mr-2">Ad: </label> <span>{{ $order->first_name . ' ' . $order->last_name }}</span></p>
                <p><label class="mr-2">Telefon: </label> <span>{{ $order->phone }}</span></p>
                <p><label class="mr-2">Şəhər: </label> <span>{{ $order->city == 1 ? 'Bakı' : $order->city }}</span></p>
                <p><label class="mr-2">Ünvan: </label> <span>{{ $order->address }}</span></p>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group col-md-12">
        <table class="table table-borderless table-vertical-center">
            <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">Məhsul No</th>
                    <th class="">Say</th>
                    <th class="">Qiymət (Ümumi)</th>
                    <th class="">Qiymət (Ödənilən)</th>
                    <th class="">Stok</th>
                    <th class="">Status</th>
                    <th class="">Təfərrüatlar</th>
                </tr>
            </thead>
            <tbody id="ordersS">
                @if ($type == 'order')
                    @foreach ($order->detail as $item)
                        <tr>
                            <td>
                                <p><span>{{ $item->product->id }}</span></p>
                            </td>
                            <td>
                                <p><a href="https://modabu.az/product-detail/{{ $item->product->slug }}"
                                        target="_blank">{{ $item->product->id }}</a>
                                </p>
                            </td>
                            <td>
                                <p><span>{{ $item->quantity }}</span></p>
                            </td>
                            <td>
                                <p><span>{{ $item->price * $item->quantity }}</span></p>
                            </td>
                            <td>
                                <p><span>{{ number_format($item->price * $item->quantity - $item->discount, 2, '.', '') }}</span></p>
                            </td>
                            <td>
                                <span class="switch switch-sm switch-outline switch-icon switch-success">
                                    <label>
                                        <input type="checkbox" @if ($item->stock == 0) checked="checked" @endif
                                            onchange="toggleStatus($(this))" data-url="{{ route('admin.stock') }}"
                                            data-item="{{ $item->id }}" data-type="{{ $type }}" />
                                        <span></span>
                                    </label>
                                </span>
                            </td>
                            <td>
                                <button class="btn btn-warning mr-2" data-toggle="modal" data-target="#statusModal"
                                    id="changeStatus" data-item="{{ $item->id }}" data-type="{{ $type }}"
                                    data-sid="{{ $item->parentStatus->id }}"
                                    data-title="{{ $item->parentStatus->title }}">
                                    Statusu dəyiş
                                </button>
                            </td>
                            <td>
                                <p>
                                    <span>Rəng: {{ $item->attributes['color'] }}, </span>
                                    <span>Ölçü: {{ $item->attributes['size'] }}</span>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <p><span>{{ $order->product->id }}</span></p>
                        </td>
                        <td>
                            <p><a href="https://modabu.az/product-detail/{{ $order->product->slug }}"
                                    target="_blank">{{ $order->product->id }}</a>
                            </p>
                        </td>
                        <td>
                            <p><span>{{ $order->quantity }}</span></p>
                        </td>
                        <td>
                            <p><span>{{ $order->payed }}</span></p>
                        </td>
                        <td>
                            <span class="switch switch-sm switch-outline switch-icon switch-success">
                                <label>
                                    <input type="checkbox" @if ($order->stock == 0) checked="checked" @endif
                                        onchange="toggleStatus($(this))" data-url="{{ route('admin.stock') }}"
                                        data-item="{{ $order->id }}" data-type="{{ $type }}" />
                                    <span></span>
                                </label>
                            </span>
                        </td>
                        <td>
                            <button class="btn btn-warning mr-2" data-toggle="modal" data-target="#statusModal"
                                id="changeStatus" data-item="{{ $order->id }}" data-type="{{ $type }}"
                                data-sid="{{ $order->parentStatus->id }}"
                                data-title="{{ $order->parentStatus->title }}">
                                Statusu dəyiş
                            </button>
                        </td>
                        <td>
                            <p>
                                <span>Rəng: {{ $order->attributes['color'] }}, </span>
                                <span>Ölçü: {{ $order->attributes['size'] }}</span>
                            </p>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
