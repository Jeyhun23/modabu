@extends("layouts.admin")

@section("content")
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    {{$page_title}}
                </h3>
            </div>

            <div class="card-toolbar">
                <!--begin::Button-->
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!-- Modal-->
            <div class="modal fade" id="pdfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <form action="{{route('admin.blog-categories.store')}}" method="post" class="modal-content"
                        id="createBlogCategory">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Contact Messages</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="form-group">
                                            <label for="">Message: </label>
                                            <p id="message">

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"
                data-url="{{route('admin.blogs')}}">
                <thead>
                    <tr>
                        <th title="Field #Date">Date</th>
                        <th title="Field #Name">Name</th>
                        <th title="Field #Phone">Phone</th>
                        <th title="Field #Case">Case</th>
                        <th title="Field #Status">Status</th>
                        <th title="Field #Options">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($messages as $item)
                    <tr>
                        <td>{{ date("d.m.Y", strtotime($item->created_at)) }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->case }}</td>
                        <td>
                            <div class="form-group row">
                                <div class="col-3">
                                    <span class="switch switch-sm switch-outline switch-icon switch-success">
                                        <label>
                                            <input type="checkbox" @if($item->status) checked="checked" @endif
                                            disabled/>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="javascript;" data-message="{{ strip_tags($item->message) }}" type="button"
                                class="btn btn-primary" data-toggle="modal" data-target="#pdfModal" onclick="viewMessage(this)">View</a>
                            <button type="button" class="btn btn-danger"
                                onclick="deleteMessage('{{ route('admin.contact.delete', $item->id) }}')">Delete
                            </button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/actions/contact.js')}}"></script>
<script>
    $(document).ready(function () {
            $('#pdfModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var message = button.data('message');
                var modal = $(this);
                var pr = modal.find('#message');
                pr.html(message);
            })
        });
    async function toggleStatus(element) {
            var url = element.data('url');
            await fetch(url, {
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
        };
    function deleteMessage(url) {
        Swal.fire({
            title: "Are you sure?",
            text: "If you save, Service will be deleted!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            reverseButtons: true
        }).then(async function(result) {
            if (result.value) {
                await fetch(url, {
                    method: 'delete',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                }).then(function(response) {
                    if (response.status == 200) {
                        Swal.fire("Deleted","","success")
                        location.reload();
                    } else {
                        Swal.fire("System error","","error")
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire("Cancelled","","error")
            }
        });
    }
</script>
@endsection