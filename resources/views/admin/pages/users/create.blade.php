@extends("layouts.admin")

@section("content")
<style>
    .image-input .image-input-wrapper {
        width: 114px;
        height: 114px;
    }
</style>
<div class="container col-md-9">

    <div class="card card-custom">
        <div class="card-header card-header-tabs-line">
            <div class="card-title">
                <h3 class="card-label">Write Informations</h3>
            </div>
        </div>


        <div class="card-body">
            <form action="{{route('admin.products.store')}}" method="post" enctype="multipart/form-data"
                id="productStoreForm">
                @csrf
                <div class="tab-content">


                    <div class="form-group">
                        <label>Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Title.." name="title"
                            value="{{ old('title') }}" />
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Category<span class="text-danger">*</span></label>
                            <select name="category" id="" class="form-control selectpicker" data-live-search="true">
                                @foreach ($categories as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @if ($item->children_count)
                                @foreach ($item->children as $ct)
                                <option value="{{$ct->id}}">&emsp; - {{$ct->name}}</option>
                                @endforeach
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Brand<span class="text-danger">*</span></label>
                            <select name="brand" id="" class="form-control selectpicker" data-live-search="true">
                                @foreach ($brands as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Color<span class="text-danger">*</span></label>
                            <select class="form-control selectpicker" multiple="multiple" tabindex="null"
                                name="colors[]" data-live-search="true" id="colors" onchange="update_sku();">
                                @foreach ($colors as $item)
                                <option value="{{$item->id}}"
                                    data-content="<span style='display: inline-block;width: min-content;'><p style='display:flex;align-items: center;'><i style='border:1px solid black;width:10px;margin-right: 7px;height:10px;padding:5px;background-color:{{ $item->code }}'></i>{{ $item->name }}</p></span>"
                                    data-name="{{ $item->name }}">
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    {{-- IMAGE --}}
                    <hr>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label style="display:block">Upload Thumbnail</label>
                            <div class="image-input image-input-outline" id="kt_image_1">
                                <div class="image-input-wrapper"
                                    style="background-image: url(assets/media/users/100_1.jpg)"></div>

                                <label
                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                    data-action="change" data-toggle="tooltip" title="Add thumbnail image">
                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                    <input type="file" name="thumbnail" accept=".png, .jpg, .jpeg" />
                                </label>

                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                    data-action="cancel" data-toggle="tooltip" title="Cancel">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="images" value="" id="images">
                        <div class="col-lg-6">
                            <label>Upload images</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 p-0">
                                <div class="dropzone dropzone-default" id="kt_dropzone_1">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">
                                            Drop files here or click to upload.
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    {{-- ATTRIBUTES --}}
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label style="display:block">Attributes</label>
                            <select class="form-control select2" id="kt_select2_9" name="attributes[]"
                                multiple="multiple">
                                @foreach ($attributes as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6" id="attrs">
                            <label style="display:block">Attribute choices</label>

                        </div>
                    </div>
                    <hr>
                    {{-- PRICE --}}
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Price<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="100" name="price" id="price"
                                value="{{ old('price') }}" />
                        </div>
                        <div class="col-lg-6" id="quantity">
                            <label>Quantity<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="1" name="qty" value="1" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Discount<span class="text-info">(Optional)</span></label>
                            <input type="text" class="form-control" placeholder="10" name="discount"
                                value="{{ old('discount') }}" />
                        </div>
                        <div class="col-lg-6">
                            <label>Discount type<span class="text-info">(Optional)</span></label>
                            <select class="form-control selectpicker" tabindex="null" name="discount_type">
                                <option value="1">Amount</option>
                                <option value="2">Percent</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="sku_combination" id="sku_combination">

                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-form-label text-left col-lg-12 col-sm-12">Summary</label>
                        <div class="col-lg-12 col-md-9 col-sm-12">
                            <textarea name="description" id="kt_summernote_1"
                                class="summernote">{!! old('description') !!}</textarea>
                        </div>
                    </div>

                    <hr>

                    <div class="panel-heading bord-btm">
                        <h3 class="panel-title">SEO Meta Tags</h3>
                    </div>

                    <div class="form-group row mt-10">
                        <label class="col-lg-3 control-label">Meta Title</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="meta[title]" value=""
                                placeholder="Meta Title">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label">Description</label>
                        <div class="col-lg-9">
                            <textarea name="meta[description]" rows="8" class="form-control"> </textarea>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-12"></div>
                        <div class="col-lg-9">
                            <button type="submit" class="btn btn-primary mr-2">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    let fileUrl = "{{ route('admin.file.upload', 'product') }}";
    let productsUrl = "{{ route('admin.products') }}";
</script>
<script src="{{asset('/assets/js/pages/crud/forms/editors/summernote.js')}}"></script>
<script src="{{asset('/assets/js/pages/crud/forms/widgets/select2.js?v=7.1.5')}}"></script>

<script src="{{ asset('/assets/js/actions/dropzonejs.js') }}"></script>
<script src="{{ asset('/assets/js/actions/image-input.js') }}"></script>
<script>
    function randomString(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
   
    document.getElementById("colors").onchange = update_sku();

    function delete_row(em){
		$(em).closest('.form-group').remove();
		update_sku();
    }
    
    function update_sku(){
        $("#sku_combination").html('');
        let text = "", price = $("#price").val(), firstVariations, secondVariations, kke, kkel;
		$.each($("#colors option:selected"), function(i, color){
            let attributes = $("#kt_select2_9 option:selected");
            $.each(attributes, function(k, attribute) {
                if(attributes.length == 1){
                    let variations =  $('input[name="variations['+ $(attribute).val() +']"]');
                    $.each(variations, function(k, variation) {
                        if($(variation).val() != ''){
                            let items = JSON.parse($(variation).val());
                            if(items.length){
                                kke = 0;
                                items.forEach(element =>  {
                                    text = text.concat('\n'
                                        +'<tr>'
                                        +    '<td><label for="" class="control-label">'+$(color).data('name') + '' + '-' + element.value+'</label></td>' 
                                        +    '<td><input type="number" name="prices['+$(color).val()+']['+ $(attribute).val() +']['+ kke +']" value="'+ price +'" min="0" step="0.01" class="form-control"></td>' 
                                        +    '<td><input type="text" readonly value="'+$(color).val() + 'xxxx' + kke +'" class="form-control" required=""></td>' 
                                        +    '<td><input type="number" name="qtys['+$(color).val()+']['+ $(attribute).val() +']['+ kke +']" value="1" min="0" step="1" class="form-control" required=""></td>' 
                                        +'</tr>');
                                    kke++;
                                });
                            }
                        }
                    });
                }else{
                    $.each(attributes, function(kk, aa) {
                        //console.log(kk);
                        if(kk == 0){
                            firstVariations =  $('input[name="variations['+ $(aa).val() +']"]');
                        } else{
                            secondVariations =  $('input[name="variations['+ $(aa).val() +']"]');
                        }
                    });
                    $.each(firstVariations, function(k, firstVariation) {
                        if($(firstVariation).val() != ''){
                            let firstItems = JSON.parse($(firstVariation).val());
                            if(firstItems.length){
                                kkel = 0;
                                firstItems.forEach(firstElement =>  {
                                    $.each(secondVariations, function(k, secondVariation) {
                                        console.log($(secondVariation).val());
                                        if($(secondVariation).val() != ''){
                                            let secondItems = JSON.parse($(secondVariation).val());
                                            if(secondItems.length){
                                                kke = 0;
                                                secondItems.forEach(secondElement =>  {
                                                    text = text.concat('\n'
                                                        +'<tr>'
                                                        +    '<td><label for="" class="control-label">'+$(color).data('name') + '-'+ firstElement.value + '-' + secondElement.value+'</label></td>' 
                                                        +    '<td><input type="number" name="prices['+$(color).val()+']['+ $(attribute).val() +']['+ kkel +']['+ kke +']" value="'+ price +'" min="0" step="0.01" class="form-control"></td>' 
                                                        +    '<td><input type="text" readonly name="skus['+$(color).val()+']['+ $(attribute).val() +']['+ kkel +']['+ kke +']" value="'+$(color).val() + 'xx' + kkel + 'xx' + kke +'" class="form-control" required=""></td>' 
                                                        +    '<td><input type="number" name="qtys['+$(color).val()+']['+ $(attribute).val() +']['+ kkel +']['+ kke +']" value="1" min="0" step="1" class="form-control" required=""></td>' 
                                                        +'</tr>');
                                                    kke++;
                                                });
                                            }
                                        }else{
                                            text = text.concat('\n'
                                                        +'<tr>'
                                                        +    '<td><label for="" class="control-label">'+$(color).data('name') + '-'+ firstElement.value + '-' + '</label></td>' 
                                                        +    '<td><input type="number" name="prices['+$(color).val()+']['+ kkel +']ice_df" value="'+ price +'" min="0" step="0.01" class="form-control"></td>' 
                                                        +    '<td><input type="text" readonly name="skus['+$(color).val()+']['+ kkel +']" value="'+$(color).val() + 'xx' + kkel + 'xx' + '" class="form-control" required=""></td>' 
                                                        +    '<td><input type="number" name="qtys['+$(color).val()+']['+ kkel +']" value="1" min="0" step="1" class="form-control" required=""></td>' 
                                                        +'</tr>');
                                        }
                                    });
                                    kkel++;
                                });
                            }
                        }
                    });
                    return false;
                }
            });
        });
        let table ='<table class="table table-bordered" id="variationTable">' +
                    '<thead>' +
                        '<tr>' +
                            '<td class="text-center"><label for="" class="control-label">Variant</label></td>' +
                            '<td class="text-center"><label for="" class="control-label">Variant Price</label></td>' +
                            '<td class="text-center"><label for="" class="control-label">SKU</label></td>' +
                            '<td class="text-center"><label for="" class="control-label">Quantity</label></td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody id="resultV">' + text + '</tbody>' +
                '</table>';
        $("#sku_combination").html(table);
    }

    function add_more_customer_choice_option(i, name){
        $('#attrs').append(
            '<div class="col-lg-12 col-md-9 col-sm-12 mb-5">'+
                '<input type="hidden" name="attribute_choice[]" value="'+i+'">'+
                '<input id="kt_tagify_'+i+'" class="form-control tagify" name="variations['+i+']" placeholder="type..." autofocus="">'+
            '</div>'
            );
        var input = document.getElementById("kt_tagify_" + i);
        taggify = new Tagify(input);
        taggify.on('add', update_sku).on('remove', update_sku);
    }
    
    $('#kt_select2_9').on('change', function() {
        let selects = [];
		$.each($("#kt_select2_9 option:selected"), function(j, attribute){
            flag = false;
			$('input[name="attribute_choice[]"]').each(function(i, choice_no) {
				if($(attribute).val() == $(choice_no).val()){
					flag = true;
				}
			});
            if(!flag){
				add_more_customer_choice_option($(attribute).val(), $(attribute).text());
            }
            selects.push($(attribute).val());
        });
        $('input[name="attribute_choice[]"]').each(function(i, choice_no) {
			if(selects.includes($(choice_no).val())){
				flag = true;
            }else{
                $(choice_no).parent().remove();
            }
		});
		update_sku();
	});


var serializeForm = function (form) {
	var obj = {};
	var formData = new FormData(form);
	for (var key of formData.keys()) {
		obj[key] = formData.get(key);
        console.log(formData.get(key));
	}
	return obj;
};
    
$("#productStoreForm").submit(async function(e){
    e.preventDefault();
	let form = $(this);
    let text = "";
	var formData = new FormData(e.target);
    const fileField = document.querySelector('input[type="file"]');
    formData.append('thumbnail', fileField.files[0]);
	await fetch(form.attr('action'), {
		method:'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'accept':'application/json',
        },
		body: formData,
	}).then(function(res) {
        if(res.status != 201)
		    return res.json();
        else
            Swal.fire({
                title: 'Successfully added!',
                html: text,
                icon: 'success',
                confirmButtonText: 'Ok'
            });
            window.location.href = productsUrl;
	}).then(function(res) {
        if(res.errors){
            return res.errors;
        }
	}).then(function(errors) {
        for(x in errors){
            text += errors[x][0] + '<br>';
        }
        Swal.fire({
            title: 'Something went to wrong!',
            html: text,
            icon: 'error',
            confirmButtonText: 'Ok'
        });
	}).catch(function(e) {
		console.log('Error', e);
	});
});
</script>
@endsection