@extends("layouts.admin")

@section("content")
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    {{$page_title}}
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="?page=case-editor" class="btn btn-primary font-weight-bolder" data-toggle="modal"
                    data-target="#pdfModal">
                    <span class="svg-icon svg-icon-md">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                            height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path
                                    d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                    fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                    </span>
                    Add New
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!-- Modal-->
            <div class="modal fade" id="pdfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="{{route('admin.blog-categories.store')}}" method="post" class="modal-content"
                        id="createBlogCategory">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add category name</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-custom">
                                <div class="card-header card-header-tabs-line">

                                    <div class="card-toolbar">
                                        <ul class="nav nav-tabs nav-bold nav-tabs-line">
                                            @foreach ($languages as $item)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                                                    href="#kt_tab_pane_1_{{$item->id}}">{{ $item->title }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        @foreach ($languages as $item)
                                        <div class="tab-pane fade show @if($loop->first) active @endif"
                                            id="kt_tab_pane_1_{{$item->id}}" role="tabpanel">
                                            <div class="form-group">
                                                <label>Category Name</label>
                                                <input type="text" class="form-control" name="title[{{$item->code}}]"
                                                    placeholder="Add category name" />
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="pdfModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="{{route('admin.blog-categories.update')}}" method="post" class="modal-content"
                        id="updateBlogCategory">
                        @csrf
                        <input type="hidden" name="blog_category_id" value="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add category name</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-custom">
                                <div class="card-header card-header-tabs-line">

                                    <div class="card-toolbar">
                                        <ul class="nav nav-tabs nav-bold nav-tabs-line">
                                            @foreach ($languages as $item)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                                                    href="#kt_tab_pane_1_{{$item->id}}">{{ $item->title }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        @foreach ($languages as $item)
                                        <div class="tab-pane fade show @if($loop->first) active @endif"
                                            id="kt_tab_pane_1_{{$item->id}}" role="tabpanel">
                                            <div class="form-group">
                                                <label>Category Name</label>
                                                <input type="text" class="form-control" name="title[{{$item->code}}]"
                                                    placeholder="Add category name" />
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal /// -->

            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"
                data-url="{{route('admin.blogs')}}">
                <thead>
                    <tr>
                        <th title="Field #1">Date</th>
                        <th title="Field #2">Status</th>
                        <th title="Field #3">Title</th>
                        <th title="Field #5">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $item)
                    <tr>
                        <td>{{date("d.m.Y", strtotime($item->created_at))}}</td>
                        <td>
                            <div class="form-group row">
                                <div class="col-3">
                                    <span class="switch switch-sm switch-outline switch-icon switch-success">
                                        <label>
                                            <input type="checkbox" @if($item->status) checked="checked"
                                            @endif onchange="toggleStatus($(this))"
                                            data-url="{{route("admin.blog-categories.status", $item->id)}}"/>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td><a href="">{{$item->translation->title}}</a></td>

                        <td>
                            <a href="{{route('admin.blogs.edit', $item->id)}}" type="button"
                                class="btn btn-primary">Edit</a>
                            <button type="button" class="btn btn-danger"
                                onclick="deleteCategory('{{ route('admin.blog-categories.delete', $item->id) }}')">Delete
                            </button>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
@endsection

@section("scripts")
<script src="{{asset('/assets/js/pages/crud/ktdatatable/base/html-table.js')}}"></script>
<script>
    async function toggleStatus(element) {
            var url = element.data('url');
            await fetch(url, {
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
        };
    function deleteCategory(url) {
        Swal.fire({
            title: "Are you sure?",
            text: "If you save, Service will be deleted!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            reverseButtons: true
        }).then(async function(result) {
            if (result.value) {
                await fetch(url, {
                    method: 'delete',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                }).then(function(response) {
                    if (response.status == 200) {
                        Swal.fire("Deleted","","success")
                        location.reload();
                    } else {
                        Swal.fire("System error","","error")
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire("Cancelled","","error")
            }
        });
    }

    $("#createBlogCategory").submit(async function(e){
        e.preventDefault();
        let form = $(this);
        await fetch(form.attr('action'), {
            method: form.attr('method'),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            body: new URLSearchParams(form.serialize()),
        }).then(function(response) {
            if (response.status == 200) {
                Swal.fire("Added","","success")
                location.reload();
            }
            return response.json();
        }).then(function(errors){
            errors = errors.errors;
            if(errors){
                let txt = "";
                Object.entries(errors).forEach(([key, value]) => {
                    txt += value + '<br>';
                });
                Swal.fire(txt,"","error");
            }
        });
        return false;
    });
</script>
@endsection