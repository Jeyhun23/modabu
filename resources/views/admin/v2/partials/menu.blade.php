<header class="header-top bor-bottom-white-20 b-black-blur">
    <div class="container">
        <div class="row">
            <div class="xl-1">
                <a href="">
                    <img src="{{asset('img/united skills.png')}}" class="logo" alt="logo">
                </a>
            </div>
            <div class="xl-9">
                <ul class="header-top-ul">
                    @php
                        $role_user=auth()->user()->roles[0]->id;
                    @endphp
                    @if (in_array($role_user, [1,4]))
                        <li>
                            <a href="{{route('orders')}}" class="c-white f-size-14">
                                Sifarişlər
                            </a>
                        </li>
                    @elseif(in_array($role_user, [3]))
                        <li>
                            <a href="{{route('orders')}}?order_status=3" class="c-white f-size-14">
                                Sifarişlər
                            </a>
                        </li>
                    @endif
                    @if (in_array($role_user, [1]))
                        <li>
                            <a href="{{route('users.index')}}" class="c-white f-size-14">
                                İstifadəçilər
                            </a>
                        </li>
                    @endif

                    @if (in_array($role_user, [1,5,4]))
                        <li>
                            <a href="{{route('products.index')}}" class="c-white f-size-14">
                                Məhsullar
                            </a>
                        </li>
                    @endif
                    @if (in_array($role_user, [1,4]))
                        <li>
                            <a href="{{route('refler.index')}}" class="c-white f-size-14">
                                Rəflər
                            </a>
                        </li>
                    @endif
                    @if (in_array($role_user, [1,4,5]))
                        <li class="dropdownk c-white f-size-14">
                            <a class="dropbtnk">Redaktor</a>
                            <div class="dropdown-contentk">
                                <a href="{{route('brands.index')}}" class="c-white f-size-14">
                                    Brendlər
                                </a>
                                <a href="{{route('colors.index')}}" class="c-white f-size-14">
                                    Rənglər
                                </a>
                                @if (in_array(auth()->user()->roles[0]->id, [1,5]))
                                    <a href="{{route('banners.index')}}" class="c-white f-size-14">
                                        Bannerlər
                                    </a>
                                    <a href="{{route('stories.index')}}" class="c-white f-size-14">
                                        Storilər
                                    </a>
                                @endif
                            </div>
                        </li>
                    @endif
                    @if (in_array($role_user, [1]))
                        <li>
                            <a href="{{route('mektublar.index')}}" class="c-white f-size-14">
                                Məktublar
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_panel_users.index')}}" class="c-white f-size-14">
                                Rollar
                            </a>
                        </li>
                        <li>
                            <a href="{{route('coupons.index')}}" class="c-white f-size-14">
                                Kuponlar
                            </a>
                        </li>
                        <li>
                            <a href="{{route('category.index')}}" class="c-white f-size-14">
                                Menu
                            </a>
                        </li>
                        <li class="dropdownk c-white f-size-14">
                            <a class="dropbtnk">SMS</a>
                            <div class="dropdown-contentk">
                                <a href="{{route('smstemplates.index')}}" class="c-white f-size-14">
                                    Şablonlar
                                </a>
                                <a href="{{route('send.sms.admin')}}" class="c-white f-size-14">
                                    SMS Göndər
                                </a>
                            </div>
                        </li>
                        <li>
                            <a href="{{route('emailtemplates.index')}}" class="c-white f-size-14">
                                Email Şablonları
                            </a>
                        </li>
                        <li>
                            <a href="{{route('statistics.index')}}" class="c-white f-size-14">
                                Statistika
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="xl-2">
                <div class="header-account">
                    <div class="dropdownk">
                        <a class="header-top-edit bor-left-white-20 dropbtnk">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M6.34027 0.533447L6.24853 0.999268L5.89761 2.70376C5.33654 2.91802 4.82773 3.22185 4.368 3.58215L2.6528 3.00864L2.19201 2.8658L1.95308 3.27726L0.772265 5.25142L0.533333 5.66294L0.883203 5.96886L2.21014 7.0977C2.16214 7.39298 2.10028 7.68503 2.10028 7.99523C2.10028 8.30543 2.16214 8.59755 2.21014 8.89282L0.883203 10.0217L0.533333 10.3276L0.772265 10.739L1.95308 12.7133L2.19201 13.1258L2.6528 12.9819L4.368 12.4084C4.82773 12.7687 5.33654 13.0724 5.89761 13.2867L6.24853 14.9912L6.34027 15.4571H9.65867L9.75146 14.9912L10.1013 13.2867C10.6624 13.0724 11.1712 12.7687 11.6309 12.4084L13.3461 12.9819L13.8069 13.1258L14.0469 12.7133L15.2267 10.739L15.4667 10.3276L15.1157 10.0217L13.7888 8.89282C13.8379 8.59755 13.8987 8.30543 13.8987 7.99523C13.8987 7.68503 13.8379 7.39298 13.7888 7.0977L15.1157 5.96886L15.4667 5.66294L15.2267 5.25142L14.0469 3.27726L13.8069 2.8658L13.3461 3.00864L11.6309 3.58215C11.1712 3.22185 10.6624 2.91802 10.1013 2.70376L9.75146 0.999268L9.65867 0.533447H6.34027Z"
                                      stroke="white" stroke-linecap="square" stroke-linejoin="round"/>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M10.1329 7.99523C10.1329 9.17206 9.17721 10.1272 7.99961 10.1272C6.82201 10.1272 5.86628 9.17206 5.86628 7.99523C5.86628 6.8184 6.82201 5.86333 7.99961 5.86333C9.17721 5.86333 10.1329 6.8184 10.1329 7.99523Z"
                                      stroke="white" stroke-linecap="square" stroke-linejoin="round"/>
                            </svg>
                        </a>
                        <div class="dropdown-contentk">
                            <form action="{{route('v2.admin.logout')}}" method="post">
                                @csrf
                                <button type="submit" class="header-top-account f-size-14">
                                    Logout
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
