<div class="top-section b-black">
    <div class="container">
        <div class="top-section-inner">
            <div>
                <div class="page-links">
                    <a href="{{route('dashboard')}}" class="c-white-op-75 f-size-14">Ana səhifə</a>
                    <span class="c-white-op-50 f-size-14">
                                İstifadəçilər
                            </span>
                </div>
                <h6 class="f-size-24 c-white">
                    İstifadəçilər
                </h6>
            </div>
            @stack('breadcrump_button')
        </div>

    </div>
</div>
