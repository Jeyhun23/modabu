<footer>
    <div class="container">
        <div class="footer-content">
            <span class="f-size-14 c-black-op-75">All rights reserved. © {{date('Y')}}</span>
            <div class="footer-logo">
                        <span class="f-size-14 c-black-op-75">
                            Developed by
                        </span>
                <a href="">
                    <img src="{{asset('img/united skillsb.png')}}" class="logo" alt="logo">
                </a>
            </div>
        </div>
    </div>
</footer>
