<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
{{--<title>UnitedSkills - Panel</title>--}}
<title>@yield('title', config('admin.title', 'UnitedSkills')) @yield('title_postfix', config('admin.title_postfix', 'Panel'))</title>

<!-- css plugins -->
@includeIf('admin.v2.partials.plugins',['type' => 'css'])

<link rel="stylesheet" href="{{asset('css/general.css?v=2.0')}}">
<!-- font links -->
<link rel="stylesheet" href="{{asset('css/font/stylesheet.css')}}">

@stack('styles')

