@extends('layouts.v2.admin')
@section('body_class','add-product')

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a href="{{route('dashboard')}}" class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <a href="{{route('smstemplates.index')}}" class="c-white-op-75 f-size-14">Send SMS</a>
                        <span class="c-white-op-50 f-size-14">
                                SMS Gonder
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        SMS Gonder
                    </h6>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('js/dropzone-5.7.0/dist/min/dropzone.min.css')}}">
    <style>
        .d-none {
            display: none;
        }
    </style>
@endpush

@section('content')
    <div class="add-product-page b-white-1">
        <div class="container">
            <form action="{{route('send.sms.admin.post')}}" method="post">
                @csrf
                <div class="row">
                    <div class="xl-1">
                    </div>
                    <div class="xl-9">
                        <div class="new-address-content bradius-8 b-white">
                            <div class="new-address-content-header bor-bottom-black-1">
                                <span class="f-size-16">
                                    Haqqında
                                </span>
                            </div>
                            @if($errors->any())
                                <div style="background: red;color: white;font-size: 12px;border-radius: 4%"
                                     id="color_create_error">
                                    <ul style="margin-left: 5px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="new-address-content-body">
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Phone
                                        </label>
                                        <input type="text" class="input-class" name="phone"
                                               value="{!! old('phone') !!}">
                                    </div>
{{--                                    <div class="news-address-content-inputs">--}}
{{--                                        <label class="checkbox-parent ">--}}
{{--                                            <input type="checkbox" class="checkbox-table" name="sendAll">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                        Send All Clients--}}
{{--                                    </div>--}}
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Message
                                        </label>
                                        <textarea name="message" id="" cols="30" required
                                                  rows="10">{!! old('message') !!}</textarea>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <button type="submit" class="new-address-button">
                                    Yadda saxla
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).ready(function () {
            function setCouponType() {
                let selectedType = $("#coupon_type").val();
                $(".coupon-type").addClass('d-none');
                $(`#${selectedType}`).parent('div').removeClass('d-none');
            }

            setCouponType();

            $("#coupon_type").change(function () {
                setCouponType();
            });


        });
    </script>
@endpush
