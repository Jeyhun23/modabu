@extends('layouts.v2.admin')


@section('content')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a href="" class="c-white-op-75 f-size-14">Ana səhifə</a>
                        <span class="c-white-op-50 f-size-14">
                                İstifadəçilər
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        İstifadəçilər
                    </h6>
                </div>
                <button class="button-blue f-size-14 c-white bradius-4  b-blue">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8 1.06665V14.9333M1.06667 7.99998H14.9333" stroke="white" />
                    </svg>

                    Yenisini əlavə et
                </button>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="popup-button-section">
            <button onclick="approval()">
                approval
            </button>
            <button onclick="save()">
                save
            </button>
            <button onclick="successSave()">
                success save
            </button>
            <button onclick="deletepop()">
                delete pop
            </button>
            <button onclick="deleted()">
                deleted
            </button>
        </div>
    </div>
    <div class="homepage-content b-white-1">
        <div class="container">
            <ul class="tabs-header b-white">
                <li>
                    <a href="" class="f-size-14 c-black-op-50 active">
                        Hamısı
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Qeydiyyatlı
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Sosial ilə qeydiyyat
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Qeydiyyatsız
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Qeydiyyatsız
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Qeydiyyatsız
                    </a>
                </li>
                <li>
                    <a href="" class="f-size-14 c-black-op-50">
                        Qeydiyyatsız
                    </a>
                </li>
            </ul>
        </div>
        <form class="search-bar">
            <div class="container">
                <div class="row">
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Müştəri adı
                        </label>
                        <input type="text" class="input-class" placeholder="Ad, ID...">
                    </div>
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Sifariş nömrəsi
                        </label>
                        <input type="text" class="input-class">
                    </div>
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Paket
                        </label>
                        <input type="text" class="input-class">
                    </div>
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Barkod
                        </label>
                        <input type="text" class="input-class">
                    </div>
                    <div class="xl-4">
                        <div class="search-bars-right">
                            <div class="date-parent">
                                <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                                    Tarix aralığı
                                </label>
                                <input type="text" class="input-class date-class" name="daterange">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <g opacity="0.5">
                                        <path
                                            d="M3.73333 0V5.33333M12.2667 0V5.33333M3.19999 8H6.39999M12.8 8H9.59999M3.19999 11.2H6.39999M9.59999 11.2H12.8M1.59999 2.66667H14.4C14.9891 2.66667 15.4667 3.14423 15.4667 3.73333V14.4C15.4667 14.9891 14.9891 15.4667 14.4 15.4667H1.59999C1.01089 15.4667 0.533325 14.9891 0.533325 14.4V3.73333C0.533325 3.14423 1.01089 2.66667 1.59999 2.66667Z"
                                            stroke="#0B0B18" />
                                    </g>
                                </svg>
                            </div>
                            <div class="search-buttons">
                                <button type="button" class="search-delete">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.5">
                                            <path d="M1.59998 1.59998L14.4 14.4M1.59998 14.4L14.4 1.59998"
                                                  stroke="#0B0B18" />
                                        </g>
                                    </svg>
                                </button>
                                <button type="button" class="search-button b-black bradius-4">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M15.4667 15.4667L11.2 11.2M6.93332 13.3333C3.3987 13.3333 0.533325 10.4679 0.533325 6.93332C0.533325 3.3987 3.3987 0.533325 6.93332 0.533325C10.4679 0.533325 13.3333 3.3987 13.3333 6.93332C13.3333 10.4679 10.4679 13.3333 6.93332 13.3333Z"
                                            stroke="white" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="search-result-parent">
            <div class="container">
                <div class="search-result">
                    <div class="search-result-header">
                            <span class="f-size-14 search-esult-span c-dblack-75">
                                8555 nəticə
                            </span>
                        <button class="delete-search-result">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path
                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                        stroke="#05061E" />
                                </g>
                            </svg>
                            <span class="f-size-14">
                                    Sil
                                </span>
                        </button>
                    </div>
                    <div class="search-result-body">
                        <div class="table-checkbox">
                            <table class="search-result-table">
                                <tr>
                                    <th>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox" >
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox" >
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox" >
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox-parent ">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div class="table-content-overflow">
                            <table class="search-result-table-overflow search-result-table">
                                <tr>
                                    <th>Column 1 search-result-table-overflowsearch-result-table-overflow</th>
                                    <th>Column 2</th>
                                    <th>.search-result-table th{
                                        padding: 2.4rem 2rem;
                                        } </th>
                                    <th>.search-result-table th{
                                        padding: 2.4rem 2rem;
                                        } </th>
                                    <th>.search-result-table th{
                                        padding: 2.4rem 2rem;
                                        } </th>
                                    <th>Column 6</th>
                                    <th>Column 7 </th>
                                    <th>Column 8</th>
                                    <th>Column 9</th>
                                </tr>

                                <tr>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                </tr>
                                <tr>

                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                </tr>
                                <tr>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                </tr>
                                <tr>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                </tr>
                                <tr>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                    <td>Item 1</td>
                                </tr>

                            </table>
                        </div>
                        <div class="table-edit">
                            <table class="search-result-table">
                                <tr>
                                    <th>
                                        Column 10
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                        fill="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                        stroke="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                        fill="#0B0B18" />
                                                </g>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                        fill="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                        stroke="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                        fill="#0B0B18" />
                                                </g>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                        fill="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                        stroke="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                        fill="#0B0B18" />
                                                </g>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                        fill="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                        stroke="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                        fill="#0B0B18" />
                                                </g>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                        fill="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                        stroke="#05061E" />
                                                </g>
                                            </svg>
                                        </button>
                                        <button class="table-buttons">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                    <path
                                                        d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                        fill="#0B0B18" />
                                                </g>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-options">
                <div class="page-options-content">
                        <span class="page-op-span f-size-14 c-black-op-75">
                            Hər səhifədə nəticə sayı
                        </span>
                    <div class="dropdown">
                        <div class="dropdown-header">
                                 <span class="f-size-14 c-black-op-50">
                                    5
                                 </span>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g opacity="0.5">
                                    <path d="M4.79999 6.93335L7.99999 10.1333L11.2 6.93335" stroke="#0B0B18" stroke-linecap="square"/>
                                </g>
                            </svg>
                        </div>
                        <ul class="dropdown-body">
                            <li>
                                <a href="">
                                    1
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    2
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    3
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    4
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    5
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="pagination">
                        <span class="f-size-14 c-black-op-75">
                            1-12 of 100
                        </span>
                    <div class="pagination-nav">
                        <a href="" class="disabled">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g >
                                    <path d="M11.3333 6L7.33333 10L11.3333 14" stroke="#0B0B18" stroke-linecap="square"/>
                                </g>
                            </svg>
                        </a>
                        <a href="">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.66666 14L12.6667 10L8.66667 6" stroke="#0B0B18" stroke-linecap="square"/>
                            </svg>

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
