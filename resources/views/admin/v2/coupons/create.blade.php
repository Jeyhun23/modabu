@extends('layouts.v2.admin')
@section('body_class','add-product')

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <a href="{{route('coupons.index')}}" class="c-white-op-75 f-size-14">&emsp14;Kuponlar</a>
                        <span class="c-white-op-50 f-size-14">
                                Yenisini əlavə et
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        Yenisini əlavə et
                    </h6>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('js/dropzone-5.7.0/dist/min/dropzone.min.css')}}">
    <style>
        .d-none {
            display: none;
        }
    </style>
@endpush

@section('content')
    <div class="add-product-page b-white-1">
        <div class="container">
            <form action="{{route('coupons.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="xl-1">
                    </div>
                    <div class="xl-9">
                        <div class="new-address-content bradius-8 b-white">
                            <div class="new-address-content-header bor-bottom-black-1">
                                <span class="f-size-16">
                                    Haqqında
                                </span>
                            </div>
                            @if($errors->any())
                                <div style="background: red;color: white;font-size: 12px;border-radius: 4%"
                                     id="color_create_error">
                                    <ul style="margin-left: 5px;">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="new-address-content-body">
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Title
                                        </label>
                                        <input type="text" class="input-class" name="title">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Code
                                        </label>
                                        <input type="text" class="input-class" name="code">
                                    </div>
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Discount
                                        </label>
                                        <input type="text" class="input-class" name="discount">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Discount Type
                                        </label>
                                        <label for="b">
                                            <div class="selectHolder selectThin">
                                                <select name="discount_type">
                                                    <option value
                                                            disabled {{ old('discount_type', null) === null ? 'selected' : '' }}>
                                                        Select Discount Type
                                                    </option>
                                                    @foreach(App\Models\Coupon::DISCOUNT_TYPE_SELECT as $key => $label)
                                                        <option
                                                            value="{{ $key }}" {{ old('discount_type', 'flat') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs selectHolder selectThin">
                                        <label class="required">Coupon Type</label>
                                        <select name="coupon_type" id="coupon_type" required>
                                            <option value
                                                    disabled {{ old('coupon_type', null) === null ? 'selected' : '' }}>
                                                Select Coupon Type
                                            </option>
                                            @foreach(App\Models\Coupon::COUPON_TYPE_SELECT as $key => $label)
                                                <option
                                                    value="{{ $key }}" {{ old('coupon_type', 'count') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="news-address-content-inputs d-none coupon-type">
                                        <label>Count</label>
                                        <input class="input-class" type="number" min="1" step="1" name="count"
                                               id="count" value="{{ old('count', '') }}">
                                    </div>

                                    <div class="news-address-content-inputs d-none coupon-type">
                                        <label>Expire Date</label>
                                        <input class="input-class " type="date" name="expire_date" id="expire_date"
                                               value="{{ old('expire_date') }}"
                                               min="{{now()->addDay()->format("Y-m-d")}}">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <button type="submit" class="new-address-button">
                                    Yadda saxla
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).ready(function () {
            function setCouponType() {
                let selectedType = $("#coupon_type").val();
                $(".coupon-type").addClass('d-none');
                $(`#${selectedType}`).parent('div').removeClass('d-none');
            }

            setCouponType();

            $("#coupon_type").change(function () {
                setCouponType();
            });


        });
    </script>
@endpush
