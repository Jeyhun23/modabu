<div class="row">
    <div class="xl-3">
        <div class="analytic-all-card">
            <h5>
                İstifadəçilər
            </h5>
            <p>
                {{$users}}
            </p>
        </div>
    </div>
    <div class="xl-3">
        <div class="analytic-all-card">
            <h5>
                Sifarişlər
            </h5>
            <p>
                {{$orders}}
            </p>
        </div>
    </div>
    <div class="xl-3">
        <div class="analytic-all-card">
            <h5>
                Məhsullar
            </h5>
            <p>
                {{$products}}
            </p>
        </div>
    </div>
    <div class="xl-3">
        <div class="analytic-all-card">
            <h5>
                Brendlər
            </h5>
            <p>
                {{$brands}}
            </p>
        </div>
    </div>
</div>