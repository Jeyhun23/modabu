@extends('layouts.v2.admin')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/theme.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    @-webkit-keyframes spin {
        to {
        stroke-dashoffset: -264;
        }
    }

    @keyframes spin {
        to {
        stroke-dashoffset: -264;
        }
    }

    .spinner circle {
        fill: none;
        stroke: slategray;
        stroke-width: 16;
        stroke-linecap: round;
        stroke-dasharray: 0, 0, 70, 194;
        stroke-dashoffset: 0;
        animation: spin 1s infinite linear;
        -webkit-animation: spin 1s infinite linear;
    }
    .radio-parent {
    display: block;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }

    .radio-parent input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    }

    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 1.6rem;
    width: 1.6rem;
    background-color: #eee;
    border-radius: 50%;
    border: 2px solid #eee;
    }


    .radio-parent input:checked ~ .checkmark {
    border: 2px solid #0B0B18;
    background: #fff;
    }

    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }

    .radio-parent input:checked ~ .checkmark:after {
    display: block;
    }

    .radio-parent .checkmark:after {
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        margin: auto;
        width: 1.2rem;
        height:1.2rem;
        border-radius: 50%;
        background: #0B0B18;
    }
</style>
@endpush
@section('breadcrump')
<div class="top-section b-black">
    <div class="container">
        <div class="top-section-inner">
            <div>
                <div class="page-links">
                    <a class="c-white-op-75 f-size-14">Ana səhifə</a>
                    <span class="c-white-op-50 f-size-14">
                        Statistika
                    </span>
                </div>
                <h6 class="f-size-24 c-white">
                    Statistika
                </h6>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="homepage-content b-white-1">
    <div class="container">
        <div class="analytic-header">
            <h4>Ümumi</h4>
            <div class="analytic-header-right">
                <ul id="tabs">
                    <li>
                        <a href="#general" class="inactive" onclick="sendRequest('general', '{{$today}}')">Bugün</a>
                    </li>
                    <li>
                        <a href="#general" class="inactive" onclick="sendRequest('general', '{{$thisweek}}')">Bu həftə</a>
                    </li>
                    <li>
                        <a href="#general" class="inactive" onclick="sendRequest('general', '{{$thismonth}}')">Bu ay</a>
                    </li>
                </ul>
                {{-- <form>
                    <label for="datepciker-1">
                        <input type="text" id="datepciker-1">
                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.83301 0V8.33333M19.1663 0V8.33333M4.99967 12.5H9.99967M19.9997 12.5H14.9997M4.99967 17.5H9.99967M14.9997 17.5H19.9997M2.49967 4.16667H22.4997C23.4201 4.16667 24.1663 4.91286 24.1663 5.83333V22.5C24.1663 23.4205 23.4201 24.1667 22.4997 24.1667H2.49968C1.5792 24.1667 0.833008 23.4205 0.833008 22.5V5.83333C0.833008 4.91286 1.5792 4.16667 2.49967 4.16667Z" stroke="white"/>
                        </svg>                                
                    </label>
                </form> --}}
            </div>
        </div>
        <div id="general">
            <div class="row">
                <div class="xl-3">
                    <div class="analytic-all-card">
                        <h5>
                            İstifadəçilər
                        </h5>
                        <p>
                            <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                            </svg>
                        </p>
                    </div>
                </div>
                <div class="xl-3">
                    <div class="analytic-all-card">
                        <h5>
                            Sifarişlər
                        </h5>
                        <p>
                            <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                            </svg>
                        </p>
                    </div>
                </div>
                <div class="xl-3">
                    <div class="analytic-all-card">
                        <h5>
                            Məhsullar
                        </h5>
                        <p>
                            <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                            </svg>
                        </p>
                    </div>
                </div>
                <div class="xl-3">
                    <div class="analytic-all-card">
                        <h5>
                            Brendlər
                        </h5>
                        <p>
                            <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                            </svg>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="analytic-header">
            <h4>Satış</h4>
        </div>
        <div class="tab-category">
            <div class="row">
                <div class="xl-6">
                    <div class="analytic-card">
                        <div class="analytic-card-header">
                            <h6>Kateqoriyalar</h6>
                            <div class="category-date">
                                <ul id="tabs-2">
                                    <li>
                                        <a href="#order_category" class="inactive" onclick="sendRequest('order_category', '{{$today}}')">Bugün</a>
                                    </li>
                                    <li>
                                        <a href="#order_category" class="inactive" onclick="sendRequest('order_category', '{{$thisweek}}')">Bu həftə</a>
                                    </li>
                                    <li>
                                        <a href="#order_category" class="inactive" onclick="sendRequest('order_category', '{{$thismonth}}')">Bu ay</a>
                                    </li>
                                </ul>
                                {{-- <form>
                                    <label for="datepciker-2">
                                        <input type="text" id="datepciker-2">
                                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g opacity="0.75">
                                            <rect width="25" height="25" fill="white" fill-opacity="0.5"/>
                                            <path d="M5.83301 0V8.33333M19.1663 0V8.33333M4.99967 12.5H9.99967M19.9997 12.5H14.9997M4.99967 17.5H9.99967M14.9997 17.5H19.9997M2.49967 4.16667H22.4997C23.4201 4.16667 24.1663 4.91286 24.1663 5.83333V22.5C24.1663 23.4205 23.4201 24.1667 22.4997 24.1667H2.49968C1.5792 24.1667 0.833008 23.4205 0.833008 22.5V5.83333C0.833008 4.91286 1.5792 4.16667 2.49967 4.16667Z" stroke="black" stroke-opacity="0.5"/>
                                            </g>
                                        </svg>                                                                                 
                                    </label>
                                </form> --}}
                            </div>
                        </div>   
                        <div class="analytic-card-body"  id="order_category">
                            <center>
                                <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                    <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                                </svg>
                            </center>
                        </div>  
                    </div>
                </div>
                <div class="xl-6">
                    <div class="analytic-card">
                        <div class="analytic-card-header">
                            <h6>Brendlər</h6>
                            <div class="category-date">
                                <ul id="tabs-3">
                                    <li>
                                        <a href="#order_brand" class="inactive" onclick="sendRequest('order_brand', '{{$today}}')">Bugün</a>
                                    </li>
                                    <li>
                                        <a href="#order_brand" class="inactive" onclick="sendRequest('order_brand', '{{$thisweek}}')">Bu həftə</a>
                                    </li>
                                    <li>
                                        <a href="#order_brand" class="inactive" onclick="sendRequest('order_brand', '{{$thismonth}}')">Bu ay</a>
                                    </li>
                                </ul>
                                {{-- <form>
                                    <label for="datepciker-3">
                                        <input type="text" id="datepciker-3">
                                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g opacity="0.75">
                                            <rect width="25" height="25" fill="white" fill-opacity="0.5"/>
                                            <path d="M5.83301 0V8.33333M19.1663 0V8.33333M4.99967 12.5H9.99967M19.9997 12.5H14.9997M4.99967 17.5H9.99967M14.9997 17.5H19.9997M2.49967 4.16667H22.4997C23.4201 4.16667 24.1663 4.91286 24.1663 5.83333V22.5C24.1663 23.4205 23.4201 24.1667 22.4997 24.1667H2.49968C1.5792 24.1667 0.833008 23.4205 0.833008 22.5V5.83333C0.833008 4.91286 1.5792 4.16667 2.49967 4.16667Z" stroke="black" stroke-opacity="0.5"/>
                                            </g>
                                        </svg>                                                                                 
                                    </label>
                                </form> --}}
                            </div>
                        </div>    
                        <div>
                            <div class="analytic-card-body"  id="order_brand">
                                <center>
                                    <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                        <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                                    </svg>
                                </center>
                            </div>                   
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        <div class="analytic-header">
            <h4>Stok</h4>
        </div>
        <div class="tab-category">
            <div class="row">
                <div class="xl-6">
                    <div class="analytic-card">
                        <div class="analytic-card-header">
                            <h6>Kateqoriyalar</h6>
                        </div>       
                        <div class="analytic-card-body" id="stock_category">
                            <center>
                                <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                    <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                                </svg>
                            </center>
                        </div>                    
                    </div>
                </div>
                <div class="xl-6">
                    <div class="analytic-card">
                        <div class="analytic-card-header">
                            <h6>Brendlər</h6>
                        </div>       
                        <div class="analytic-card-body" id="stock_brand">
                            <center>
                                <svg class="spinner" viewBox="0 0 100 100" width="20" height="20">
                                    <circle cx="50" cy="50" r="42" transform="rotate(-90,50,50)" />
                                </svg>
                            </center>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
window.onload = (event) => {
    sendRequest("general","");
    sendRequest("order_category","");
    sendRequest("order_brand","");
    sendRequest("stock_category","");
    sendRequest("stock_brand","");
};
function sendRequest(type, date) {
    var request = $.get('/v2/admin/statistics/'+type+'?create_date='+date);
    request.done(function(response) {
        $('#'+type).empty().append(response);
	});
}
</script>
@endpush
