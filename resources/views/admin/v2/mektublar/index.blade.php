@extends('layouts.v2.admin')

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <span class="c-white-op-50 f-size-14">
                                Məktublar
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        Məktublar
                    </h6>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('content')
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    <div class="homepage-content b-white-1">
        <form class="search-bar" action="{{request()->fullUrl()}}" method="get">
            <div class="container">
                <div class="row">
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Başlıq
                        </label>
                        <input type="text" name="name" class="input-class" value="{{request('name')}}">
                    </div>
                    <div class="xl-4">
                        <div class="search-bars-right">
                            <div class="search-buttons">
                                <a href="{{request()->url()}}" class="search-delete">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.5">
                                            <path d="M1.59998 1.59998L14.4 14.4M1.59998 14.4L14.4 1.59998"
                                                  stroke="#0B0B18"/>
                                        </g>
                                    </svg>
                                </a>
                                <button type="submit" class="search-button b-black bradius-4">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M15.4667 15.4667L11.2 11.2M6.93332 13.3333C3.3987 13.3333 0.533325 10.4679 0.533325 6.93332C0.533325 3.3987 3.3987 0.533325 6.93332 0.533325C10.4679 0.533325 13.3333 3.3987 13.3333 6.93332C13.3333 10.4679 10.4679 13.3333 6.93332 13.3333Z"
                                            stroke="white"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="search-result-parent">
            <div class="container">
                <div class="search-result">
                    <div class="search-result-header">
                            <span class="f-size-14 search-esult-span c-dblack-75">
                                {{$mektublar->total()}} nəticə
                            </span>
                        <button class="delete-search-result" style="color: black;background: lightgreen"
                                onclick="deletepop40('{!! route('mektub.deleteSelections') !!}','mektub')">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path
                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                        stroke="#05061E"/>
                                </g>
                            </svg>
                            <span class="f-size-14">
                                    Sil
                                </span>
                        </button>
                    </div>
                    <div class="search-result-body">
                        <div class="table-content-overflow" style="width: 100%">
                            <table class="search-result-table-overflow search-result-table text-center"
                                   style="width: 100%">
                                <tr>
                                    <th class="search-result-table-th">
                                        <label class="checkbox-parent">
                                            <input type="checkbox" class="checkbox-table sd_all"
                                                   onclick="sdAll('mektub','{!! $mektublar->pluck('id')->implode(',') !!}','.sd_all','.cs1')">
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Movzu</th>
                                    <th>Telefon</th>
                                    <th>Tarix</th>
                                    <th style="width: 100px;text-align: center;border:1px solid lightgrey">Tənzimləmələr
                                    </th>
                                </tr>
                                @forelse($mektublar as $item)
                                    <tr>
                                        <td class="search-result-table-td">
                                            <label class="checkbox-parent">
                                                <input type="checkbox" class="checkbox-table cs1 ds{!! $item->id !!}"
                                                       onclick="checkInput('mektub','{!! $item->id !!}','.ds')">
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->topic}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td style="border: 1px solid lightgrey">
                                            <div class="table-edit">
                                                <table class="search-result-table">
                                                    <div class="row" style="width: 140px;">
                                                        <div class="col-3">
                                                            <button onclick="mektublar_show({{$item}})"
                                                                    class="table-buttons">
                                                                <svg class="show-eye" width="16" height="16"
                                                                     viewBox="0 0 16 16" fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <g opacity="0.5">
                                                                        <path
                                                                            d="M1 8L0.535761 7.81431C0.48808 7.93351 0.48808 8.06649 0.535761 8.18569L1 8ZM15 8L15.4642 8.1857C15.5119 8.06649 15.5119 7.93351 15.4642 7.8143L15 8ZM7.99998 12.5C5.68597 12.5 4.06111 11.3483 2.99664 10.1655C2.46405 9.57375 2.07811 8.98029 1.82563 8.53474C1.69968 8.31247 1.60772 8.12838 1.54797 8.00164C1.51811 7.9383 1.49635 7.8894 1.48246 7.85735C1.47552 7.84133 1.47055 7.82953 1.46752 7.82225C1.466 7.81861 1.46497 7.8161 1.46443 7.81477C1.46416 7.8141 1.46401 7.81372 1.46397 7.81364C1.46396 7.8136 1.46397 7.81364 1.46402 7.81375C1.46404 7.81381 1.46409 7.81394 1.4641 7.81397C1.46417 7.81413 1.46424 7.81431 1 8C0.535761 8.18569 0.535847 8.18591 0.535941 8.18614C0.535982 8.18625 0.536084 8.1865 0.536167 8.18671C0.536334 8.18712 0.536531 8.18761 0.53676 8.18818C0.537217 8.18931 0.5378 8.19075 0.538508 8.19248C0.539923 8.19595 0.54184 8.20062 0.544263 8.20644C0.549108 8.21808 0.555977 8.23436 0.564903 8.25495C0.582752 8.29614 0.608844 8.35467 0.643439 8.42805C0.712592 8.57474 0.815944 8.78128 0.955611 9.02776C1.23438 9.51971 1.66093 10.1762 2.25334 10.8345C3.43886 12.1517 5.314 13.5 7.99998 13.5V12.5ZM1 8C1.46424 8.18569 1.46417 8.18587 1.4641 8.18603C1.46409 8.18606 1.46404 8.18619 1.46402 8.18625C1.46397 8.18636 1.46396 8.1864 1.46397 8.18636C1.46401 8.18628 1.46416 8.1859 1.46443 8.18523C1.46497 8.1839 1.466 8.18139 1.46752 8.17775C1.47055 8.17047 1.47552 8.15867 1.48246 8.14265C1.49635 8.1106 1.51811 8.0617 1.54797 7.99836C1.60772 7.87162 1.69968 7.68753 1.82563 7.46526C2.07811 7.01971 2.46405 6.42625 2.99664 5.83448C4.06111 4.65173 5.68597 3.5 7.99998 3.5V2.5C5.314 2.5 3.43886 3.84827 2.25334 5.16552C1.66093 5.82375 1.23438 6.48029 0.955611 6.97224C0.815944 7.21872 0.712592 7.42526 0.643439 7.57195C0.608844 7.64533 0.582752 7.70386 0.564903 7.74505C0.555977 7.76564 0.549108 7.78192 0.544263 7.79356C0.54184 7.79938 0.539923 7.80405 0.538508 7.80752C0.5378 7.80925 0.537217 7.81069 0.53676 7.81182C0.536531 7.81239 0.536334 7.81288 0.536167 7.81329C0.536084 7.8135 0.535982 7.81375 0.535941 7.81386C0.535847 7.81409 0.535761 7.81431 1 8ZM7.99998 3.5C10.314 3.5 11.9389 4.65173 13.0033 5.83448C13.5359 6.42625 13.9219 7.01971 14.1744 7.46526C14.3003 7.68754 14.3923 7.87162 14.452 7.99837C14.4819 8.0617 14.5037 8.1106 14.5175 8.14265C14.5245 8.15868 14.5295 8.17048 14.5325 8.17775C14.534 8.18139 14.535 8.1839 14.5356 8.18524C14.5358 8.1859 14.536 8.18628 14.536 8.18636C14.536 8.1864 14.536 8.18636 14.536 8.18625C14.536 8.1862 14.5359 8.18606 14.5359 8.18603C14.5358 8.18587 14.5358 8.1857 15 8C15.4642 7.8143 15.4642 7.81409 15.4641 7.81385C15.464 7.81375 15.4639 7.8135 15.4638 7.81329C15.4637 7.81288 15.4635 7.81239 15.4632 7.81182C15.4628 7.81069 15.4622 7.80925 15.4615 7.80752C15.4601 7.80405 15.4582 7.79938 15.4557 7.79356C15.4509 7.78192 15.444 7.76564 15.4351 7.74504C15.4172 7.70385 15.3912 7.64533 15.3566 7.57195C15.2874 7.42526 15.1841 7.21871 15.0444 6.97224C14.7656 6.48029 14.3391 5.82375 13.7466 5.16552C12.5611 3.84827 10.686 2.5 7.99998 2.5V3.5ZM15 8C14.5358 7.8143 14.5358 7.81413 14.5359 7.81397C14.5359 7.81394 14.536 7.8138 14.536 7.81375C14.536 7.81364 14.536 7.8136 14.536 7.81364C14.536 7.81372 14.5358 7.8141 14.5356 7.81476C14.535 7.8161 14.534 7.81861 14.5325 7.82225C14.5295 7.82952 14.5245 7.84132 14.5175 7.85735C14.5037 7.8894 14.4819 7.9383 14.452 8.00163C14.3923 8.12838 14.3003 8.31246 14.1744 8.53474C13.9219 8.98029 13.5359 9.57375 13.0033 10.1655C11.9389 11.3483 10.314 12.5 7.99998 12.5V13.5C10.686 13.5 12.5611 12.1517 13.7466 10.8345C14.3391 10.1762 14.7656 9.51971 15.0444 9.02776C15.1841 8.78129 15.2874 8.57474 15.3566 8.42805C15.3912 8.35467 15.4172 8.29615 15.4351 8.25496C15.444 8.23436 15.4509 8.21808 15.4557 8.20644C15.4582 8.20062 15.4601 8.19595 15.4615 8.19248C15.4622 8.19075 15.4628 8.18931 15.4632 8.18818C15.4635 8.18761 15.4637 8.18712 15.4638 8.18671C15.4639 8.1865 15.464 8.18625 15.4641 8.18615C15.4642 8.18591 15.4642 8.1857 15 8ZM8 9.5C7.17157 9.5 6.5 8.82843 6.5 8H5.5C5.5 9.38071 6.61929 10.5 8 10.5V9.5ZM9.5 8C9.5 8.82843 8.82843 9.5 8 9.5V10.5C9.38071 10.5 10.5 9.38071 10.5 8H9.5ZM8 6.5C8.82843 6.5 9.5 7.17157 9.5 8H10.5C10.5 6.61929 9.38071 5.5 8 5.5V6.5ZM8 5.5C6.61929 5.5 5.5 6.61929 5.5 8H6.5C6.5 7.17157 7.17157 6.5 8 6.5V5.5Z"
                                                                            fill="#0B0B18"/>
                                                                    </g>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="col-3">
                                                            <button
                                                                onclick="deletepop('{{route('mektublar.destroy',['mektublar'=> $item->id])}}')"
                                                                class="table-buttons">
                                                                <svg width="16" height="16" viewBox="0 0 16 16"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <g opacity="0.5">
                                                                        <path
                                                                            d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                                            stroke="#05061E"/>
                                                                    </g>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-options">
                <div class="page-options-content">
                        <span class="page-op-span f-size-14 c-black-op-75">
                            Hər səhifədə nəticə sayı
                        </span>
                    <div class="dropdown">
                        <div class="dropdown-header">
                                 <span class="f-size-14 c-black-op-50">
                                    {{$mektublar->perPage()}}
                                 </span>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g opacity="0.5">
                                    <path d="M4.79999 6.93335L7.99999 10.1333L11.2 6.93335" stroke="#0B0B18"
                                          stroke-linecap="square"/>
                                </g>
                            </svg>
                        </div>
                        <ul class="dropdown-body">
                            @foreach(config('admin.limits') as $limit)
                                <li>
                                    <a href="{{request()->fullUrlWithQuery(['limit' => $limit])}}">
                                        {{$limit}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                {{$mektublar->appends(request()->except('page'))->links('admin.v2.partials.simple-pagination')}}
            </div>
        </div>
    </div>

    <div class="popup popup-12 mektublar_show">
        <div class="layer-popup"></div>
        <div class="popup-container">
            <div class="popup-content">
                <div class="popup-header bor-bottom-black-1 mb-12">
                    <h6>
                        Mektub detallari
                    </h6>
                </div>
                <div class="news-address-content-inputs mb1" style="font-size: 12px">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Name : <span id="msname"></span>
                    </label>

                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Surname : <span id="mssurname"></span>
                    </label>

                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Email : <span id="msemail"></span>
                    </label>

                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Movzu : <span id="mstopic"></span>
                    </label>
                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Phone : <span id="msphone"></span>
                    </label>
                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Tarix : <span id="mstarix"></span>
                    </label>
                </div>
                <div class="news-address-content-inputs mb1">
                    <label for="" class="label-class f-size-14-b c-dblack-op-75 mb-12">
                        Message : <span id="msmessage"></span>
                    </label>
                </div>
                <div class="pop-buttons px-0">
                    <button class="popup-close pop-button pop-default-button" type="button">
                        Bağla
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
