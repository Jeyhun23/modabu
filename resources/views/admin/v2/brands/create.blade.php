@extends('layouts.v2.admin')
@section('body_class','add-product')

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <a href="{{route('brands.index')}}" class="c-white-op-75 f-size-14">&emsp14;Brendlər</a>
                        <span class="c-white-op-50 f-size-14">
                                Yenisini əlavə et
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        Yenisini əlavə et
                    </h6>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('js/dropzone-5.7.0/dist/min/dropzone.min.css')}}">
@endpush

@section('content')
    <div class="add-product-page b-white-1">
        <div class="container">
            <form action="{{route('brands.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="xl-3">
                        <div class="new-address bradius-8 b-white">
                            <div class="new-address-header bor-bottom-black-1">
                                <h4 class="f-size-20">
                                    Yenisini əlavə et
                                </h4>
                                <button>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.59998 1.60001L14.4 14.4M1.59998 14.4L14.4 1.60001" stroke="#0B0B18" stroke-width="2"></path>
                                    </svg>
                                </button>
                            </div>
                            <ul class="new-address-body">
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75 active">
                                        Haqqında
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        Sifariş
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        Teqlər
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        SEO
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="xl-9">
                        <div class="new-address-content bradius-8 b-white">
                            <div class="new-address-content-header bor-bottom-black-1">
                                <span class="f-size-16">
                                    Haqqında
                                </span>
                            </div>
                            <div class="new-address-content-body">
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Ad
                                        </label>
                                        <input type="text" class="input-class" name="name">
                                    </div>
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Logo
                                        </label>
                                        <input type="file" class="input-class" name="logo">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Status
                                        </label>
                                        <label for="b">
                                            <div class="private f-size-14-b c-dblack-75">
                                                Aktivdir
                                                <div class="checkboxes">
                                                    <div class="check">
                                                        <input type="checkbox" name="published" checked="" id="b">
                                                        <label for="b"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
{{--                                <div class="new-address-file">--}}
{{--                                    <form action="/file-upload"--}}
{{--                                          class="dropzone"--}}
{{--                                          id="my-awesome-dropzone">--}}
{{--                                        <div class="dz-default dz-message">--}}
{{--                                            <img src="{{asset('img/folder.png')}}" alt="">--}}
{{--                                            <h5>--}}
{{--                                                <span> Fayllardan seç</span> və ya sürüşdürüb bura at--}}
{{--                                            </h5>--}}
{{--                                            <p>Maksimum 5 MB, JPEG, PNG, PDF</p>--}}

{{--                                        </div>--}}
{{--                                    </form>--}}
{{--                                </div>--}}
                                <button type="submit" class="new-address-button">
                                    Yadda saxla
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/dropzone-5.7.0/dist/dropzone.js')}}"></script>
@endpush
