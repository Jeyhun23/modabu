@extends('layouts.v2.admin')

@section('body_class','add-product')

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <a href="{{route('users.index')}}" class="c-white-op-75 f-size-14">&emsp14;İstifadəçilər</a>
                        <span class="c-white-op-50 f-size-14">
                                Redaktə et
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        Redaktə et
                    </h6>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="add-product-page b-white-1">
        <div class="container">
            @if($errors->any())
                <div style="background: red;color: white;font-size: 12px;border-radius: 4%" id="roles_create_error">
                    <ul style="margin-left: 5px;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @isset($danger)
                <h3 style="background: red;color:white">{{$danger}}</h3>
            @endisset
            <form action="{{route('users.update',['user' => $user->id])}}" method="post">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="xl-3">
                        <div class="new-address bradius-8 b-white">
                            <div class="new-address-header bor-bottom-black-1">
                                <h4 class="f-size-20">
                                    Yenisini əlavə et
                                </h4>
                                <button>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.59998 1.60001L14.4 14.4M1.59998 14.4L14.4 1.60001" stroke="#0B0B18"
                                              stroke-width="2"></path>
                                    </svg>
                                </button>
                            </div>
                            <ul class="new-address-body">
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75 active">
                                        Haqqında
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        Sifariş
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        Teqlər
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="f-size-14-b c-dblack-75">
                                        SEO
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="xl-9">
                        <div class="new-address-content bradius-8 b-white">
                            <div class="new-address-content-header bor-bottom-black-1">
                                <span class="f-size-16">
                                    Haqqında
                                </span>
                            </div>
                            @php($nameAndSurname = explode(' ', $user->name))
                            <div class="new-address-content-body">
                                <div class="new-address-content-input-groups">
                                    <input type="hidden" name="uid" value="{{$user->id}}">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Ad
                                        </label>
                                        <input type="text" class="input-class" name="name"
                                               value="{{$nameAndSurname[0] ?? ''}}">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Soyad
                                        </label>
                                        <input type="text" class="input-class" name="surname"
                                               value="{{$nameAndSurname[1] ?? ''}}">
                                    </div>
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Email (
                                            <bold>{{$user->email}}</bold>
                                            )
                                        </label>
                                        <input type="email" name="email" class="input-class" value="{{$user->email}}">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Mobil nömrə (
                                            <bold>{{$user->phone}}</bold>
                                            )
                                        </label>
                                        <div class="mobile-select">
                                            <div class="selectHolder selectThin mobile-op">
                                                <select name="phone_prefix" id="">
                                                    <option value="">Seçin</option>
                                                    <option value="050">050</option>
                                                    <option value="051">051</option>
                                                    <option value="055">055</option>
                                                    <option value="070">070</option>
                                                    <option value="077">077</option>
                                                    <option value="060">060</option>
                                                </select>
                                            </div>
                                            <input type="number" name="phone" class="input-class">
                                        </div>
                                    </div>
                                </div>
                                <div class="new-address-content-input-groups">
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Şifrə
                                        </label>
                                        <input type="password" class="input-class" name="password">
                                    </div>
                                    <div class="news-address-content-inputs">
                                        <label for="" class="f-size-14-b c-dblack-op-75 mb-12">
                                            Status
                                        </label>
                                        <label for="b">
                                            <div class="private f-size-14-b c-dblack-75">
                                                Aktivdir
                                                <div class="checkboxes">
                                                    <div class="check">
                                                        <input type="checkbox" name="status" checked="" id="b">
                                                        <label for="b"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="new-address-button">
                                    Yadda saxla
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
