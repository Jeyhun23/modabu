@extends('layouts.v2.admin')
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tagify/3.23.0/tagify.min.css"
          integrity="sha512-SxgrVgd3c/mmDd/98A5HXv/g1RSyvhTS/D7/a3N3kNSRqf1YEM83qyHafYZcfzjoM99ZDGEgDcDBkSpBhKKyJQ=="
          crossorigin="anonymous"/>
@endpush

@section('breadcrump')
    <div class="top-section b-black">
        <div class="container">
            <div class="top-section-inner">
                <div>
                    <div class="page-links">
                        <a class="c-white-op-75 f-size-14">Ana səhifə > </a>
                        <span class="c-white-op-50 f-size-14">
                                Məhsullar
                            </span>
                    </div>
                    <h6 class="f-size-24 c-white">
                        Məhsullar
                    </h6>
                </div>
                <a href="{{route('products.create')}}" class="button-blue f-size-14 c-white bradius-4  b-blue">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8 1.06665V14.9333M1.06667 7.99998H14.9333" stroke="white"/>
                    </svg>
                    Yenisini əlavə et
                </a>
            </div>
        </div>
    </div>
@endsection

@push('breadcrump_button')
@endpush

@section('content')
    <div class="homepage-content b-white-1">
        <form class="search-bar" action="{{request()->fullUrl()}}" method="get">
            <div class="container">
                <div class="row">
                    <div class="xl-2">
                        <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                            Məhsul adı
                        </label>
                        <input type="text" name="title" class="input-class" value="{{request('title')}}">
                    </div>
                    <div class="xl-2">
                        <div class="news-address-content-inputs">
                            <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                                Brend
                            </label>
                            <div class="mobile-select">
                                <div class="selectHolder selectThin">
                                    <select name="brand" id="">
                                        <option value="">Hamısı</option>
                                        @foreach($brands as $brand)
                                            <option
                                                {{request('brand') == $brand->id ? "selected" : " "}} value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="xl-2">
                        <div class="news-address-content-inputs">
                            <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                                Kateqoriya
                            </label>
                            <div class="mobile-select">
                                <div class="selectHolder selectThin">
                                    <select name="category" id="">
                                        <option value="">Hamısı</option>
                                        @foreach($categories as $category)
                                            <option
                                                {{request('category') == $category->id ? "selected" : " "}} value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="xl-2">
                        <div class="news-address-content-inputs">
                            <label for="" class="label-class c-dblack-50 f-size-14 mb-12">
                                Rəf
                            </label>
                            <div class="mobile-select">
                                <div class="selectHolder selectThin">
                                    <select name="shelf" id="">
                                        <option value="">Hamısı</option>
                                        @foreach($shelfes as $shelf)
                                            <option
                                                {{request('shelf') == $shelf->id ? "selected" : " "}} value="{{$shelf->id}}">{{$shelf->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="xl-4">
                        <div class="search-bars-right">
                            <div class="search-buttons">
                                <a href="{{request()->url()}}" class="search-delete">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.5">
                                            <path d="M1.59998 1.59998L14.4 14.4M1.59998 14.4L14.4 1.59998"
                                                  stroke="#0B0B18"/>
                                        </g>
                                    </svg>
                                </a>
                                <button type="submit" class="search-button b-black bradius-4">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M15.4667 15.4667L11.2 11.2M6.93332 13.3333C3.3987 13.3333 0.533325 10.4679 0.533325 6.93332C0.533325 3.3987 3.3987 0.533325 6.93332 0.533325C10.4679 0.533325 13.3333 3.3987 13.3333 6.93332C13.3333 10.4679 10.4679 13.3333 6.93332 13.3333Z"
                                            stroke="white"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="search-result-parent">
            <div class="container">
                <div class="search-result">
                    <div class="search-result-header">
                            <span class="f-size-14 search-esult-span c-dblack-75">
                                {{$products->total()}} nəticə
                            </span>
                        <button class="delete-search-result">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path
                                        d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                        stroke="#05061E"/>
                                </g>
                            </svg>
                            <span class="f-size-14">
                                    Sil
                                </span>
                        </button>
                    </div>
                    <div class="search-result-body">
                        <div class="table-content-overflow">
                            <table class="search-result-table-overflow search-result-table">
                                <tr>
                                    <th class="search-result-table-th">
                                        <label class="checkbox-parent">
                                            <input type="checkbox" id="checkParent" class="checkbox-table">
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <th>#</th>
                                    <th>Thumbnail</th>
                                    <th>Başlıq</th>
                                    <th>Barkod</th>
                                    <th>Rəf</th>
                                    <th>Rəng</th>
                                    <th>Qiymət</th>
                                    <th>Brend</th>
                                    <th>Stok</th>
                                    <th>Status</th>
                                    <th>Kateqoriya</th>
                                    <th style="width: 100px;text-align: center;">Tənzimləmələr</th>
                                </tr>
                                @forelse($products as $item)
                                    <tr>
                                        <td class="search-result-table-td">
                                            <label class="checkbox-parent ">
                                                <input type="checkbox" class="checkbox-table">
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td>{{$item->id}}</td>
                                        <td><img src="{{asset('storage/'.$item->thumbnail)}}" width="100" alt=""></td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->shelf ? $item->shelf->barcode: 'Seçilməyib'}}</td>
                                        <td>{{$item->shelf ?$item->shelf->name: 'Seçilməyib'}}</td>
                                        <td>
                                            @foreach($item->colors as $color)
                                                {{$color->color ? $color->color->name: ''}}{{ !$loop->last ? ',' : ''}}
                                            @endforeach
                                        </td>
                                        <td>{{$item->price}}</td>
                                        <td>{{$item->brand ? $item->brand->name : ''}}</td>
                                        <td>{{$item->qty}}  </td>
                                        <td>
                                            <label for="product-{{$item->id}}">
                                                <div class="private f-size-14-b c-dblack-75">
                                                    <div class="checkboxes">
                                                        <div class="check">
                                                            <input type="checkbox"
                                                                   data-url="{{route('products.status',['product_id' => $item->id])}}"
                                                                   {{$item->confirmed ? 'checked' : ''}} class="productStatus"
                                                                   id="product-{{$item->id}}">
                                                            <label for="product-{{$item->id}}"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </label>
                                        </td>
                                        <td>{{$item->category ? $item->category->name : ''}}</td>
                                        <td class="right-button-parent">
                                            <div class="right-buttons">
                                                <a href="{{route('products.edit',['product' => $item->id])}}"
                                                   class="table-buttons">
                                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g opacity="0.5">
                                                            <path
                                                                d="M1.06667 11.7333L0.713112 11.3797L0.566666 11.5262V11.7333H1.06667ZM10.6667 2.1333L11.0202 1.77975C10.9265 1.68598 10.7993 1.6333 10.6667 1.6333C10.5341 1.6333 10.4069 1.68598 10.3131 1.77975L10.6667 2.1333ZM13.8667 5.3333L14.2202 5.68685C14.4155 5.49159 14.4155 5.17501 14.2202 4.97975L13.8667 5.3333ZM4.26667 14.9333V15.4333H4.47377L4.62022 15.2869L4.26667 14.9333ZM1.06667 14.9333H0.566666C0.566666 15.2094 0.790523 15.4333 1.06667 15.4333V14.9333ZM1.42022 12.0869L11.0202 2.48685L10.3131 1.77975L0.713112 11.3797L1.42022 12.0869ZM10.3131 2.48685L13.5131 5.68685L14.2202 4.97975L11.0202 1.77975L10.3131 2.48685ZM13.5131 4.97975L3.91311 14.5797L4.62022 15.2869L14.2202 5.68685L13.5131 4.97975ZM4.26667 14.4333H1.06667V15.4333H4.26667V14.4333ZM1.56667 14.9333V11.7333H0.566666V14.9333H1.56667ZM9.06667 15.4333H15.9998V14.4333H9.06667V15.4333Z"
                                                                fill="#05061E"/>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <button
                                                    onclick="deletepop('{{route('products.destroy',['product'=> $item->id])}}')"
                                                    class="table-buttons">
                                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g opacity="0.5">
                                                            <path
                                                                d="M5.25714 3.32143V1.92857C5.25714 1.41574 5.66648 1 6.17143 1H9.82857C10.3335 1 10.7429 1.41574 10.7429 1.92857V3.32143M1 3.78571H15M3.37143 3.78571V13.0714C3.37143 13.5843 3.78077 14 4.28571 14H11.7143C12.2192 14 12.6286 13.5843 12.6286 13.0714V3.78571M8 6.62958V10.2277V11.4175"
                                                                stroke="#05061E"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                                <button class="table-buttons">
                                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g opacity="0.5">
                                                            <path
                                                                d="M15 2L15.454 2.20953C15.5397 2.02378 15.5036 1.80455 15.3629 1.65606C15.2222 1.50757 15.0052 1.45979 14.8151 1.53544L15 2ZM1 7.57143L0.815122 7.10686C0.635786 7.17823 0.513544 7.34645 0.501049 7.53906C0.488554 7.73167 0.588037 7.91427 0.756649 8.00821L1 7.57143ZM9 15L8.58004 15.2714C8.67898 15.4245 8.85343 15.5116 9.03528 15.4988C9.21714 15.4859 9.37758 15.3751 9.45398 15.2095L9 15ZM14.8151 1.53544L0.815122 7.10686L1.18488 8.03599L15.1849 2.46456L14.8151 1.53544ZM0.756649 8.00821L5.75665 10.7939L6.24335 9.92036L1.24335 7.13464L0.756649 8.00821ZM5.58004 10.6285L8.58004 15.2714L9.41996 14.7286L6.41996 10.0858L5.58004 10.6285ZM9.45398 15.2095L15.454 2.20953L14.546 1.79047L8.54602 14.7905L9.45398 15.2095ZM14.6598 1.6336L5.65977 9.99075L6.34023 10.7235L15.3402 2.3664L14.6598 1.6336Z"
                                                                fill="#0B0B18"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-options">
                <div class="page-options-content">
                        <span class="page-op-span f-size-14 c-black-op-75">
                            Hər səhifədə nəticə sayı
                        </span>
                    <div class="dropdown">
                        <div class="dropdown-header">
                                 <span class="f-size-14 c-black-op-50">
                                    {{$products->perPage()}}
                                 </span>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g opacity="0.5">
                                    <path d="M4.79999 6.93335L7.99999 10.1333L11.2 6.93335" stroke="#0B0B18"
                                          stroke-linecap="square"/>
                                </g>
                            </svg>
                        </div>
                        <ul class="dropdown-body">
                            @foreach(config('admin.limits') as $limit)
                                <li>
                                    <a href="{{request()->fullUrlWithQuery(['limit' => $limit])}}">
                                        {{$limit}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                {{$products->appends(request()->except('page'))->links('admin.v2.partials.simple-pagination')}}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('.productStatus').on('change', function () {
            var url = $(this).attr('data-url');
            $.get(url, function (result) {
                successSave()
            })
        })
    </script>
@endpush
