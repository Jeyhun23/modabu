<!--begin::Header-->
<div id="kt_header" class="header header-fixed">

    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">

        <!--begin::Left-->
        <div class="d-flex align-items-stretch mr-3">

            <!--begin::Header Logo-->
            <div class="header-logo">
                <a href="{{ route('admin.dashboard') }}">
                    <img alt="Modabu Logo" width="120px" src="{{asset("assets/media/logo2.svg")}}" class="logo-default max-h-40px" />
                    <img alt="Logo" src="{{asset("assets/media/logo2.svg")}}" class="logo-sticky max-h-40px" />
                </a>
            </div>

            <!--end::Header Logo-->

            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">

                <!--begin::Header Menu-->
                <div id="kt_header_menu"
                    class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">

                    <!--begin::Header Nav-->
                    <ul class="menu-nav">
                        <!-- <li class="menu-item menu-item-open menu-item-here menu-item-submenu menu-item-rel menu-item-open menu-item-here"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="index.php" class="menu-link">
                                <span class="menu-text">Əsas</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> -->
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.users') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">İstifadəçilər</span>
                            </a>
                        </li>
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.orders') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Sifarişlər</span>
                            </a>
                        </li>
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.products') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Məhsullar</span>
                            </a>
                        </li>
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.banners') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Bannerlər</span>
                            </a>
                        </li>
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.products') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Kateqoriyalar</span>
                            </a>
                        </li>

                        {{-- <li class="menu-item  menu-item-submenu menu-item-rel" data-menu-toggle="click"
                            aria-haspopup="true">
                            <a href="javascript:;" class="menu-link menu-toggle">
                                <span class="menu-text">Blog</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                <ul class="menu-subnav">
                                    <li class="menu-item  menu-item-submenu">
                                        <a href="{{ route('admin.products') }}" class="menu-link">
                                            <span class="svg-icon menu-icon"></span>
                                            <span class="menu-text">Categories</span>
                                        </a>
                                    </li>
                                    <li class="menu-item  menu-item-submenu">
                                        <a href="{{ route('admin.products') }}" class="menu-link">
                                            <span class="svg-icon menu-icon"></span>
                                            <span class="menu-text">Blogs</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li> --}}

                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.products') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Əlaqə</span>
                            </a>
                        </li>
                        <li class="menu-item  menu-item-submenu">
                            <a href="{{ route('admin.products') }}" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Tənzimləmələr</span>
                            </a>
                        </li>
                        {{-- 
                        <li class="menu-item  menu-item-submenu">
                            <a href="?page=categories" class="menu-link">
                                <span class="svg-icon menu-icon"></span>
                                <span class="menu-text">Categories</span>
                            </a>
                        </li>
                         --}}
                    </ul>
                </div>
            </div>
        </div>

        <!--end::Left-->

        <!--begin::Topbar-->
        <div class="topbar">
            <!--begin::User-->
            <div class="dropdown">

                <!--begin::Toggle-->
                <div class="topbar-item ">
                    <div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto"
                        id="kt_quick_user_toggle">
                        <span
                            class="text-white opacity-70 font-weight-bold font-size-base d-none d-md-inline mr-1">Salam,</span>
                        <span
                            class="text-white opacity-90 font-weight-bolder font-size-base d-none d-md-inline mr-4">{{ auth()->user()->name }}</span>
                        <span class="symbol symbol-35">
                            <span class="symbol-label text-white font-size-h5 font-weight-bold bg-white-o-30">{{ substr(auth()->user()->name, 0, 1) }}</span>
                        </span>
                    </div>
                </div>

                <!--end::Toggle-->
            </div>

            <!--end::User-->
        </div>

        <!--end::Topbar-->
    </div>

    <!--end::Container-->
</div>

<!--end::Header-->