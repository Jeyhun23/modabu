
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @includeIf('admin.v2.partials.header')
</head>

<body>
<div class="@yield('body_class','homepage')">
    @includeIf('admin.v2.partials.modals')
    @includeIf('admin.v2.partials.menu')
    @yield('breadcrump')
    @yield('content')
    @includeIf('admin.v2.partials.footer')
</div>


<!-- js plugins -->
@includeIf('admin.v2.partials.plugins',['type' => 'js'])

<script src="{{asset('js/main.js?v=1.1.0')}}"></script>
<script src="{{asset('js/for_ajax.js')}}"></script>
@stack('scripts')
</body>

</html>
