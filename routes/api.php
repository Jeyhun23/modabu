<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\Auth\ApiAuthController;
use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\V2\Api\ApiController;
use App\Http\Controllers\WishlistController;


//V2
use App\Http\Controllers\V2\Api\Auth\ForgotPasswordController;
use App\Http\Controllers\V2\Api\Auth\LoginController;
use App\Http\Controllers\V2\Api\Auth\RegisterController;
use App\Http\Controllers\V2\Api\Auth\ResetPasswordController;
use App\Http\Controllers\V2\Api\Auth\VerificationController;
use App\Http\Controllers\V2\Api\Cart\CartController as CartController_V2;
use App\Http\Controllers\V2\Api\Cart\OrderController as OrderController_V2;
use App\Http\Controllers\V2\Api\Payment\PaymentController as PaymentController_V2;
use App\Http\Controllers\V2\Api\Product\ProductController as ProductController_V2;
use App\Http\Controllers\V2\Api\User\AddressController as AddressController_V2;
use App\Http\Controllers\V2\Api\User\OrderController as UserOrderController_V2;
use App\Http\Controllers\V2\Api\User\UserController as UserController_V2;


use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('/register', [ApiAuthController::class, 'register']); //
Route::post('/login', [ApiAuthController::class, 'login']); //

Route::group(['prefix' => 'password'], function () {
    Route::post('/create', [PasswordResetController::class, 'create']);
    Route::get('/{token}', [PasswordResetController::class, 'find']);
    Route::post('/reset', [PasswordResetController::class, 'reset']);
}); //



Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user', [UserController::class, 'getUser']);
}); //

Route::group(['prefix' => 'products'], function () {
    Route::get('/', [ProductController::class, 'list']);
    Route::get('/variations', [ProductController::class, 'getVariations']);
    Route::get('/{slug}', [ProductController::class, 'show']);
}); //

Route::group(['prefix' => 'cart'], function () {
    //
    Route::get('/', [CartController::class, 'getCart']);
    Route::post('/', [CartController::class, 'makeOrder']);
    Route::delete('/', [CartController::class, 'clearCart']);
    Route::post('/all', [CartController::class, 'addToCartAll']);
    Route::post('/{product_id}', [CartController::class, 'addToCart']);
    Route::delete('/{product_id}', [CartController::class, 'removeFromCart']);
    Route::post('/qty/{product_id}', [CartController::class, 'incrementCartItem']);
    Route::delete('/qty/{product_id}', [CartController::class, 'decrementCartItem']);
}); //

Route::group(['prefix' => 'wishlists', 'middleware' => 'auth:api'], function () {
    Route::get('/', [WishlistController::class, 'userWishlists']);
    Route::post('/{product_id}', [WishlistController::class, 'addWishlist']);
    Route::delete('/{product_id}', [WishlistController::class, 'removeWishlist']);
}); //

Route::group(['prefix' => 'profile', 'middleware' => 'auth:api'], function () {
    Route::get('/', [UserController::class, 'getUser']);
    Route::put('/', [UserController::class, 'updateContactInformations']);
    Route::put('/settings', [UserController::class, 'updateSettings']);
    Route::get('/orders', [UserController::class, 'getOrders']);

    Route::get('/addresses', [AddressController::class, 'list']);
    Route::post('/addresses', [AddressController::class, 'store']);
    Route::put('/addresses/{address}', [AddressController::class, 'update']);
    Route::post('/addresses/{address}', [AddressController::class, 'setDefault']);
    Route::delete('/addresses/{address}', [AddressController::class, 'destroy']);
}); //

Route::get('/colors', [ProductController::class, 'colors']);
Route::get('/sizes', [ProductController::class, 'sizes']);

Route::group(['prefix' => 'brands'], function () {
    Route::get('/', [BrandController::class, 'list']);
    Route::get('/{slug}', [BrandController::class, 'show']);
});

Route::group(['prefix' => 'sizes'], function () {
    Route::get('/', [ProductController::class, 'list']);
    Route::get('/{slug}', [ProductController::class, 'show']);
});

Route::group(['prefix' => 'categories'], function () {
    Route::get('/', [CategoryController::class, 'list']);
    Route::get('/{slug}', [CategoryController::class, 'show']);
});

Route::post('/pay/{product_id}', [OrderController::class, 'payDirectly']);
Route::post('/contact', [ContactController::class, 'store']);
Route::get('/banners', [BannerController::class, 'list']);


Route::prefix('v2')->group(function () {
    //auth
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/register', [RegisterController::class, 'register']);
    Route::post('/check', [VerificationController::class, 'check']);
    Route::post('/verify', [VerificationController::class, 'verify']);
    Route::put('/register', [RegisterController::class, 'setPassword']);
    Route::get('/logout', [LoginController::class, 'logout']);

    //group
    Route::group(['prefix' => 'password'], function () {
        Route::post('/forgot', [ForgotPasswordController::class, 'sendToken']);
        Route::post('/reset', [ResetPasswordController::class, 'reset']);
    });

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('/user', [UserController::class, 'getUser']);
    });

    Route::group(['prefix' => 'profile', 'middleware' => 'auth:api'], function () {
        Route::get('/', [UserController_V2::class, 'getUser']);
        Route::put('/', [UserController_V2::class, 'updateContactInformations']);
        Route::put('/settings', [UserController_V2::class, 'updateSettings']);
        Route::get('/orders', [UserOrderController_V2::class, 'getOrders']);
        Route::get('/orders/{order}', [UserOrderController_V2::class, 'getOrder']);

        Route::group(['prefix' => 'addresses'], function () {
            Route::get('/', [AddressController_V2::class, 'list']);
            Route::post('/', [AddressController_V2::class, 'store']);
            Route::put('/{address}', [AddressController_V2::class, 'update']);
            Route::post('/{address}', [AddressController_V2::class, 'setDefault']);
            Route::delete('/{address}', [AddressController_V2::class, 'destroy']);
        });
    });


    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', [CartController_V2::class, 'getCart'])->middleware('auth:api');
        Route::post('/', [OrderController_V2::class, 'makeOrder']);
        Route::delete('/', [CartController_V2::class, 'clearCart']);
        Route::post('/all', [CartController_V2::class, 'addToCartAll']);
        Route::post('/{product_id}', [CartController_V2::class, 'addToCart']);
        Route::delete('/{product_id}', [CartController_V2::class, 'removeFromCart']);
        Route::post('/qty/{product_id}', [CartController_V2::class, 'incrementCartItem']);
        Route::delete('/qty/{product_id}', [CartController_V2::class, 'decrementCartItem']);
    });

    Route::group(['prefix' => 'wishlists', 'middleware' => 'auth:api'], function () {
        Route::get('/', [WishlistController::class, 'userWishlists']);
        Route::post('/{product_id}', [WishlistController::class, 'addWishlist']);
        Route::delete('/{product_id}', [WishlistController::class, 'removeWishlist']);
    });

    Route::post('/order/{product_id}', [OrderController_V2::class, 'payDirectly'])->middleware('guest');

    Route::prefix('payment')->group(function () {
        Route::post('/', [PaymentController_V2::class, 'approveUrl']);
        Route::post('/cancelled', [PaymentController_V2::class, 'cancelUrl']);
        Route::post('/declined', [PaymentController_V2::class, 'declineUrl']);
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [ProductController_V2::class, 'list']);
        Route::get('/variations', [ProductController_V2::class, 'getVariations']);
        Route::get('/{slug}', [ProductController_V2::class, 'show']);
    });

    Route::get('/cities', [ApiController::class, 'cities']);

    Route::get('/colors', [ProductController::class, 'colors']);

    Route::group(['prefix' => 'brands'], function () {
        Route::get('/', [BrandController::class, 'list']);
        Route::get('/{slug}', [BrandController::class, 'show']);
    });

    Route::group(['prefix' => 'sizes'], function () {
        //Route::get('/', [ProductController::class, 'list']);
        //Route::get('/{slug}', [ProductController::class, 'show']);
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [CategoryController::class, 'list']);
        Route::get('/{slug}', [CategoryController::class, 'show']);
    });

    Route::post('/contact', [ContactController::class, 'store']);
    Route::get('/banners', [BannerController::class, 'list']);
    //
});

//Route::get('check', function () {
//    $orderid = "18086302";
//    $sessionid = "709276FB2E4A4F40786AC23EBE50B6B4";
//    $order = (new PaymentController_V2())->getOrderStatus($orderid, $sessionid);
//});
