<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\HomeController;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// V2 Routes
Route::group(['prefix' => '/v2'], function () {
    Route::get('/', [App\Http\Controllers\V2\Auth\LoginController::class, 'login'])->name('login');
    Route::post('login', [App\Http\Controllers\V2\Auth\LoginController::class, 'loginUser'])->name('loginUser');

    Route::get('debugadminon', function () {
        \Debugbar::enable();
    });
    Route::get('debugadminoff', function () {
        \Debugbar::disable();
    });

//    Route::post('/forgot_password', [App\Http\Controllers\V2\Auth\LoginController::class, 'forgot_password'])->name('v2.admin.forgot_password.store');
//    Route::get('/forgot_password', [App\Http\Controllers\V2\Auth\LoginController::class, 'get_forgot_password'])->name('v2.admin.forgot_password');
//    Route::get('/resetpass', [App\Http\Controllers\V2\Auth\LoginController::class, 'refresh_password'])->name('v2.admin.reset_password');
//    Route::post('/resetpass', [App\Http\Controllers\V2\Auth\LoginController::class, 'refresh_password_post']);

    Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'auth.admin']], function () {
        Route::group(['middleware' => 'checkRole:1'], function () {
            Route::post('/logout', [App\Http\Controllers\V2\Auth\LoginController::class, 'logout'])->name('v2.admin.logout');

            Route::get('/client_login/{user}', [App\Http\Controllers\V2\AdminController::class, 'client_login'])->name('client_login');
            Route::resource('users', App\Http\Controllers\V2\UserController::class);
            Route::resource('roles', App\Http\Controllers\V2\RoleController::class);

            Route::resource('smstemplates', App\Http\Controllers\V2\SMSTemplateController::class);
            Route::delete('/deleteall_sms_tem', [\App\Http\Controllers\V2\SMSTemplateController::class, 'destroyAllSelections'])->name('deleteall_sms_tem');
            Route::put('/update/sms_temp/status/{smstemplate}', [\App\Http\Controllers\V2\SMSTemplateController::class, 'updateVisible'])->name('update.sms_temp');
            Route::get('/send/sms/clients', [\App\Http\Controllers\V2\SMSTemplateController::class, 'smsSendToClient'])->name('send.sms.admin');
            Route::post('/send/sms/clients', [\App\Http\Controllers\V2\SMSTemplateController::class, 'smsSendToClientPost'])->name('send.sms.admin.post');

            Route::resource('emailtemplates', App\Http\Controllers\V2\EmailTemplateController::class);
            Route::delete('/deleteall_email_tem', [\App\Http\Controllers\V2\EmailTemplateController::class, 'destroyAllSelections'])->name('deleteall_email_tem');
            Route::put('/update/email_temp/status/{emailtemplate}', [\App\Http\Controllers\V2\EmailTemplateController::class, 'updateVisible'])->name('update.email_temp');

            Route::delete('/adminus_destroyAllSelections', [\App\Http\Controllers\V2\AdminPanelUsersController::class, 'destroyAllSelections'])->name('adminus.deleteSelections');
            Route::resource('admin_panel_users', App\Http\Controllers\V2\AdminPanelUsersController::class);
            Route::delete('/roles_destroyAllSelections', [\App\Http\Controllers\V2\RoleController::class, 'destroyAllSelections'])->name('roles.deleteSelections');
            Route::resource('mektublar', App\Http\Controllers\V2\MektublarController::class);
            Route::delete('/mektub_destroyAllSelections', [\App\Http\Controllers\V2\MektublarController::class, 'destroyAllSelections'])->name('mektub.deleteSelections');
            Route::resource('coupons', App\Http\Controllers\V2\CouponController::class);
            Route::delete('/coupons_destroyAllSelections', [\App\Http\Controllers\V2\CouponController::class, 'destroyAllSelections'])->name('coupons.deleteSelections');

            Route::resource('category', \App\Http\Controllers\V2\CategoryController::class)->except('show');
            Route::delete('/categories_destroyAllSelections', [\App\Http\Controllers\V2\CategoryController::class, 'destroyAllSelections'])->name('category.deleteSelections');
            Route::put('/categories/update_category_visibility/{category}', [\App\Http\Controllers\V2\CategoryController::class, 'update_category_visibility'])->name('update_category_visibility');

        });
        Route::group(['middleware' => 'checkRole:1|4|3'], function () { //Kuryer
            Route::get('orders', [App\Http\Controllers\V2\OrderController::class, 'index'])->name('orders');
            Route::get('orders/{orderId}/{type}/showDetail', [App\Http\Controllers\V2\OrderController::class, 'showDetail'])->name('orders.showDetail');
            Route::post('orders/detail/update', [App\Http\Controllers\V2\OrderController::class, 'updateOrderDetailStatus'])->name('orders.updateOrderDetailStatus');
            Route::delete('orders/{detailId}/detail', [App\Http\Controllers\V2\OrderController::class, 'deleteOrderDetail'])->name('orders.deleteOrderDetail');
            Route::post('/orders/stock', [App\Http\Controllers\V2\OrderController::class, 'updateStock'])->name('orders.stock');
        });
        Route::group(['middleware' => 'checkRole:1|5'], function () { // Kontent
            Route::resource('banners', App\Http\Controllers\V2\BannerController::class);
            Route::put('/banner/update_banner_visibility/{banner}', [\App\Http\Controllers\V2\BannerController::class, 'update_banner_visibility'])->name('update_banner_visibility');
            Route::delete('/banner/delete/{banner}', [\App\Http\Controllers\V2\BannerController::class, 'destroy'])->name('delete_banner');
            Route::resource('brands', App\Http\Controllers\V2\BrandController::class);
            Route::delete('/stories_destroyAllSelections', [\App\Http\Controllers\V2\StoryController::class, 'destroyAllSelections'])->name('story.deleteSelections');
            Route::delete('/color_destroyAllSelections', [\App\Http\Controllers\V2\ColorController::class, 'destroyAllSelections'])->name('color.deleteSelections');
            Route::resource('stories', App\Http\Controllers\V2\StoryController::class);
            Route::resource('colors', App\Http\Controllers\V2\ColorController::class);
        });
        Route::group(['middleware' => 'checkRole:1|4|5'], function () { // Kontent ve ANbar
            Route::resource('products', App\Http\Controllers\V2\ProductController::class);
            Route::get('/product/{product_id}/changeStatus', [App\Http\Controllers\V2\ProductController::class, 'productsStatus'])->name('products.status');
            Route::post('/product/removeProductFromCombine', [App\Http\Controllers\V2\ProductController::class, 'removeProductFromCombine'])->name('products.removeProductFromCombine');
        });
        Route::group(['middleware' => 'checkRole:1|4'], function () {  //ANbar
            Route::resource('refler', App\Http\Controllers\V2\ReflerController::class);
            Route::delete('/refler_destroyAllSelections', [\App\Http\Controllers\V2\ReflerController::class, 'destroyAllSelections'])->name('refler.deleteSelections');
        });
        Route::group(['middleware' => 'checkRole:1'], function () {  //Statistics
            Route::get('statistics', [App\Http\Controllers\V2\StatisticsController::class, 'index'])->name('statistics.index');
            Route::get('statistics/{type}', [App\Http\Controllers\V2\StatisticsController::class, 'getData'])->name('statistics.getData');
        });
//        Route::get('dashboard', [App\Http\Controllers\V2\AdminController::class, 'dashboard'])->name('dashboard');
    });
});


// V1 Routes

//Route::redirect('/', '/login');
Route::get('', [HomeController::class, 'index'])->name('main');
Route::get('/login', [LoginController::class, 'index'])->name('admin.login');
Route::post('/login', [LoginController::class, 'login']);

Route::prefix('admin')->middleware(['auth', 'auth.admin'])->name('admin.')->group(function () {
    Route::get('/', [AdminController::class, 'products'])->name('dashboard');
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
    Route::post('/logout', [LoginController::class, 'logout']);

    Route::post('/file/{type}', [AdminController::class, 'uploadFile'])->name('file.upload');
    Route::delete('/file/{id}', [AdminController::class, 'deleteFile'])->name('file.delete');

    Route::get('api/products', [AdminController::class, 'productsList']);
    Route::get('api/users', [AdminController::class, 'usersList']);

    Route::get('/products', [AdminController::class, 'products'])->name('products');
    Route::get('/products/create', [AdminController::class, 'productsCreate'])->name('products.create');
    Route::post('/products/create', [AdminController::class, 'productsStore'])->name('products.store');
    Route::get('/products/{product_id}', [AdminController::class, 'productsEdit'])->name('products.edit');
    Route::put('/products/{product_id}', [AdminController::class, 'productsUpdate'])->name('products.update');
    Route::post('/products/{product_id}', [AdminController::class, 'productsStatus'])->name('products.status');
    Route::delete('/products/{product_id}', [AdminController::class, 'productsDelete'])->name('products.delete');

    Route::get('/orders', [AdminController::class, 'orders'])->name('orders');
    Route::post('/orders', [AdminController::class, 'ordersStatus'])->name('orders.status');
    Route::post('/orders/stock', [AdminController::class, 'updateStock'])->name('stock');
    Route::delete('/orders/{orderno}/{type}', [AdminController::class, 'ordersDelete']);
    Route::get('/api/orders', [AdminController::class, 'ordersList']);
    Route::get('/api/orders/{order}/{type}', [AdminController::class, 'ordersInfo']);

    Route::get('/banners', [BannerController::class, 'index'])->name('banners');
    Route::post('/banners', [BannerController::class, 'store'])->name('banners.store');
    Route::put('/banners', [BannerController::class, 'update'])->name('banners.update');
    Route::post('/banners/{banner_id}', [BannerController::class, 'status'])->name('banners.status');
    Route::delete('/banners/{banner_id}', [BannerController::class, 'destroy'])->name('banners.delete');

    Route::get('/thumb', [AdminController::class, 'addTuhumbnail']);
    Route::get('/resize/{type}/{down}/{up}', [AdminController::class, 'resize']);
    Route::get('/variations', [AdminController::class, 'variations']);

    Route::get('/users', [AdminController::class, 'users'])->name('users');
    Route::get('/users/create', [AdminController::class, 'usersCreate'])->name('users.create');
    Route::post('/users/create', [AdminController::class, 'usersStore'])->name('users.store');
    Route::get('/users/{product_id}', [AdminController::class, 'usersEdit'])->name('users.edit');
    Route::put('/users/{product_id}', [AdminController::class, 'usersUpdate'])->name('users.update');
    Route::post('/users/{product_id}', [AdminController::class, 'usersStatus'])->name('users.status');
    Route::delete('/users/{product_id}', [AdminController::class, 'usersDelete'])->name('users.delete');

    Route::get('/codes', [AdminController::class, 'setClientCodes']);
    Route::get('/addresses', [AdminController::class, 'setAddresses']);
    Route::get('/addresses-orders', [AdminController::class, 'setOrderAddresses']);
});
