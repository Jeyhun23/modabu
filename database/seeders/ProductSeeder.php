<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            ['name' => 'Trend Alaçatı Stili', 'logo' => 'brands/brand.png', 'slug' => 'brands-1', 'meta' => '', 'published' => true],
            ['name' => 'Lafaba', 'logo' => 'brands/brand.png', 'slug' => 'brands-2', 'meta' => '', 'published' => true],
            ['name' => 'Dilvin', 'logo' => 'brands/brand.png', 'slug' => 'brands-3', 'meta' => '', 'published' => true],
            ['name' => 'Kaktüs', 'logo' => 'brands/brand.png', 'slug' => 'brands-4', 'meta' => '', 'published' => true],
        ]);

        DB::table('categories')->insert([
            ['parent_id' => 0, 'name' => 'Svitşört', 'banner' => '/files/category.png', 'slug' => 'category-1', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Şalvarlar', 'banner' => '/files/category.png', 'slug' => 'category-2', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Üst Geyim', 'banner' => '/files/category.png', 'slug' => 'category-3', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Dəstlər', 'banner' => '/files/category.png', 'slug' => 'category-4', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Sviter', 'banner' => '/files/category.png', 'slug' => 'category-5', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Köynək və Bluzlar', 'banner' => '/files/category.png', 'slug' => 'category-6', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Donlar və Ətəklər', 'banner' => '/files/category.png', 'slug' => 'category-7', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Köynək və Bluzlar', 'banner' => '/files/category.png', 'slug' => 'category-8', 'meta' => '', 'published' => true],
            ['parent_id' => 0, 'name' => 'Donlar və Ətəklər', 'banner' => '/files/category.png', 'slug' => 'category-9', 'meta' => '', 'published' => true],
            ['parent_id' => 4, 'name' => 'Eşofman Dəsti', 'banner' => '/files/category.png', 'slug' => 'category-10', 'meta' => '', 'published' => true],
            ['parent_id' => 4, 'name' => 'Eşofman Altı', 'banner' => '/files/category-.png', 'slug' => 'category-11', 'meta' => '', 'published' => true],
            ['parent_id' => 4, 'name' => 'Pijama Dəsti', 'banner' => '/files/category.png', 'slug' => 'category-12', 'meta' => '', 'published' => true],
        ]);

        DB::table('products')->insert([
            'user_id' => true,
            'category_id' => 2,
            'brand_id' => true,
            'title' => 'IPhone 12',
            'thumbnail' => 'files/products/ajhsdgkhjbcvbkdfgjkhasdgfhsadfnzcbvh-sldkjhgb.jpg',
            'description' => 'IPhone 12 Description',
            'slug' => 'test-product',
            'meta' => json_encode([
                'title' => 'IPhone 12',
                'Description' => 'IPhone 12 Description',
                'image' => 'files/products/ajhsdgkhjbcvbkdfgjkhasdgfhsadfnzcbvh-sldkjhgb.jpg',
            ]),
            'price' => 100.50,
            'qty' => true,
            'discount' => 10,
            'discount_type' => 2,
            'featured' => 0,
            'confirmed' => true,
        ]);

        DB::table('products')->insert([
            'user_id' => true,
            'category_id' => 3,
            'brand_id' => 2,
            'title' => 'IPhone 11',
            'thumbnail' => 'files/products/ajhsdgkhjbcvbkdfgjkhasdgfhsadfnzcbvh-sldkjhgb.jpg',
            'description' => 'IPhone 11 Description',
            'slug' => 'test-product-2',
            'meta' => json_encode([
                'title' => 'IPhone 11',
                'Description' => 'IPhone 11 Description',
                'image' => 'files/products/ajhsdgkhjbcvbkdfgjkhasdgfhsadfnzcbvh-sldkjhgb.jpg',
            ]),
            'price' => 200,
            'qty' => true,
            'discount' => 10,
            'discount_type' => 2,
            'featured' => 0,
            'confirmed' => true,
        ]);

        DB::table('product_colors')->insert([
            'product_id' => true,
            'color_id' => true,
        ]);

        DB::table('product_attributes')->insert([
            'product_id' => true,
            'attribute_id' => 2,
        ]);

        DB::table('files')->insert([
            [
                'p_id' => true,
                'type' => 'product',
                'name' => 'product-1-first-image',
                'path' => 'files/products/jklshdfagkjasdjfhjklfhasjkhglkajshgkjldhs.png',
                'mime' => 'image/jpeg',
                'file_size' => 536135,
                'deleted_at' => null,
            ],
            [
                'p_id' => true,
                'type' => 'product',
                'name' => 'product-1-second-image',
                'path' => 'files/products/tryertyretyertyrtyretyretyertyertyertyrty.png',
                'mime' => 'image/jpeg',
                'file_size' => 23135,
                'deleted_at' => null,
            ],
            [
                'p_id' => true,
                'type' => 'product',
                'name' => 'product-1-third-image',
                'path' => 'files/products/xcvbnoaiucnbyabyoavjfhvgbvjurhgsfgnbgslhfg.png',
                'mime' => 'image/jpeg',
                'file_size' => 145621,
                'deleted_at' => null,
            ],
        ]);
    }
}