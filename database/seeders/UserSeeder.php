<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Samiq Museyibli',
            'email' => 'samiqmuseyibli@gmail.com',
            'phone' => '994556130398',
            'password' => Hash::make('A123456'),
        ]);
        DB::table('users')->insert([
            'name' => 'Samig Museyibli',
            'email' => 'samigmuseyibli@gmail.com',
            'phone' => '994993030066',
            'password' => Hash::make('A123456'),
        ]);
        DB::table('oauth_clients')->insert([
            'id' => 1,
            'name' => 'Modabu Personal Access Client',
            'secret' => 'JZ9dHwKcUVke6yrKmJ37rPaHZVDZfjmVCb0RP9dA',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
        ]);
    }
}