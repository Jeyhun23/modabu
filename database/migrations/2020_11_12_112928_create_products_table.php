<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->integer('category_id')->nullable();
            $table->integer('brand_id')->nullable();

            $table->string('title')->nullable();
            $table->string('thumbnail')->nullable();
            $table->longText('description')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->longText('meta')->nullable();

            $table->decimal('price', 8, 2)->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('discount', 8, 2)->nullable();
            $table->string('discount_type')->nullable();

            $table->boolean('featured')->default(false);
            $table->boolean('confirmed')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
