<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCombinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('source_product');
            $table->unsignedBigInteger('target_product');
            $table->timestamps();

            $table->foreign('source_product')->references('id')->on('products');
            $table->foreign('target_product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_combines');
    }
}
