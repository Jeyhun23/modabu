<?php

namespace Database\Factories;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => 'files/banners/' . Str::random(32) . '.png',
            'url' => $this->faker->url,
            'position' => $this->faker->numberBetween(1, 2),
            'rank' => $this->faker->randomNumber(),
            'published' => 1,
        ];
    }
}