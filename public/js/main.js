$(document).ready(function () {
    // date select
    $(function () {
        $('.dateRange').daterangepicker({
                opens: "left",
                startDate: moment().subtract(1, 'months'),
                endDate: moment().add(1, 'days'),
                dateFormat: 'yy-mm-dd',
            },
            function (start, end, label) {
                console.log(
                    "A new date selection was made: " +
                    start.format("YYYY-MM-DD") +
                    " to " +
                    end.format("YYYY-MM-DD")
                );
            }
        );

        $(".dropdown-header").click(function () {
            $(".dropdown-body").slideToggle();
        });
    });
    // login password
    $(".toggle-password").click(function () {
        $(this).toggleClass("active");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    // selectbox
    var selectHolder = $(".selectHolder");
    selectHolder.each(function () {
        var currentSelectHolder = $(this);
        var select = currentSelectHolder.find("select");
        var options = select.children();
        var selected_option = select.find("option:checked");

        // SELECT HEADER
        var selectHeader = $("<h5></h5>");
        selectHeader.addClass("select_header");
        selectHeader.html(selected_option.html());
        currentSelectHolder.append(selectHeader);
        // SELECT BODY HOLDER
        var selectBodyHolder = $("<div></div>");
        selectBodyHolder.addClass("select_body_holder");
        currentSelectHolder.append(selectBodyHolder);
        // SELECT BODY
        var selectBody = $("<ul></ul>");
        selectBody.addClass("select_body");
        options.each(function () {
            var option_text = $(this).html();
            var value = $(this).attr("value");
            var disabled = $(this).attr("disabled");
            var li = $("<li></li>");
            li.append(option_text);
            li.attr("data-value", value);
            if (disabled) {
                li.attr("data-disabled", "true");
            } else {
                li.attr("data-disabled", "false");
            }
            selectBody.append(li);
        });
        selectBodyHolder.append(selectBody);
    });
    // CLICK SELECT HEADER
    $(document).on("click", ".select_header", function () {
        $(".selectHolder .select_header").not($(this)).removeClass("active");
        $(".selectHolder .select_body_holder")
            .not($(this).next(0))
            .removeClass("active");
        $(this).toggleClass("active");
        $(this).next(0).toggleClass("active");
    });
    // CLICK SELECT BODY ITEM
    $(document).on("click", ".select_body li", function () {
        var select = $(this).parents(".select_body_holder").prevAll().eq(1);
        if (!$(this).data("disabled")) {
            select.val($(this).data("value")).trigger("change");
            $(this)
                .parents(".select_body_holder")
                .prev()
                .html($(this).html())
                .removeClass("active");
            $(this).parents(".select_body_holder").removeClass("active");
        }
    });

    $(document).on("click", function (e) {
        var target = $(e.target);
        if (
            !(
                target.hasClass("selectHolder") ||
                target.parents(".selectHolder").length
            )
        ) {
            $(".selectHolder").children().removeClass("active");
        }
    });

    approval = () => {
        $(".popup-1").addClass("active");
        $("body").addClass("active");
        console.log(".popup-1");
    };

    dinamik_create = (hides, shows) => {
        hides.forEach(function (it) {
            $(it).hide();
        });
        shows.forEach(function (it) {
            $(it).addClass("active");
        });
        $("body").addClass("active");
    }

    dinamik_create_close = (hides, removeActive) => {
        hides.forEach(function (it) {
            $(it).hide();
        });
        shows.forEach(function (it) {
            $(it).removeClass("active");
        });
        $("body").removeClass("active");
    }

    dinamik_update = (item, prefix, hides, shows, set_values, form_class, form_url, more_input, more_checkbox_if_checked) => {
        set_values.forEach((i) => {
            $(prefix + i).val(item[i]);
        })
        hides.forEach(function (it) {
            $(it).hide();
        });
        shows.forEach(function (it) {
            $(it).addClass("active");
        });
        $(form_class).attr('action', form_url);

        if (more_input) {
            for (const [key, value] of Object.entries(more_input)) {
                $(key).val(value);
            }
        }
        $("body").addClass("active");

        if (more_checkbox_if_checked) {
            for (const [key, value] of Object.entries(more_checkbox_if_checked)) {
                if (value) {
                    $(key).prop('checked', true);
                } else {
                    $(key).prop('checked', false);
                }
            }
        }
    }

    dinamik_update_close = (hides, removeActive) => {
        hides.forEach(function (it) {
            $(it).hide();
        });
        shows.forEach(function (it) {
            $(it).removeClass("active");
        });
        $("body").removeClass("active");
    }

    save = () => {
        $(".popup-2").addClass("active");
        $("body").addClass("active");
    }
    successSave = () => {
        $(".popup-3").addClass("active");
        $("body").addClass("active");
    }
    deletepop = (url, child = false) => {
        $(".popup-4").addClass("active");
        $(".popup-4").find('.deleteBtn').attr("data-url", url);
        if (child) {
            $('.pop-p211').show();
        } else {
            $('.pop-p211').hide();
        }
        $("body").addClass("active");
    }
    deletepop40 = (url, storage) => {
        $('.pir').data('url', url);
        $('.pir').data('storage', storage);
        $(".popup-40").addClass("active");
        $("body").addClass("active");
    }
    deleted = () => {
        $(".popup-5").addClass("active");
        $("body").addClass("active");
    }
    error = (errorText) => {
        $(".popup-6").addClass("active");
        if (errorText !== undefined) {
            $(".popup-6").find(".messageContent").html('');
            $(".popup-6").find(".messageContent").append(errorText);
        }

        $("body").addClass("active");
    }


    $(".popup-close").click(function () {
        $(".popup").removeClass("active");
        $("body").removeClass("active");
    });
    //   $(".layer-popup").click(function(){
    //     $(".popup").removeClass("active");
    //     $("body").removeClass("active");
    //   })

    $("#left-menu-slide li a").click(function () {
        var target = $(this).attr("href");
        $(this).addClass("active");
        $("#left-menu-slide li a").not($(this)).removeClass("active");

        $("html, body")
            .stop()
            .animate({
                    scrollTop: $(target).offset().top,
                },
                300,
                function () {
                    location.hash = target;
                }
            );
    });

    $('.checkbox-table').on("change", function () {
        if ($('.checkbox-table').is(":checked")) {
            $(".delete-search-result").addClass("active");
        } else {
            $(".delete-search-result").removeClass("active");
        }
    });


    $("#checkParent").click(function () {
        $(".checkbox-table:checkbox").not(this).prop("checked", this.checked);
    });

    $(".layer-popup").click(function () {
        $(".popup").removeClass("active");
        $("body").removeClass("active");
    })

    /*
    |--------------------------------------------------------------------------
    |  Send ajax request for delete data
    |--------------------------------------------------------------------------
    */

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).on('click', '.deleteBtn', function () {
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                "_method": 'DELETE'
            },
            success: function (result) {
                deleted();
                location.reload()
            }
        });
    });

    // deleteBtn41 = (sessionStorageName) => {
    //     $.ajax({
    //         url: '/v2/admin/stories_destroyAllSelections',
    //         type: 'post',
    //         data: {
    //             "_method": 'DELETE',
    //             "keys": window.sessionStorage.getItem(sessionStorageName)
    //         },
    //         success: function (result) {
    //             deleted();
    //             location.reload()
    //         }
    //     });
    // }
    // $(document).on('click', '.deleteBtn41', function () {
    //     $.ajax({
    //         url: '/v2/admin/stories_destroyAllSelections',
    //         type: 'post',
    //         data: {
    //             "_method": 'DELETE',
    //             "keys": window.sessionStorage.getItem('story')
    //         },
    //         success: function (result) {
    //             deleted();
    //             location.reload()
    //         }
    //     });
    // });

    function arrayRemove(arr, value) {
        return arr.filter(function (ele) {
            return ele != value;
        });
    }

    checkInput = (sessionStorageName, id, prefixOfNode) => {
        var keys = window.sessionStorage.getItem(sessionStorageName);
        if (keys) {
            var ar = keys.split(',');
            ar = ar.filter((item) => {
                return item != id
            })
            if ($(prefixOfNode + id).is(':checked')) {
                ar.push(id)
            }
            ar = ar.join(',');
            window.sessionStorage.setItem(sessionStorageName, ar);
        } else {
            window.sessionStorage.setItem(sessionStorageName, id);
        }
    }

    window.sessionStorage.setItem("story", '');
    window.sessionStorage.setItem("roles", '');
    window.sessionStorage.setItem("refler", '');
    window.sessionStorage.setItem("color", '');
    window.sessionStorage.setItem("au", '');
    window.sessionStorage.setItem("coupon", '');
    window.sessionStorage.setItem("category", '');
    window.sessionStorage.setItem("smst", '');

    sdAll = (sessionStorageName, value, check_all_class, node_class_checkboxs) => {
        if ($(check_all_class).is(':checked')) {
            window.sessionStorage.setItem(sessionStorageName, value);
            $(node_class_checkboxs).not(this).prop('checked', true);
        } else {
            var ke = window.sessionStorage.getItem(sessionStorageName);
            window.sessionStorage.setItem(sessionStorageName, '');
            $(node_class_checkboxs).not(this).prop('checked', false);
        }
    };

    // new


	 //  tabs

     $("#tabs li a:not(:first),#tabs-2 li a:not(:first),#tabs-3 li a:not(:first)").addClass("inactive");
			
     $(".tabs-container").hide();
     $(".tabs-container:first").show();
     $("#tabs li a,#tabs-2 li a,#tabs-3 li a").click(function () {
         var t = $(this).attr("href");
         $("#tabs li a, #tabs-2 li a,#tabs-3 li a").addClass("inactive");
         $(this).removeClass("inactive");
         $(".tabs-container").hide();
         $(t).fadeIn("slow");
         return false;
     });
     
     if ($(this).hasClass("inactive")) {
         $("#tabs li a,#tabs-2 li a,#tabs-3 li a").addClass("inactive");
         $(this).removeClass("inactive");
         $(".tabs-container").hide();
         $(t).fadeIn("slow");
     }
     

     $( "#datepciker-1, #datepciker-2, #datepciker-3" ).datepicker();


// new
});