"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                saveState: {
                    cookie: false
                },
            },
            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },
            columns: [{
                    field: 'Tarix',
                    title: 'Tarix',
                    width: 90,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Şəkil',
                    title: 'Şəkil',
                    width: 90,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Başlıq',
                    title: 'Başlıq',
                    width: 90,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Sütun',
                    title: 'Sütun',
                    width: 60,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Sıra',
                    title: 'Sıra',
                    width: 60,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Status',
                    title: 'Status',
                    width: 60,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: 'Əməliyyatlar',
                    title: 'Əməliyyatlar',
                    width: 145,
                    sortable: false,
                    autoHide: false,
                }
            ],
        });



        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Statusa');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableHtmlTableDemo.init();
});