// Class definition
var KTTagifyDemos = function() {
    // Private functions
    var demo1 = function() {
        var input = document.getElementById('kt_tagify_1'),
            // init Tagify script on the above inputs
            tagify = new Tagify(input);


        // "remove all tags" button event listener
        //document.getElementById('kt_tagify_1_remove').addEventListener('click', tagify.removeAllTags.bind(tagify));


        // Chainable event listeners
        tagify.on('add', onAddTag)
            .on('remove', onRemoveTag);
        //.on('input', onInput)
        //.on('edit', onTagEdit)
        //.on('invalid', onInvalidTag)
        //.on('click', onTagClick)
        //.on('dropdown:show', onDropdownShow)
        //.on('dropdown:hide', onDropdownHide);

        function onAddTag(e) {
            update_sku();
        }

        function onRemoveTag(e) {
            update_sku();
        }
        /*
        // tag remvoed callback
        
        // on character(s) added/removed (user is typing/deleting)
        function onInput(e) {
            console.log(e.detail);
            console.log("onInput: ", e.detail);
        }
        function onTagEdit(e) {
            console.log("onTagEdit: ", e.detail);
        }
        // invalid tag added callback
        function onInvalidTag(e) {
            console.log("onInvalidTag: ", e.detail);
        }
        // invalid tag added callback
        function onTagClick(e) {
            console.log(e.detail);
            console.log("onTagClick: ", e.detail);
        }
        function onDropdownShow(e) {
            console.log("onDropdownShow: ", e.detail)
        }
        function onDropdownHide(e) {
            console.log("onDropdownHide: ", e.detail)
        }
        */
    }

    var demo2 = function() {
        var input = document.getElementById('kt_tagify_2');
        tagify = new Tagify(input);
        tagify.on('add', onAddTag)
            .on('remove', onRemoveTag);
        //.on('input', onInput)
        //.on('edit', onTagEdit)
        //.on('invalid', onInvalidTag)
        //.on('click', onTagClick)
        //.on('dropdown:show', onDropdownShow)
        //.on('dropdown:hide', onDropdownHide);

        function onAddTag(e) {
            update_sku();
        }

        function onRemoveTag(e) {
            update_sku();
        }
    }

    return {
        // public functions
        init: function() {
            demo1();
            demo2();
        }
    };
}();

jQuery(document).ready(function() {
    KTTagifyDemos.init();
});