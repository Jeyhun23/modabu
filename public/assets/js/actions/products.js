"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: HOST_URL + '/api/products',
                        method: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        params: {
                            //type: 'form'
                        },
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            translate: {
                records: {
                    message: 'Yüklənir..',
                    processing: 'Gözləyin...',
                    noRecords: 'Məhsul tapılmadı',
                }
            },
            layout: {
                scroll: true,
                overlayColor: '#000000',
                type: 'loader',
                spinner: {
                    state: 'brand',
                }
            },
            search: {
                input: $("#kt_datatable_search_query"),
                key: "searchText"
            },
            columns: [{
                    field: "created_at",
                    title: "Tarix",
                    width: 80,
                    autoHide: false,
                }, {
                    field: "thumbnail",
                    title: "Şəkil",
                    sortable: false,
                    autoHide: false,
                    width: 80,
                    template: function(row) {
                        return '<img src="' + SITE_URL + '/storage' + row.thumbnail + '" width="80">';
                    }
                }, {
                    field: "title",
                    title: "Başlıq",
                    width: 200,
                    autoHide: false,
                    template: function(row) {
                        return '<a href="https://modabu.az/product-detail/' + row.slug + '">' + row.title + '</a>';
                    }

                }, {
                    field: "price",
                    title: "Qiymət",
                    width: 80,
                    autoHide: false,

                },
                {
                    field: "category",
                    title: "Kateqoriya",
                    width: 120,
                    autoHide: false,
                },
                {
                    field: "confirmed",
                    title: "Status",
                    width: 80,
                    autoHide: false,
                    template: function(row) {
                        var attribute = {
                            0: '',
                            1: 'checked="checked"'
                        };
                        return '<span class="switch switch-sm switch-outline switch-icon switch-success">\
                                    <label>\
                                        <input type="checkbox" ' + attribute[row.confirmed] + ' onchange="toggleStatus($(this))" data-url="' + HOST_URL + '/products/' + row.id + '"/>\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }
                },
                {
                    field: "Əməliyyatlar",
                    title: "Əməliyyatlar",
                    width: 145,
                    sortable: false,
                    autoHide: false,
                    template: function(row) {
                        return '<a href="' + HOST_URL + '/products/' + row.id + '" type="button" class="btn btn-primary">Redaktə et</a>\
                                <button type="button" class="btn btn-danger" onclick="deleteProduct(\'' + HOST_URL + '/products/' + row.id + '\')">Sil</button>';
                    }
                },
            ],
        });

        $('#kt_datatable_search_sort').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'sortBy');
        });
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableHtmlTableDemo.init();
});

async function toggleStatus(element) {
    var url = element.data('url');
    await fetch(url, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
};

function deleteProduct(url) {
    Swal.fire({
        title: "Are you sure?",
        text: "If you save, Product will be deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        reverseButtons: true
    }).then(async function(result) {
        if (result.value) {
            await fetch(url, {
                method: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).then(function(response) {
                if (response.status == 200) {
                    Swal.fire("Deleted", "", "success")
                    location.reload();
                } else {
                    Swal.fire("System error", "", "error")
                }
            });
        } else if (result.dismiss === "cancel") {
            Swal.fire("Cancelled", "", "error")
        }
    });
}