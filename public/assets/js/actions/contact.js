"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                saveState: {
                    cookie: false
                },
            },
            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },
            columns: [{
                    field: "Date",
                    title: "Date",
                    autoHide: false,
                }, {
                    field: "Name",
                    title: "Name",
                    autoHide: false,
                }, {
                    field: "Phone",
                    title: "Phone",
                    sortable: false,
                    autoHide: false,
                }, {
                    field: "Case",
                    title: "Case",
                    autoHide: false,
                },
                {
                    field: "Status",
                    title: "Status",
                    autoHide: false,
                },
                {
                    field: "Options",
                    title: "Options",
                    width: 150,
                    sortable: false,
                    autoHide: false,
                },
            ],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableHtmlTableDemo.init();
});