"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: HOST_URL + '/api/users',
                        method: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        params: {
                            //type: 'form'
                        },
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            translate: {
                records: {
                    message: 'Yüklənir..',
                    processing: 'Gözləyin...',
                    noRecords: 'İstifadəçi tapılmadı',
                }
            },
            layout: {
                scroll: true,
                overlayColor: '#000000',
                type: 'loader',
                spinner: {
                    state: 'brand',
                }
            },
            search: {
                input: $("#kt_datatable_search_query"),
                key: "searchText"
            },
            columns: [{
                    field: "id",
                    title: "#",
                    width: 50,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: "name",
                    title: "AD",
                    sortable: false,
                    autoHide: false,
                    width: 80
                },
                {
                    field: "email",
                    title: "email",
                    width: 200,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: "phone",
                    title: "Telefon",
                    width: 150,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: "orderCount",
                    title: "SifariŞ sayı",
                    width: 120,
                    autoHide: false,
                    sortable: false
                },
                {
                    field: "createdAt",
                    title: "Qey. Tarixi",
                    width: 120,
                    autoHide: false,
                    sortable: false
                },
            ],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableHtmlTableDemo.init();
});

async function toggleStatus(element) {
    var url = element.data('url');
    await fetch(url, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
};

function deleteUser(url) {
    Swal.fire({
        title: "Are you sure?",
        text: "If you save, User will be deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        reverseButtons: true
    }).then(async function(result) {
        if (result.value) {
            await fetch(url, {
                method: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).then(function(response) {
                if (response.status == 200) {
                    Swal.fire("Deleted", "", "success")
                    location.reload();
                } else {
                    Swal.fire("System error", "", "error")
                }
            });
        } else if (result.dismiss === "cancel") {
            Swal.fire("Cancelled", "", "error")
        }
    });
}
