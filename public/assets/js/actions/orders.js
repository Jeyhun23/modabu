"use strict";
// Class definition

var KTDatatableDataLocalDemo = (function() {
    // Private functions

    // demo initializer
    var demo = function() {
        var datatable = $("#kt_datatable").KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: HOST_URL + '/api/orders',
                        method: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        params: {
                            //type: 'form'
                        },
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false, // display/hide footer
            },
            sortable: true,
            pagination: true,
            search: {
                input: $("#searchQuery"),
                key: "searchQuery",
            },

            // columns definition
            columns: [{
                    field: "orderDate",
                    title: "Sifariş tarixi",
                    sortable: false,
                    autoHide: false,
                    width: 120,
                },
                {
                    field: "orderno",
                    title: "Sifariş nömrəsi",
                    sortable: false,
                    width: 100,
                    autoHide: false,
                },
                {
                    field: "fullname",
                    title: "Ad Soyad",
                    sortable: false,
                    width: 150,
                    autoHide: false,
                },
                {
                    field: "user.name",
                    title: "İstifadəçi",
                    sortable: false,
                    width: 150,
                    autoHide: false,
                },
                {
                    field: "productCount",
                    title: "Məhsul sayı",
                    sortable: false,
                    autoHide: false,
                    width: 120
                },
                {
                    field: 'options',
                    title: '',
                    sortable: false,
                    width: 225,
                    autoHide: false,
                    template: function(row) {
                        return '<button class="btn btn-info mr-2" data-toggle="modal" data-item="' + row.id + '" data-type="' + row.type + '" data-target="#orderDetail" id="viewOrder">Bax</button>' +
                            '<button class="btn btn-danger" onclick="deleteOrder(\'' + HOST_URL + '/orders/' + row.orderno + '/' + row.type + '\')">Sil</button>';

                    },
                }
            ]

        });

        $("#orderTypeQuery").on("change", function() {
            datatable.search($(this).val().toLowerCase(), "type");
        });

        $("#orderTypeQuery").selectpicker();
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
})();



$("#kt_datatable").on('click', "#viewOrder", function(e) {
    e.preventDefault();
    $("#result").html('...');
    var item = $(this).data('item');
    var type = $(this).data('type');
    fetch(HOST_URL + '/api/orders/' + item + '/' + type, {
        'method': "GET",
        'accept': 'application/json',
    }).then(function(response) {
        if (response.status == 200) {
            return response.json();
        }
    }).then(function(res) {
        $("#result").html(res.data);
    });
});

$("#orderDetail").on('click', "#changeStatus", function(e) {
    e.preventDefault();
    var item = $(this).data('item');
    var id = $(this).data('item')
    var type = $(this).data('type');
    var statusID = $(this).data('sid');
    var statusTitle = $(this).data('title');
    console.log(statusID);
    $("#itemID").val(id);
    $("#orderType").val(type);
    var select = document.getElementById('statusSelect');
    var opts = select.options;
    for (var opt, j = 0; opt = opts[j]; j++) {
        if (opt.value == statusID) {
            select.selectedIndex = j;
            break;
        }
    }
    $("#statusSelect").parent().find(".filter-option-inner-inner").html(statusTitle);
});

$("#orderStatusForm").submit(function(e) {
    e.preventDefault();
    let form = $(this);
    var item = form.find("#itemID").val();
    var type = form.find("#orderType").val();
    var status = form.find('#statusSelect').val();
    fetch(form.attr('action'), {
        method: form.attr('method'),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
        },
        body: new URLSearchParams({
            item: item,
            type: type,
            status: status
        })
    });
    $("#statusModal").hide();
    location.reload();
});



function removeLastComma(strng) {
    var n = strng.lastIndexOf(",");
    var a = strng.substring(0, n)
    return a;
}

function deleteOrder(url) {
    Swal.fire({
        title: "Are you sure?",
        text: "If you save, Service will be deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        reverseButtons: true
    }).then(async function(result) {
        if (result.value) {
            await fetch(url, {
                method: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).then(function(response) {
                if (response.status == 200) {
                    Swal.fire("Deleted", "", "success")
                    location.reload();
                } else {
                    Swal.fire("System error", "", "error")
                }
            });
        } else if (result.dismiss === "cancel") {
            Swal.fire("Cancelled", "", "error")
        }
    });
}

function toggleStatus(element) {
    var url = element.data('url');
    var item = element.data('item');
    var type = element.data('type');
    fetch(url, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
        },
        body: new URLSearchParams({
            id: item,
            type: type
        })
    });
};

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});