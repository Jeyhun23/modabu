$(document).ready(function () {
	// date select
	$(function () {
		$('input[name="daterange"]').daterangepicker({
				opens: "left",
			},
			function (start, end, label) {
				console.log(
					"A new date selection was made: " +
					start.format("YYYY-MM-DD") +
					" to " +
					end.format("YYYY-MM-DD")
				);
			}
		);

		$(".dropdown-header").click(function () {
			$(".dropdown-body").slideToggle();
		});
	});
	// login password
	$(".toggle-password").click(function () {
		$(this).toggleClass("active");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
	// selectbox
	var selectHolder = $(".selectHolder");
	selectHolder.each(function () {
		var currentSelectHolder = $(this);
		var select = currentSelectHolder.find("select");
		var options = select.children();
		var selected_option = select.find("option:checked");

		// SELECT HEADER
		var selectHeader = $("<h5></h5>");
		selectHeader.addClass("select_header");
		selectHeader.html(selected_option.html());
		currentSelectHolder.append(selectHeader);
		// SELECT BODY HOLDER
		var selectBodyHolder = $("<div></div>");
		selectBodyHolder.addClass("select_body_holder");
		currentSelectHolder.append(selectBodyHolder);
		// SELECT BODY
		var selectBody = $("<ul></ul>");
		selectBody.addClass("select_body");
		options.each(function () {
			var option_text = $(this).html();
			var value = $(this).attr("value");
			var disabled = $(this).attr("disabled");
			var li = $("<li></li>");
			li.append(option_text);
			li.attr("data-value", value);
			if (disabled) {
				li.attr("data-disabled", "true");
			} else {
				li.attr("data-disabled", "false");
			}
			selectBody.append(li);
		});
		selectBodyHolder.append(selectBody);
	});
	// CLICK SELECT HEADER
	$(document).on("click", ".select_header", function () {
		$(".selectHolder .select_header").not($(this)).removeClass("active");
		$(".selectHolder .select_body_holder")
			.not($(this).next(0))
			.removeClass("active");
		$(this).toggleClass("active");
		$(this).next(0).toggleClass("active");
	});
	// CLICK SELECT BODY ITEM
	$(document).on("click", ".select_body li", function () {
		var select = $(this).parents(".select_body_holder").prevAll().eq(1);
		if (!$(this).data("disabled")) {
			select.val($(this).data("value")).trigger("change");
			$(this)
				.parents(".select_body_holder")
				.prev()
				.html($(this).html())
				.removeClass("active");
			$(this).parents(".select_body_holder").removeClass("active");
		}
	});

	$(document).on("click", function (e) {
		var target = $(e.target);
		if (
			!(
				target.hasClass("selectHolder") ||
				target.parents(".selectHolder").length
			)
		) {
			$(".selectHolder").children().removeClass("active");
		}
	});

	approval = () => {
		$(".popup-1").addClass("active");
		$("body").addClass("active");
		console.log(".popup-1");
	};

	save = () => {
		$(".popup-2").addClass("active");
		$("body").addClass("active");
	};
	successSave = () => {
		$(".popup-3").addClass("active");
		$("body").addClass("active");
	};
	deletepop = () => {
		$(".popup-4").addClass("active");
		$("body").addClass("active");
	};
	deleted = () => {
		$(".popup-5").addClass("active");
		$("body").addClass("active");
	};
	createStory = () => {
		$(".popup-6").addClass("active");
		$("body").addClass("active");
	};

	$(".popup-close").click(function () {
		$(".popup").removeClass("active");
		$("body").removeClass("active");
	});
	//   $(".layer-popup").click(function(){
	//     $(".popup").removeClass("active");
	//     $("body").removeClass("active");
	//   })

	$("#left-menu-slide li a").click(function () {
		var target = $(this).attr("href");
		$(this).addClass("active");
		$("#left-menu-slide li a").not($(this)).removeClass("active");

		$("html, body")
			.stop()
			.animate({
					scrollTop: $(target).offset().top,
				},
				300,
				function () {
					location.hash = target;
				}
			);
	});

	$('.checkbox-table').on("change", function () {
		if ($('.checkbox-table').is(":checked")) {
			$(".delete-search-result").addClass("active");
		} else {
			$(".delete-search-result").removeClass("active");
		}
	});


	$("#checkParent").click(function () {
		$(".checkbox-table:checkbox").not(this).prop("checked", this.checked);
	});

// new


	 //  tabs

            $("#tabs li a:not(:first),#tabs-2 li a:not(:first),#tabs-3 li a:not(:first)").addClass("inactive");
			
            $(".tabs-container").hide();
            $(".tabs-container:first").show();
            $("#tabs li a,#tabs-2 li a,#tabs-3 li a").click(function () {
				var t = $(this).attr("href");
                $("#tabs li a, #tabs-2 li a,#tabs-3 li a").addClass("inactive");
                $(this).removeClass("inactive");
                $(".tabs-container").hide();
                $(t).fadeIn("slow");
                return false;
            });
			
			if ($(this).hasClass("inactive")) {
				$("#tabs li a,#tabs-2 li a,#tabs-3 li a").addClass("inactive");
				$(this).removeClass("inactive");
				$(".tabs-container").hide();
				$(t).fadeIn("slow");
			}
			

			$( "#datepciker-1, #datepciker-2, #datepciker-3" ).datepicker();


// new
});