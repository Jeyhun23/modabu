<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'model_type',
        'model_id',
        'name',
        'brand',
        'price',
        'image',
        'quantity',
        'attributes',
    ];

    protected $append = [
        //'product_discount',
        //'product_discount_type'
    ];

    //public function setImageAttribute($value)
    //{
    //    $this->attributes['image'] = strtolower($value);
    //}

    public function product()
    {
        return $this->belongsTo(Product::class, 'model_id');
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
