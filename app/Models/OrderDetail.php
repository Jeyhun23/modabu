<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'kuryer_reason_id',
        'reason_id',
        'order_id',
        'product_id',
        'price',
        'discount',
        'quantity',
        'attributes',
        'status',
        'stock',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function kuryer_detail()
    {
        return $this->belongsTo(User::class, 'kuryer_id', 'id');
    }

    public function parentStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'status');
    }

    public function getAttributesAttribute($attributes)
    {
        return json_decode($attributes, true);
    }
}
