<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;

class Sms extends Model
{
    use HasFactory;

    public $table = 'sms';

    const ATL_USER = "modabusms";
    const ATL_PASS = "eR4!zLza";
    const ATL_URL = "https://sms.atltech.az:7443/bulksms/api";
    const ATL_TITLE = "MODABU.AZ";

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'phone',
        'message',
        'response',
        'created_at',
        'updated_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
