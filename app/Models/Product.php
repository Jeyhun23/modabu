<?php

namespace App\Models;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Product extends Model
{
    use HasFactory;
    use QueryCacheable;

    protected $cacheFor = 180;

    protected static $flushCacheOnUpdate = true;

    protected $fillable = [
        'user_id',
        'category_id',
        'brand_id',
        'ref_id',
        'title',
        'thumbnail',
        'description',
        'slug',
        'meta',
        'price',
        'qty',
        'discount',
        'discount_type',
        'featured',
        'confirmed',
    ];

    public function related()
    {
        return $this->hasMany(self::class, 'id', 'id')->where(['category_id' => $this->category_id, 'confirmed' => true]);
    }

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function colors()
    {
        return $this->hasMany(ProductColor::class)->with(['color']);
    }

    public function images()
    {
        return $this->hasMany(File::class, 'p_id')->where('type', 'product');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function getName()
    {
        return $this->title;
    }

    public function hasDiscount(): bool
    {
        return (bool) $this->discount;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function variations()
    {
        return $this->hasMany(Variation::class)->orderBy('id', 'asc');
    }

    public function shelf()
    {
        return $this->belongsTo(Refler::class, 'ref_id');
    }

    public function combines()
    {
        $array = [$this->id];
        $combines = ProductCombine::where(['source_product' => $this->id])->orWhere(['target_product' => $this->id])->select('source_product', 'target_product')->get();
        foreach ($combines as $combine) {
            if ($combine->source_product == $this->id) {
                array_push($array, $combine->target_product);
            } else {
                array_push($array, $combine->source_product);
            }
        }
        $array = array_unique($array);
        return self::with(['colors', 'attributes', 'brand', 'images', 'leftCombines', 'rightCombines'])
            ->whereIn('id', $array)
            ->get()
            ->transform(function (Product $product) {
                return (new ProductResource($product));
            });
    }

    public function leftCombines()
    {
        return $this->hasMany(ProductCombine::class, 'source_product');
    }

    public function rightCombines()
    {
        return $this->hasMany(ProductCombine::class, 'target_product');
    }
}
