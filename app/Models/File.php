<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class File extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'files';

    protected $fillable = [
        'p_id',
        'type',
        'name',
        'path',
        'mime',
        'file_size',
        'deleted_at',
    ];

    protected $appends = ['full_url'];

    public function getFullUrlAttribute()
    {
        //return Storage::disk('products')->url($this->getAttribute('url'));
    }

    public static function storeFile(string $path, $file): string
    {
        $name = Str::random(64) . "." . $file->getClientOriginalExtension();
        Storage::disk('local')->putFileAs($path, $file, $name);
        return $name;
    }

    public static function resiveAndstoreFile($image, string $path, $ext, $width = 300, $height = 300): string
    {
        $name = Str::random(64) . "." . $ext;
        $thumb = Image::make($image)->fit($width, $height)->encode($ext);
        $path = $image->storeAs('public/stories', $name);
        return $name;
    }

    public static function deleteFile(string $full_path)
    {
        Log::info($full_path);
        \Illuminate\Support\Facades\File::delete($full_path);
    }
}
