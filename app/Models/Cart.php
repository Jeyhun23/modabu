<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'coupon_id',
        'discount',
        'total',
        'payable',
        'shipping_charges',
    ];

    /**
     * Get the items of the cart.
     */
    public function items()
    {
        return $this->hasMany(CartItem::class);
    }
}