<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCombine extends Model
{
    use HasFactory;

    protected $fillable = [
       'source_product', 'target_product'
    ];


    public function product(){
        return $this->belongsTo(Product::class, 'target_product');
    }
}
