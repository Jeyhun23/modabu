<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    use HasFactory;

    public $table = "variations";

    protected $fillable = [
        'product_id',
        'color_id',
        'attribute_id',
        'variation_parent',
        'variation_child',
        'sku',
        'price',
        'qty',
    ];

    public function color()
    {
        return $this->belongsTo(Color::class);
    }
}