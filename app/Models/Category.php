<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->with(['children']);
    }

    public function products()
    {
        return $this->hasMany(Product::class)->where('products.confirmed', true)->limit(4);
    }

    public function children2()
    {
        return $this->hasMany(self::class, 'parent_id')->with(['children' => function ($query) {
            $query->select('id', 'parent_id', 'name');
        }]);
    }
}
