<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderForm extends Model
{
    use HasFactory;
    protected $fillable = [
        'orderno',
        'product_id',
        'first_name',
        'last_name',
        'phone',
        'city',
        'address',
        'status',
        'total',
        'discount',
        'payed',
        'quantity',
        'attributes',
        'status',
        'stock',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function parentStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'status');
    }

    public function getAttributesAttribute($attributes)
    {
        return json_decode($attributes, true);
    }
}