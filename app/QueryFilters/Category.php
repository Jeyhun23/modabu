<?php


namespace App\QueryFilters;


class Category extends Filter
{

    protected function applyFilters($builder)
    {
        $categoryId = request($this->filterName());
        if ($categoryId) {
            return $builder->whereHas('category',function ($query) use ($categoryId) {
                $query->where('category_id',$categoryId);
            });
        }

        return $builder;
    }
}
