<?php


namespace App\QueryFilters;


class Code extends Filter
{
    protected function applyFilters($builder)
    {
        $code = request($this->filterName());
        if ($code) {
            return $builder->where('code', 'like', '%' . $code . '%');
        }

        return $builder;
    }
}
