<?php


namespace App\QueryFilters;


class Brand extends Filter
{

    protected function applyFilters($builder)
    {
        $brandId = request($this->filterName());
        if ($brandId) {
            return $builder->whereHas('brand',function ($query) use ($brandId) {
                $query->where('brand_id',$brandId);
            });
        }

        return $builder;
    }
}
