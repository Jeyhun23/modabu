<?php


namespace App\QueryFilters;


class Shelf extends Filter
{

    protected function applyFilters($builder)
    {
        $shelfId = request($this->filterName());
        if ($shelfId) {
            return $builder->whereHas('shelf',function ($query) use ($shelfId) {
                $query->where('ref_id',$shelfId);
            });
        }

        return $builder;
    }
}
