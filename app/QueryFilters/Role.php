<?php


namespace App\QueryFilters;


class Role extends Filter
{
    protected function applyFilters($builder)
    {
        $role = request($this->filterName());
        if ($role) {
            return $builder->whereHas('roles', function ($q) use ($role) {
                $q->where('role_id', $role);
            });
        }

        return $builder;
    }
}
