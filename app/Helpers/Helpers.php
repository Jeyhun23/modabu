<?php

use App\Settings;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

//highlights the selected navigation on admin panel
if (!function_exists('areActiveRoutes')) {
    function areActiveRoutes(array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) {
                return $output;
            }

        }

        return false;
    }
}

//highlights the selected navigation on admin panel
if (!function_exists('combineProducts')) {
    function combineProducts(array $products)
    {
        sort($products);
        $products = [$products];

        $result = array();

        foreach ($products as $group) {
            $lastIdx = count($group) - 1;
            $startIdx = 1;
            foreach ($group as $member) {
                for ($pos = $startIdx; $pos <= $lastIdx; $pos++) {
                    $result[] = array(
                        'source' => $member,
                        'target' => $group[$pos]
                    );
                }
                $startIdx++;
            }
        }
        return $result;
    }
}

/**
 * Get pagination limit from query string
 * @returns int
 */
if (!function_exists('getPaginationLimit')) {
    function getPaginationLimit($newLimit = null, $queryStringName = 'limit')
    {
        $defaultLimit = is_null($newLimit) ? config('admin.limit') : (int)$newLimit;
        return request()->has($queryStringName) ? (int)request($queryStringName) : $defaultLimit;
    }
}

if (!function_exists('filterPhone')) {
    function filterPhone($phone = '')
    {
        if ($phone != '') {
            return $phone = preg_replace('/[^0-9]/', '', $phone);
        }
        return false;
    }
}

if (!function_exists('formatDate')) {
    function formatDate($format = 'd-m-Y', $date)
    {
        //return date($format, strtotime($date));
        $date = Carbon::parse($date);
        return $date->format($format);
    }
}

/**
 * get settings value
 * @return Response
 */

if (!function_exists('firstImage')) {
    function firstImage($images = array())
    {
        if ($images && count($images)) {
            foreach ($images as $image) {
                return asset($image->url);
            }
        }
    }
}

if (!function_exists('truncate')) {
    function truncate($string, $length = 100, $stopanywhere = false)
    {
        //truncates a string to a certain char length, stopping on a word if not specified otherwise.
        if (strlen($string) > $length) {
            //limit hit!
            $string = substr($string, 0, ($length - 3));
            if ($stopanywhere) {
                $string .= '...';
            } else {
                //stop on a word.
                $string = substr($string, 0, strrpos($string, ' ')) . '...';
            }
        }
        return $string;
    }
}

/**
 * Open Translation File
 * @return Response
 */
function openJSONFile($code)
{
    $jsonString = [];
    if (File::exists(base_path('resources/lang/' . $code . '.json'))) {
        $jsonString = file_get_contents(base_path('resources/lang/' . $code . '.json'));
        $jsonString = json_decode($jsonString, true);
    }
    return $jsonString;
}

/**
 * Save JSON File
 * @return Response
 */
function saveJSONFile($code, $data)
{
    ksort($data);
    $jsonData = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    file_put_contents(base_path('resources/lang/' . $code . '.json'), stripslashes($jsonData));
}

/**
 * Add Translations to Databse
 */
if (!function_exists('translate')) {
    function translate($word)
    {
        return __($word);
    }
}

if (!function_exists('combinations')) {
    function combinations($arrays)
    {
        $result = array(array());
        foreach ($arrays as $property => $property_values) {
            $tmp = array();
            foreach ($result as $result_item) {
                foreach ($property_values as $property_value) {
                    $tmp[] = array_merge($result_item, array($property => $property_value));
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}

if (!function_exists('encode_hash')) {
    function encode_hash($data)
    {
        $en = new \Illuminate\Encryption\Encrypter(config('app.aes_key256'), config('app.cipher'));
        return $en->encrypt($data);
    }
}

if (!function_exists('decode_hash')) {
    function decode_hash($data)
    {
        $en = new \Illuminate\Encryption\Encrypter(config('app.aes_key256'), config('app.cipher'));
        return $en->decrypt($data);
    }
}
