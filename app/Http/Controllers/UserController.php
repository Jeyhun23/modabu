<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserContactInformationRequest;
use App\Http\Requests\UserUpdateSettingsRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use ApiResponser;

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function getUser(Request $request)
    {
        $user = User::find($request->user()->id);
        return new UserResource($user);
    }

    public function updateContactInformations(UserContactInformationRequest $request)
    {
        $user = User::find($request->user()->id);
        try {
            $user->name = $request->get('firstName') . ' ' . $request->get('lastName');
            $user->phone = $request->get('phone');
            $user->city = $request->get('city');
            $user->address = $request->get('address');
            $user->save();
            return new UserResource($user);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    public function updateSettings(UserUpdateSettingsRequest $request)
    {
        try {
            $request->user()->fill([
                'name' => $request->input('firstname') . ' ' . $request->input('lastname'),
                'email' => $request->get('email'),
            ])->save();
            if ($request->get('password')) {
                $request->user()->fill([
                    'password' => Hash::make($request->get('password')),
                ])->save();
            }
            return new UserResource($request->user());
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    public function getOrders(Request $request)
    {
        $limit = 5;
        $user = User::find($request->user()->id);
        return new OrderCollection($user->orders()->orderBy('created_at', 'desc')->with(['detail'])->paginate($limit));
    }
}