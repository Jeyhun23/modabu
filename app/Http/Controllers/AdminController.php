<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\AdminOrderCollection;
use App\Http\Resources\AdminProductCollection;
use App\Http\Resources\OrderFormCollection;
use App\Http\Resources\UserCollection;
use App\Models\Address;
use App\Models\Attribute;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\File;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderForm;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeVariation;
use App\Models\ProductColor;
use App\Models\User;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    use ApiResponser;

    public function index()
    {
    }

    /** Orders
     * @return view
     */
    public function orders()
    {
        View::share('page_title', "Sifarişlər");
        View::share('page_breadcrumb', "Sifarişlər");
        $statuses = OrderStatus::all();
        return view("admin.pages.orders.index", compact('statuses'));
    }
    /** Orders
     * @return json
     */
    public function ordersList(Request $request)
    {
        //return $request->all();
        $pagination = $request->get('pagination');
        $query = $request->get('query');
        $type = $query['type'] ?? null;
        $searchQuery = $query['searchQuery'] ?? null;
        $limit = 10;
        if (isset($pagination) && $pagination['perpage']) {
            $limit = (int) $pagination['perpage'];
        }
        if (isset($pagination) && $pagination['page']) {
            $request->merge(['page' => $pagination['page']]);
        }
        if ($type && $type == 'form') {
            $orders = OrderForm::with(['product'])
                ->orderBy('created_at', 'desc');
            if ($searchQuery) {
                $orders->where(function ($query) use ($searchQuery) {
                    return $query->where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%{$searchQuery}%");
                });
            }
            $orders = $orders->paginate($limit);
            return new OrderFormCollection($orders);
        }
        if ($type && $type == 'cart-no') {
            $orders = Order::with(['detail.product'])
                ->whereNull('user_id')
                ->orderBy('created_at', 'desc')
                ->orderBy('status', 'asc')
                ->withCount('detail as productCount');
            if ($searchQuery) {
                $orders->where('address', 'like', "%{$searchQuery}%");
            }
            $orders = $orders->paginate($limit);
            return new AdminOrderCollection($orders);
        }
        $orders = Order::with(['detail.product'])
            ->whereNotNull('user_id')
            ->orderBy('created_at', 'desc')
            ->orderBy('status', 'asc')
            ->withCount('detail as productCount');
        if ($searchQuery) {
            $orders->where(function ($q) use ($searchQuery) {
                return $q->whereHas('user', function ($query) use ($searchQuery) {
                    return $query->where('name', 'like', "%{$searchQuery}%")
                        ->orWhere('address', 'like', "%{$searchQuery}%")
                        ->orWhere('email', 'like', "%{$searchQuery}%");
                });
            });
        }
        $orders = $orders->paginate($limit);
        return new AdminOrderCollection($orders);
    }

    public function ordersStatus(Request $request)
    {
        $type = $request->type;
        if ($type != "order") {
            $order = OrderForm::findOrFail($request->item);
        } else {
            $order = OrderDetail::findOrFail($request->item);
        }
        if ($order) {
            $order->status = (int) $request->get('status') ?: 0;
            $order->save();
            return back()->with(['status' => 'success']);
        }
        return back()->withErrors(['status' => 'error']);
    }

    public function ordersDelete(int $orderno, string $type)
    {
        if ($type == "form") {
            $order = OrderForm::where(['orderno' => $orderno])->firstOrFail();
        } else {
            $order = Order::with(['detail'])->where(['orderno' => $orderno])->firstOrFail();
        }
        $order->delete();
        return $this->sendResponse(null, 'success');
    }

    public function updateStock(Request $request)
    {
        //return $request->all();
        $type = $request->type;
        if ($type == "order") {
            $order = OrderDetail::find($request->id);
        } else {
            $order = OrderForm::find($request->id);
        }
        $stock = $order->stock;
        if ($type == "order") {
            $attrs = $order->attributes;
            if ($attrs) {
                $color = Color::firstWhere('name', $attrs['color']);
                $size = $attrs['size'];
                if ($color && $size) {
                    $sku = $color->id . 'xx' . 1 . 'xx' . $size;
                    $variation = Variation::where([
                        'product_id' => $order->product_id,
                        'color_id' => $color->id,
                    ])->where('sku', 'like', "%{$sku}%")->first();
                    if ($variation) {
                        $variation->update(['qty' => $stock ? ($variation->qty - $order->quantity) : ($variation->qty + $order->quantity)]);
                    }
                }
            }
        } else {
            $attrs = $order->attributes;
            if ($attrs) {
                $color = Color::firstWhere('name', $attrs['color']);
                $size = $attrs['size'];
                if ($color && $size) {
                    $sku = $color->id . 'xx' . 1 . 'xx' . $size;
                    $variation = Variation::where([
                        'product_id' => $order->product_id,
                        'color_id' => $color->id,
                    ])->where('sku', 'like', "%{$sku}%")->first();
                    if ($variation) {
                        $qty = $order->quantity ?: 1;
                        $variation->update(['qty' => $stock ? ($variation->qty - $qty) : ($variation->qty + $qty)]);
                    }
                }
            }
        }
        return $order->update(['stock' => DB::raw("(1 - stock)")]);
    }

    public function ordersInfo(int $order, $type = "order")
    {
        if ($type == "order") {
            $order = Order::find($order);
        } else {
            $order = OrderForm::find($order);
        }
        $view = view('admin.pages.orders.detail', compact('order', 'type'))->render();
        return response()->json(['data' => $view]);
    }
    /** Products
     * @return view
     */
    public function products()
    {
        //$products = Product::with('brand', 'category')->get();
        View::share('page_title', "Məhsullar");
        View::share('page_breadcrumb', "Məhsullar");
        return view("admin.pages.products.index");
    }

    public function productsList(Request $request)
    {
        $pagination = $request->get('pagination');
        $query = $request->get('query');
        $limit = 10;
        if (isset($pagination) && $pagination['perpage']) {
            $limit = (int) $pagination['perpage'];
        }
        if (isset($pagination) && $pagination['page']) {
            $request->merge(['page' => $pagination['page']]);
        }
        $products = Product::orderBy('created_at', 'desc')->dontCache();
        if (isset($query) && isset($query['searchText'])) {
            $products->where(function ($q) use ($query) {
                $q->where('title', 'like', '%' . $query['searchText'] . '%');
                $q->orWhere('description', 'like', '%' . $query['searchText'] . '%');
            });
        }
        if (isset($query) && isset($query['sortBy']) && $query['sortBy'] == "discount") {
            $products->where(function ($q) use ($query) {
                $q->where('discount', '!=', 0)->whereNotNull('discount');;
            });
        }
        $products = $products->paginate($limit);
        return new AdminProductCollection($products);
    }

    public function productsCreate()
    {
        $categories = Category::with(['children'])->withCount(['children'])->where(['parent_id' => 0])->get();
        $brands = Brand::all();
        $colors = Color::all();
        $attributes = Attribute::all();
        View::share('page_title', "Products");
        View::share('page_breadcrumb', "Add new Product");
        return view("admin.pages.products.create", compact('categories', 'brands', 'colors', 'attributes'));
    }

    public function productsStore(ProductStoreRequest $request)
    {
        //return response()->json([$request->all()]);
        $colors = $request->get('colors');
        $images = $request->get('images');
        $attributes = $request->get('attributes');
        $variations = $request->get('variations');
        $prices = $request->get('prices');
        $qtys = $request->get('qtys');

        $meta = $request->get('meta');
        $product = Product::query()->create(['user_id' => auth()->user()->id, 'confirmed' => false]);
        $path = null;
        try {
            if ($request->file("thumbnail")) {
                $path = File::storeFile('public/products/thumbnail', $request->file("thumbnail"));
            }
            $product->thumbnail = '/products/thumbnail/' . $path;
            $product->category_id = $request->get('category');
            $product->brand_id = $request->get('brand');
            $product->title = $request->get('title');
            $product->description = $request->get('description');
            $product->meta = json_encode($meta);
            $product->price = $request->get('price');
            $product->qty = $request->get('qty');
            $product->discount = $request->get('discount');
            $product->discount_type = $request->get('discount_type');
            $product->featured = 0;
            $product->save();
            $product->slug = Str::slug($request->get('title')) . $product->id;
            $product->save();

            if ($images) {
                $images = explode(',', $images);
                File::whereIn('id', $images)->update([
                    'p_id' => $product->id,
                ]);
            }
            $this->resize($product);

            if (is_array($colors) && count($colors) > 0) {
                foreach ($colors as $color) {
                    ProductColor::query()->insert([
                        'product_id' => $product->id,
                        'color_id' => $color,
                    ]);
                }
            }
            if (is_array($attributes) && count($attributes) > 0) {
                foreach ($attributes as $attribute) {
                    $pa = ProductAttribute::query()->insertGetId([
                        'product_id' => $product->id,
                        'attribute_id' => $attribute,
                    ]);
                    $vars = json_decode($variations[$attribute]);
                    if (count($vars) > 0) {
                        foreach ($vars as $var) {
                            ProductAttributeVariation::query()->insert([
                                'product_attribute_id' => $pa,
                                'name' => strtoupper($var->value),
                            ]);
                        }
                    }
                }
            }
            $__v = array();
            if ($colors && count($colors) > 0) {
                foreach ($colors as $i => $color) {
                    $firstAttribute = $attributes[0] ?? null;
                    $fv = json_decode($variations[$firstAttribute]);
                    if (isset($firstAttribute) && count($fv) > 0) {
                        foreach ($fv as $key => $firstVariation) {
                            if ($attributes && count($attributes) > 1) {
                                $secondAttribute = $attributes[1];
                                $sv = json_decode($variations[$secondAttribute]);
                                foreach ($sv as $kk => $secondVariation) {
                                    array_push($__v, [
                                        'color' => (int) $color,
                                        'attribute' => (int) $firstAttribute,
                                        'parent' => $firstVariation->value,
                                        'child' => $secondVariation->value,
                                        'price' => (float) $prices[$color][$firstAttribute][$key][$kk],
                                        'qty' => (int) $qtys[$color][$firstAttribute][$key][$kk],
                                    ]);
                                }
                            } else {
                                array_push($__v, [
                                    'color' => (int) $color,
                                    'parent' => $firstVariation->value,
                                    'attribute' => (int) $firstAttribute,
                                    'child' => 0,
                                    'price' => (float) $prices[$color][$firstAttribute][$key],
                                    'qty' => (int) $qtys[$color][$firstAttribute][$key],
                                ]);
                            }
                        }
                    }
                }
            } else {
                //comment
            }
            if (count($__v) >= 1) {
                foreach ($__v as $v) {
                    Variation::query()->insert([
                        'product_id' => $product->id,
                        'color_id' => $v['color'],
                        'attribute_id' => $v['attribute'],
                        'variation_parent' => $v['parent'],
                        'variation_child' => $v['child'],
                        'sku' => $v['color'] . 'xx' . $v['attribute'] . 'xx' . $v['parent'] . 'xx' . $v['child'],
                        'price' => $v['price'] ?? $request->get('price'),
                        'qty' => $v['qty'],
                    ]);
                }
            }

            return $this->sendResponse(null, 'success', 201);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage());
        }
    }

    public function productsEdit($id)
    {
        //return false;
        $categories = Category::with(['children'])->withCount(['children'])->where(['parent_id' => 0])->get();
        $brands = Brand::all();
        $colors = Color::all();
        $attributes = Attribute::all();
        $product = Product::with(['colors', 'attributes', 'images', 'variations'])->findOrFail($id);
        $productColors = $product->colors->pluck(['color_id'])->toArray();
        $productAttributes = $product->attributes->pluck(['attribute_id'])->toArray();
        View::share('page_title', "Products");
        View::share('page_breadcrumb', "Edit Product");
        return view("admin.pages.products.edit", compact('categories', 'brands', 'colors', 'attributes', 'product', 'productColors', 'productAttributes'));
    }

    public function productsUpdate($id, ProductUpdateRequest $request)
    {
        //return $request->all();
        $colors = $request->get('colors');
        $images = $request->get('images');
        $attributes = $request->get('attributes');
        $variations = $request->get('variations');
        $prices = $request->get('prices');
        $qtys = $request->get('qtys');
        $meta = $request->get('meta');

        $product = Product::firstWhere(['id' => $id]);
        try {
            if ($request->file("thumbnail") && Storage::disk('local')->exists($product->thumbnail)) {
                Storage::disk('local')->delete($product->thumbnail);
            }
            if ($request->file("thumbnail")) {
                $product->thumbnail = '/products/thumbnail/' . File::storeFile('public/products/thumbnail', $request->file("thumbnail"));
            }
            $product->category_id = $request->get('category');
            $product->brand_id = $request->get('brand');
            $product->title = $request->get('title');
            $product->description = $request->get('description');
            $product->meta = json_encode($meta);
            $product->price = $request->get('price');
            $product->qty = $request->get('qty');
            $product->discount = $request->get('discount');
            $product->discount_type = $request->get('discount_type');
            $product->featured = 0;
            $product->slug = Str::slug($request->get('title')) . $product->id;
            $product->save();
            if ($images) {
                $images = explode(',', $images);
                File::whereIn('id', $images)->update([
                    'p_id' => $product->id,
                ]);
            }

            $this->resize($product);
            $product->colors()->delete();
            $product->attributes()->delete();
            $product->variations()->delete();

            if (is_array($colors) && count($colors) > 0) {
                foreach ($colors as $color) {
                    ProductColor::query()->updateOrCreate([
                        'product_id' => $product->id,
                        'color_id' => $color,
                    ], [
                        'product_id' => $product->id,
                        'color_id' => $color,
                    ]);
                }
            }
            if (is_array($attributes) && count($attributes) > 0) {
                foreach ($attributes as $attribute) {
                    $pa = ProductAttribute::query()->updateOrCreate([
                        'product_id' => $product->id,
                        'attribute_id' => $attribute,
                    ], [
                        'product_id' => $product->id,
                        'attribute_id' => $attribute,
                    ]);
                    $vars = json_decode($variations[$attribute]);
                    if (count($vars) > 0) {
                        foreach ($vars as $var) {
                            ProductAttributeVariation::updateOrCreate([
                                'product_attribute_id' => $pa->id,
                                'name' => strtoupper($var->value),
                            ], [
                                'product_attribute_id' => $pa->id,
                                'name' => strtoupper($var->value),
                            ]);
                        }
                    }
                }
            }
            //return $prices;
            $__v = array();
            if ($colors && count($colors) > 0) {
                foreach ($colors as $color) {
                    $firstAttribute = $attributes[0] ?? null;
                    $fv = json_decode($variations[$firstAttribute]);
                    if (isset($firstAttribute) && count($fv) > 0) {
                        foreach ($fv as $key => $firstVariation) {
                            if ($attributes && count($attributes) > 1) {
                                $secondAttribute = $attributes[1];
                                $sv = json_decode($variations[$secondAttribute]);
                                foreach ($sv as $kk => $secondVariation) {
                                    array_push($__v, [
                                        'color' => (int) $color,
                                        'attribute' => (int) $firstAttribute,
                                        'parent' => $firstVariation->value,
                                        'child' => $secondVariation->value,
                                        'price' => (float) $prices[$color][$firstAttribute][$key][$kk],
                                        'qty' => (int) $qtys[$color][$firstAttribute][$key][$kk],
                                    ]);
                                }
                            } else {
                                array_push($__v, [
                                    'color' => (int) $color,
                                    'parent' => $firstVariation->value,
                                    'attribute' => (int) $firstAttribute,
                                    'child' => 0,
                                    'price' => (float) $prices[$color][$firstAttribute][$key],
                                    'qty' => (int) $qtys[$color][$firstAttribute][$key],
                                ]);
                            }
                        }
                    }
                }
            }
            //return $__v;
            if (count($__v) >= 1) {
                foreach ($__v as $v) {
                    Variation::query()->updateOrCreate([
                        'product_id' => $product->id,
                        'color_id' => $v['color'],
                        'attribute_id' => $v['attribute'],
                        'variation_parent' => $v['parent'],
                        'variation_child' => $v['child'],
                    ], [
                        'sku' => $v['color'] . 'xx' . $v['attribute'] . 'xx' . $v['parent'] . 'xx' . $v['child'],
                        'price' => $v['price'] ?? $request->get('price'),
                        'qty' => $v['qty'],
                    ]);
                }
            }

            return $this->sendResponse(null, 'success', 201);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage());
        }
    }

    public function productsStatus($id)
    {
        Product::query()
            ->where('id', $id)
            ->update(['confirmed' => DB::raw("(1 - confirmed)")]);

        return response()->json(['data' => translate('success')], 200);
    }

    public function productsDelete($id)
    {
        $product = Product::query()->with(['attributes.variations', 'images'])->findOrFail($id)->delete();
        return response()->json(['data' => 'success'], 200);
    }

    public function uploadFile($type, Request $request)
    {
        $user = $request->user();
        if ($request->hasFile('file')) {
            request()->validate([
                'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            ]);
            $file = $request->file('file');
            $size = $file->getSize();
            $path = File::storeFile('public/products', $file);
            if (Session::has('uid')) {
                $uuid = Session::get('uuid');
            } else {
                $uuid = Str::uuid();
                Session::put('uuid', $uuid);
            }
            $f = File::query()->create([
                'p_id' => null,
                'type' => $type,
                'name' => $file->getClientOriginalName(),
                'path' => 'products/' . $path,
                'mime' => $file->getMimeType(),
                'file_size' => $size,
            ]);
            return response()->json(['data' => $f->id]);
        }
        return response()->json(['data' => 'file not found'], 404);
    }

    public function addTuhumbnail()
    {
        return 0;
        $products = Product::all();
        foreach ($products as $product) {
            $thumbnail = $product->thumbnail;
            $exists = Storage::disk('local')->exists($thumbnail);
            if ($exists) {
                //Storage::copy('old/file.jpg', 'new/file.jpg');
                $f = File::query()->create([
                    'p_id' => $product->id,
                    'type' => 'product',
                    'name' => 'thumbnail',
                    'path' => $thumbnail,
                    'mime' => 'image/jpeg',
                    'file_size' => '11111',
                ]);
            }
        }
    }

    public function resize(Product $product)
    {
        $k = 0;

        $products = Product::get();
        //foreach ($products as $product) {
        $thumbnail = $product->thumbnail;
        $exists = Storage::disk('local')->exists('/public' . $thumbnail);
        if ($exists) {
            $k++;
            $fileName = explode("/", $thumbnail);
            $fileName = explode(".", end($fileName));

            $size = Storage::disk('local')->size('/public' . $thumbnail);
            $file = Storage::disk('local')->get('/public' . $thumbnail);
            $img = Image::make($file)->resize(264, 362, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save(storage_path('app/public/products/264x362/') . $fileName[0] . '.' . $fileName[1], 90);
            echo "Before: " . $size . " Current: " . $img->filesize() . "<br>";
        }
        //}

        //$products = Product::where('id', '>', $down)->where('id', '<=', $up)->get();
        //foreach ($products as $product) {
        $images = $product->images;
        foreach ($images as $image) {
            $exists = Storage::disk('local')->exists('/public/' . $image->path);
            if ($exists) {
                $k++;
                $fileName = explode("/", $image->path);
                $fileName = explode(".", end($fileName));

                $size = Storage::disk('local')->size('/public/' . $image->path);
                $file = Storage::disk('local')->get('/public/' . $image->path);
                $img = Image::make($file)->resize(373, 560, function ($constraint) {
                    $constraint->aspectRatio();
                    //$constraint->upsize();
                });
                $img->save(storage_path('app/public/products/373x560/') . $fileName[0] . '.' . $fileName[1], 100);
                echo "Before: " . $size . " Current: " . $img->filesize() . "<br>";
            }
        }
        //}
        return true;
    }

    public function variations()
    {
        //return 0;
        $products = Product::get();
        foreach ($products as $product) {
            $colors = $product->colors;
            foreach ($colors as $color) {
                $attributes = $product->attributes();
                $attributes1 = $product->attributes()->first();
                $attributes2 = $product->attributes()->skip(1)->first();
                if ($attributes->count() > 1) {
                    $variations1 = $attributes1->variations;
                    $variations2 = $attributes2->variations;
                    foreach ($variations1 as $v1) {
                        foreach ($variations2 as $v2) {
                            //echo $color->color_id . 'xx' . $attributes1->attribute_id . 'xx' . $v1->name . 'xx' . $v2->name . '<br>';
                            Variation::query()->insert([
                                'product_id' => $product->id,
                                'color_id' => $color->color_id,
                                'attribute_id' => $attributes1->attribute_id,
                                'variation_parent' => $v1->name,
                                'variation_child' => null,
                                'sku' => $color->color_id . 'xx' . $attributes1->attribute_id . 'xx' . $v1->name . 'xx' . $v2->name,
                                'price' => (float) $product->price,
                                'qty' => (int) $product->qty,
                            ]);
                        }
                    }
                } else {
                    $variations1 = $attributes1->variations;
                    foreach ($variations1 as $v1) {
                        //echo $color->color_id . 'xx' . $attributes1->attribute_id . 'xx' . $v1->name . '<br>';
                        Variation::query()->insert([
                            'product_id' => $product->id,
                            'color_id' => $color->color_id,
                            'attribute_id' => $attributes1->attribute_id,
                            'variation_parent' => $v1->name,
                            'variation_child' => null,
                            'sku' => $color->color_id . 'xx' . $attributes1->attribute_id . 'xx' . $v1->name,
                            'price' => (float) $product->price,
                            'qty' => (int) $product->qty,
                        ]);
                    }
                }
            }
        }
    }

    public function getVariations(Request $request)
    {
        //return $request->all();
        $request->validate([
            'product_id' => ['required', 'integer', 'exists:products,id'],
            'color_id' => ['required', 'integer', 'exists:colors,id'],
        ]);
        $product = $request->get('product_id');
        $color = $request->get('color_id');
        $parent = $request->get('parent');
        $sku = $color . 'xx1xx';
        if ($parent) {
            $sku .= $parent . 'xx';
        }
        return $variations = Variation::where([
            'product_id' => $product,
            'color_id' => $color,
        ])->where('sku', 'like', "%{$sku}%")->get()->transform(function ($v) {
            return [
                "variation" => $v->variation_parent,
                "price" => $v->price,
                "qty" => $v->qty,
            ];
        });
    }

    /**USER */
    public function users()
    {
        View::share('page_title', "İstifadəçilər");
        View::share('page_breadcrumb', "İstifadəçilər");
        return view("admin.pages.users.index");
    }

    public function usersList(Request $request)
    {
        $pagination = $request->get('pagination');
        $query = $request->get('query');
        $limit = 20;
        if (isset($pagination) && $pagination['perpage']) {
            $limit = (int) $pagination['perpage'];
        }
        if (isset($pagination) && $pagination['page']) {
            $request->merge(['page' => $pagination['page']]);
        }
        $users = User::query()->orderBy('created_at', 'desc')->withCount(['orders']);
        if (isset($query) && isset($query['searchText'])) {
            $users->where(function ($q) use ($query) {
                $q->where('name', 'like', '%' . $query['searchText'] . '%');
                $q->orWhere('email', 'like', '%' . $query['searchText'] . '%');
            });
        }
        $users = $users->paginate($limit);
        return new UserCollection($users);
    }

    public function setClientCodes()
    {
        return 0;
        $users = User::query()->orderBy('created_at', 'desc')->get();
        $users->map(function ($user) {
            $user->code = $user->id;
            $user->save();
        });
        return 'success - 1';
    }

    public function setAddresses()
    {
        return 0;
        $users = User::query()->orderBy('created_at', 'desc')->get();
        $users->map(function ($user) {
            $address = Address::create([
                'user_id' => $user->id,
                'title' => 'Ev',
                'fullname' => $user->name,
                'city' => 'Bakı',
                'address' => $user->address,
                'phone' => $user->phone,
            ]);
            if ($user->addresses->count() == 1) {
                $address->update(['is_default' => true]);
            }
        });
        return 'success - 2';
    }

    public function setOrderAddresses()
    {
        return 0;
        $orders = Order::whereNotNull('user_id')->with('user')->get();
        $orders->map(function ($order) {
            $address = Address::where([
                'user_id' => $order->user->id,
                'is_default' => true,
            ])->first();
            if ($address) {
                $order->update([
                    'address' => json_encode([
                        'fullname' => $address->fullname,
                        'phone' => $address->phone,
                        'city' => $address->city,
                        'address' => $address->address,
                    ]),
                ]);
            }
        });
        return 'success - 3';
    }
    /**USER */
}
