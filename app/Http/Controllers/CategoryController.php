<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiResponser;

    function list(Request $request) {
        $categories = Category::where(['published' => true, 'parent_id' => 0])->with(['children'])->orderBy('rank', 'asc')->get();
        return $this->sendResponse($categories);
    }

    public function show(string $slug)
    {
        $product = '';
        return $this->sendResponse($product);
    }
}
