<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactRequest;
use App\Models\Contact;
use App\Traits\ApiResponser;

class ContactController extends Controller
{
    use ApiResponser;

    public function store(StoreContactRequest $request)
    {
        $data = $request->only([
            'first_name', 'last_name', 'phone', 'email', 'message',
        ]);
        try {
            Contact::query()->create($data);
            {
                //$_prMail = new ContactMail($data);
                //$_prMail->subject = "Message from " . $request->firstname;
                //Mail::to(settings('contact_email'))->send($_prMail);
            }
            return $this->sendResponse(null, 'success', 201);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }
}