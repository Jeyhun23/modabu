<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayDirectlyRequest;
use App\Models\Color;
use App\Models\OrderForm;
use App\Models\Product;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    use ApiResponser;

    public function payDirectly(PayDirectlyRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $attributes = json_decode($request->get('attributes')['attributes'], true);
        $color = Color::firstWhere(['name' => $attributes['color']]);
        $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
        if ($variation->qty < 1) {
            return $this->sendError(null, ['max quantity'], 404);
        }
        try {
            OrderForm::query()->create([
                'orderno' => substr(time(), -6) . rand(10, 99),
                'product_id' => $product->id,
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
                'address' => $request->get('address'),
                'total' => (float) $product->price,
                'discount' => ((int) $product->discount_type === 1) ? $product->discount : (float) ($product->price * $product->discount * 0.01),
                'payed' => ((int) $product->discount_type === 1) ? $product->price - $product->discount : (float) $product->price - $product->price * $product->discount * 0.01,
                'quantity' => $request->get('quantity') ?: 1,
                'attributes' => $request->get('attributes')['attributes'],
                'stock' => false,
            ]);
            $variation->update(['qty' => DB::raw('qty - 1')]);
            return $this->sendResponse(null, 'success', 201);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }
}
