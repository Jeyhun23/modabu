<?php

namespace App\Http\Controllers;

use App\Http\Requests\BannerStoreRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Models\Banner;
use App\Models\File;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class BannerController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        View::share('page_title', "Bannerlər");
        View::share('page_breadcrumb', "Bannerlər");
        return view("admin.pages.banners.index", compact('banners'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function list() {
        $banners = Banner::where(['published' => true])->orderBy('rank', 'asc')->get();
        return $this->sendResponse($banners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerStoreRequest $request)
    {
        if ($request->file("image")) {
            $path = File::storeFile('public/banners', $request->file("image"));
        }
        try {
            $banner = new Banner();
            $banner->title = $request->get('title');
            $banner->url = $request->get('url');
            $banner->image = '/banners/' . $path;
            $banner->position = $request->get('position');
            $banner->rank = $request->get('rank');
            $banner->save();
            return response()->json(['data' => 'Əlavə edildi'], 201);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(BannerUpdateRequest $request)
    {
        try {
            $banner = Banner::findOrFail($request->input('id'));
            if ($request->hasFile("image")) {
                if (Storage::disk('local')->exists($banner->image)) {
                    Storage::disk('local')->delete($banner->image);
                }
                $path = File::storeFile('public/banners', $request->file("image"));
                $banner->update([
                    $banner->image = '/banners/' . $path,
                ]);
            }
            $banner->update([
                'title' => $request->input('title'),
                'url' => $request->input('url'),
                'position' => $request->input('position'),
                'rank' => $request->input('rank'),
            ]);
            return response()->json(['data' => 'success', 'errors' => null]);
        } catch (\Exception $e) {
            return response()->json(['data' => null, 'errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy($banner)
    {
        $banner = Banner::where(['id' => $banner])->firstOrFail();
        $banner->delete();
        return response()->json(['data' => translate('success')], 200);
    }

    public function status($id)
    {
        Banner::query()
            ->where('id', $id)
            ->update(['published' => DB::raw("(1 - published)")]);

        return response()->json(['data' => translate('success')], 200);
    }
}