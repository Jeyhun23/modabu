<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Color;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use NumberFormatter;

class CartController extends Controller
{
    use ApiResponser;
    public function __construct(Request $request)
    {

    }

    /**
     * Add to cart
     *
     * @return json
     */

    public function addToCart($id, Request $request)
    {
        //return $request->get('attributes');
        //$start = microtime(true);
        $user = request()->user('api');
        $discount = 0;
        $create = false;
        $product = Product::with('brand')->findOrFail($id);
        $price = $product->price;

        //VALIDATION
        $arr2 = json_decode($request->get('attributes'), true);
        $color = $arr2['color'] ? Color::firstWhere(['name' => $arr2['color']]) : null;
        if ($color == null) {
            return $this->sendError(null, ['color not exists'], 404);
        }
        $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $arr2['size']])->first();
        if (!$variation) {
            return $this->sendError(null, ['size not exists'], 404);
        }
        //VALIDATION

        if ($product->discount_type == 1) {
            $discount += $product->discount;
            $price -= $product->discount;
        } else {
            $discount += $price * $product->discount / 100;
            $price -= $price * $product->discount / 100;
        }
        $cart = Cart::firstWhere(['user_id' => $user->id]);
        if ($cart) {
            $cart->discount = (float) $cart->discount + $discount;
            $cart->total = (float) $cart->total + $product->price;
            $cart->payable = (float) $cart->payable + $price;
            $cart->save();
        } else {
            $cart = Cart::query()->create(
                [
                    'user_id' => $user->id,
                    'coupon_id' => null,
                    'discount' => (float) number_format($discount, 2, '.', ''),
                    'total' => (float) number_format($product->price, 2, '.', ''),
                    'payable' => (float) number_format($product->price - $discount, 2, '.', ''),
                    'shipping_charges' => null,
                ]
            );
        }

        $cartItem = CartItem::query()->firstWhere([
            'cart_id' => $cart->id,
            'model_id' => $product->id,
            'attributes' => $request->get('attributes'),
        ]);
        if ($cartItem) {
            if ($cartItem->quantity + 1 > $variation->qty) {
                return $this->sendError(null, ['max quantity'], 404);
            }
            $cartItem->update([
                'name' => $product->title,
                'price' => $product->price,
                'quantity' => DB::raw("(1 + quantity)"),
            ]);
        } else {
            $create = true;
        }

        if ($create) {
            CartItem::create([
                'cart_id' => $cart->id,
                'model_id' => $product->id,
                'model_type' => 'App\Models\Product',
                'name' => $product->title,
                'brand' => $product->brand->name,
                'price' => $product->price,
                'image' => $product->thumbnail,
                'quantity' => 1,
                'attributes' => $request->get('attributes'),
            ]);
        }
        //return $time_elapsed_secs = microtime(true) - $start;
        return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
    }

    public function getCart()
    {
        $user = request()->user('api');
        return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
    }

    /**
     * Remove from cart
     *
     * @return json
     */
    public function removeFromCart(int $id, Request $request)
    {
        $user = request()->user('api');
        $discount = 0;

        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart) {
                $cart->discount = (float) $cart->discount - $discount * $cartItem->quantity;
                $cart->total = (float) $cart->total - $product->price * $cartItem->quantity;
                $cart->payable = (float) $cart->payable - $price * $cartItem->quantity;
                $cart->save();
                if ($cart->items()->count() == 1) {
                    $cart->delete();
                } else {
                    $cartItem->delete();
                }
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Increment cart item quantity
     *
     * @return json
     */
    public function incrementCartItem(int $id)
    {
        $user = request()->user('api');
        $discount = 0;
        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $attributes = json_decode($cartItem->attributes, true);
            $color = $attributes['color'] ? Color::firstWhere(['name' => $attributes['color']]) : null;
            $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
            if ($cartItem->quantity + 1 > $variation->qty) {
                return $this->sendError(null, ['max quantity'], 404);
            }
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart && $cartItem) {
                $cart->discount = (float) $cart->discount + $discount;
                $cart->total = (float) $cart->total + $product->price;
                $cart->payable = (float) $cart->payable + $price;
                $cart->save();
                $cartItem->quantity += 1;
                $cartItem->save();
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Decrement cart item quantity
     *
     * @return json
     */
    public function decrementCartItem(int $id)
    {
        $user = request()->user('api');
        $discount = 0;
        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart && $cartItem && $cartItem->quantity > 1) {
                $cart->discount = (float) $cart->discount - $discount;
                $cart->total = (float) $cart->total - $product->price;
                $cart->payable = (float) $cart->payable - $price;
                $cart->save();
                $cartItem->quantity -= 1;
                $cartItem->save();
                //if ($cartItem->quantity == 0) {
                //    $cartItem->delete();
                //}
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Clear Cart
     *
     * @return json
     */
    public function clearCart()
    {
        $user = request()->user('api');
        return Cart::with('items')->firstWhere(['user_id' => $user->id])->delete();
    }

    /**
     * Make Order
     *
     * @return success
     */
    public function makeOrder(Request $request)
    {
        $user = request()->user('api');
        if ($user) {
            $request->validate([
                'address' => ['required', 'integer', 'exists:addresses,id'],
            ]);
            $address = Address::firstWhere(['id' => $request->address, 'user_id' => $user->id]);
            if (!$address) {
                return $this->sendError('Düzgün ünvan seçin!', ['select valid address'], 400);
            }
            $cart = Cart::with('items')->firstWhere(['user_id' => $user->id]);
            $errors = array();
            if ($cart) {
                foreach ($cart->items as $item) {
                    $product = Product::firstWhere(['id' => $item->model_id, 'confirmed' => true]);
                    $attributes = json_decode($item->attributes, true);
                    $color = $attributes['color'] ? Color::firstWhere(['name' => $attributes['color']]) : null;
                    $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
                    if (!$variation || $item->quantity > $variation->qty) {
                        $a = ['item' => $item->id, 'stock' => $variation ? $variation->qty : 0];
                        array_push($errors, $a);
                    }
                }
                if (!empty($errors)) {
                    return $this->sendError('quantity exceed', $errors, 400);
                }
                $_total = 0;
                $_payable = 0;
                $_discounts = 0;
                foreach ($cart->items as $item) {
                    $discount = 0;
                    $product = Product::firstWhere(['id' => $item->model_id, 'confirmed' => true]);
                    if ((float) $product->discount != 0) {
                        if ($product->discount_type == 1) {
                            $discount = $product->discount * $item->quantity;
                        } else {
                            $discount = $product->price * $product->discount / 100 * $item->quantity;
                        }
                    }
                    $_discounts += $discount;
                    $_payable += $product->price * $item->quantity - $discount;
                    $_total += $product->price * $item->quantity;
                }
                $order = Order::query()->create([
                    'user_id' => $user->id,
                    'total' => $_total,
                    'discount' => $_discounts,
                    'payed' => $_total,
                    'address' => json_encode([
                        'fullname' => $address->fullname,
                        'phone' => $address->phone,
                        'city' => $address->city,
                        'address' => $address->address,
                    ]),
                ]);
                foreach ($cart->items as $item) {
                    $product = Product::firstWhere(['id' => $item->model_id, 'confirmed' => true]);
                    $discount = 0;
                    $attributes = json_decode($item->attributes, true);
                    $color = Color::firstWhere(['name' => $attributes['color']]);
                    $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
                    $variation->update(['qty' => DB::raw('qty - 1')]);
                    if ((float) $product->discount != 0) {
                        if ($product->discount_type == 1) {
                            $discount = $product->discount * $item->quantity;
                        } else {
                            $discount = $product->price * $product->discount / 100 * $item->quantity;
                        }
                    }
                    OrderDetail::query()->create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'price' => $product->price,
                        'discount' => $discount,
                        'quantity' => $item->quantity,
                        'attributes' => $item->attributes,
                        'stock' => false
                    ]);
                }
                $cart->items()->delete();
                $cart->delete();
                return $this->sendResponse(null, 'Successfully completed');
            }
            return $this->sendError('Empty Cart', ['Cart is Empty'], 404);
        } else {
            $request->validate([
                'products.*.id' => ['required', 'integer', 'exists:products,id'],
                'products.*.qty' => ['required', 'integer'],
                'products.*.attributes' => ['required', 'string'],
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                'phone' => ['required', 'string'],
                'city' => ['required', 'string'],
                'address' => ['required', 'string'],
            ]);

            $errors = [];
            $rP = $request->products;
            foreach ($rP as $item) {
                $product = Product::firstWhere(['id' => $item['id'], 'confirmed' => true]);
                $attributes = json_decode($item['attributes'], true);
                $color = $attributes['color'] ? Color::firstWhere(['name' => $attributes['color']]) : null;
                $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
                if (!$variation || $item['qty'] > $variation->qty) {
                    $a = ['item' => $item['id'], 'stock' => $variation ? $variation->qty : 0];
                    array_push($errors, $a);
                }
            }
            if (!empty($errors)) {
                return $this->sendError('quantity exceed', $errors, 400);
            }
            $total = 0;
            $payable = 0;
            $discounts = 0;
            try {

                foreach ($rP as $item) {
                    $discount = 0;
                    $product = Product::firstWhere(['id' => $item['id'], 'confirmed' => true]);
                    if ((float) $product->discount != 0) {
                        if ($product->discount_type == 1) {
                            $discount = $product->discount * $item['qty'];
                        } else {
                            $discount = $product->price * $product->discount / 100 * $item['qty'];
                        }
                    }
                    $discounts += $discount;
                    $payable += $product->price * $item['qty'] - $discount;
                    $total += $product->price * $item['qty'];
                }

                $order = Order::query()->create([
                    'user_id' => null,
                    'total' => $total,
                    'discount' => $discounts,
                    'payed' => $payable,
                    'address' => json_encode([
                        'fullname' => $request->input('firstname') . ' ' . $request->input('lastname'),
                        'phone' => $request->input('phone'),
                        'city' => $request->input('city'),
                        'address' => $request->input('address'),
                    ]),
                ]);
                foreach ($rP as $prdct) {
                    $product = Product::firstWhere(['id' => $prdct['id'], 'confirmed' => true]);
                    $discount = 0;
                    $attributes = json_decode($prdct['attributes'], true);
                    $color = Color::firstWhere(['name' => $attributes['color']]);
                    $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
                    $variation->update(['qty' => DB::raw('qty - 1')]);
                    if ((float) $product->discount != 0) {
                        if ($product->discount_type == 1) {
                            $discount = $product->discount * $prdct['qty'];
                        } else {
                            $discount = $product->price * $product->discount / 100 * $prdct['qty'];
                        }
                    }
                    OrderDetail::query()->create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'price' => $product->price,
                        'discount' => $discount,
                        'quantity' => $prdct['qty'],
                        'attributes' => $prdct['attributes'],
                        'stock' => false
                    ]);
                }
                return $this->sendResponse('success', 'Sifariş tamamlandı');
            } catch (\Exception $e) {
                return $this->sendError('error', [$e->getMessage()]);
            }
        }
    }

    public function addToCartAll(Request $request)
    {
        //$request->validate([]);
        //return $request->all();
        $user = request()->user('api');
        $products = $request->get('products');
        if (is_array($products) && !empty($products)) {
            foreach ($products as $item) {
                $product = Product::with('brand')->findOrFail($item['id']);
                $discount = 0;
                $create = false;
                $price = $product->price;
                if ($product->hasDiscount()) {
                    if ($product->discount_type == 1) {
                        $discount += $product->discount;
                        $price -= $product->discount;
                    } else {
                        $discount += $price * $product->discount / 100;
                        $price -= $price * $product->discount / 100;
                    }
                }
                $cart = Cart::firstWhere(['user_id' => $user->id]);
                if ($cart) {
                    $cart->discount = (float) $cart->discount + $discount;
                    $cart->total = (float) $cart->total + $product->price;
                    $cart->payable = (float) $cart->payable + $price;
                    $cart->save();
                } else {
                    $cart = Cart::query()->create(
                        [
                            'user_id' => $user->id,
                            'coupon_id' => null,
                            'discount' => (float) number_format($discount, 2, '.', ''),
                            'total' => (float) number_format($product->price, 2, '.', ''),
                            'payable' => (float) number_format($product->price - $discount, 2, '.', ''),
                            'shipping_charges' => null,
                        ]
                    );
                }

                $cartItem = CartItem::query()->firstWhere([
                    'cart_id' => $cart->id,
                    'model_id' => $product->id,
                ]);
                if ($cartItem) {
                    $arr1 = json_decode($cartItem->attributes, true);
                    $arr2 = json_decode($item['attributes'], true);
                    if (is_array($arr1) && is_array($arr2) && (bool) array_diff($arr1, $arr2) === false) {
                        $cartItem->update([
                            'name' => $product->title,
                            'price' => $product->price,
                            'quantity' => DB::raw("(" . $item['qty'] . " + quantity)"),
                        ]);
                    } else {
                        $create = true;
                    }
                } else {
                    $create = true;
                }
                if ($create) {
                    CartItem::create([
                        'cart_id' => $cart->id,
                        'model_id' => $product->id,
                        'model_type' => 'App\Models\Product',
                        'name' => $product->title,
                        'brand' => $product->brand->name,
                        'price' => $product->price,
                        'image' => $product->thumbnail,
                        'quantity' => $item['qty'],
                        'attributes' => $item['attributes'],
                    ]);
                }
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, 'invalid request', 500);
    }
}
