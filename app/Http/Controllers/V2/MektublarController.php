<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Mektublar;
use App\Models\Story;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class MektublarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mektublar = Mektublar::query()
            ->orderBy('created_at', 'desc');

        $mektublar = app(Pipeline::class)
            ->send($mektublar)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.mektublar.index', compact('mektublar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Mektublar $mektublar
     * @return \Illuminate\Http\Response
     */
    public function show(Mektublar $mektublar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Mektublar $mektublar
     * @return \Illuminate\Http\Response
     */
    public function edit(Mektublar $mektublar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Mektublar $mektublar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mektublar $mektublar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Mektublar $mektublar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mektublar $mektublar)
    {
        $mektublar->delete();

        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            Mektublar::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
