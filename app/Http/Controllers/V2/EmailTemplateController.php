<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailTempalteRequest;
use App\Http\Requests\SMSTempalteRequest;
use App\Models\EmailTemplates;
use App\Models\SMSTemplates;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class EmailTemplateController extends Controller
{
    public function index()
    {
        $email = EmailTemplates::query()
            ->orderBy('created_at', 'desc');

        $email = app(Pipeline::class)
            ->send($email)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.email_template.index', compact('email'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.v2.email_template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmailTempalteRequest $request)
    {
        EmailTemplates::create([
            'name' => $request->name,
            'message' => $request->message,
            'visible' => request()->has('visible') ? (request('visible') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('emailtemplates.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplates $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTemplates $emailtemplate)
    {
        return view('admin.v2.email_template.edit', compact('emailtemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmailTempalteRequest $request, EmailTemplates $emailtemplate)
    {
        $emailtemplate->update([
            'name' => $request->name,
            'message' => $request->message,
            'visible' => request()->has('visible') ? (request('visible') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('emailtemplates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplates $emailtemplate)
    {
        $emailtemplate->delete();
        return redirect()->route('emailtemplates.index');
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            EmailTemplates::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function updateVisible(EmailTemplates $emailtemplate)
    {
        $emailtemplate->update([
            'visible' => !$emailtemplate->visible
        ]);
        return redirect()->route('emailtemplates.index');
    }
}
