<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    private $dateRange;
    private $from;
    private $to;

    public function __construct()
    {
        $this->dateRange = request()->input('create_date') ?: null;
        $this->dateRange = explode('-', $this->dateRange);
        $this->from      = isset($this->dateRange[0]) ? date("Y-m-d",strtotime($this->dateRange[0])) : null;
        $this->to        = isset($this->dateRange[1]) ?  date("Y-m-d",strtotime($this->dateRange[1])) : now();
    }

    public function index()
    {
        
        $today = date('m/d/Y', strtotime('today'))."-".date('m/d/Y', strtotime('tomorrow'));
        $thisweek = date('m/d/Y', strtotime('monday this week'))."-".date('m/d/Y', strtotime('tomorrow'));
        $thismonth = date('m/d/Y', strtotime('first day of this month'))."-".date('m/d/Y', strtotime('tomorrow'));
        return view("admin.v2.statistics.index", compact('today','thisweek','thismonth'));
    }

    public function getData($type)
    {
        switch ($type) {
            case "general":
              return $this->general();
              break;
            case "order_category":
                return $this->order_category();
              break;
            case "order_brand":
                return $this->order_brand();
              break;
            case "stock_category":
                return $this->stock_category();
              break;
            case "stock_brand":
                return $this->stock_brand();
              break;
            default:
              abort(404);
          }
        return view("admin.v2.statistics.index");
    }

    //general
    private function general(){
        $users = User::doesnthave('roles')->whereBetween('created_at', [$this->from, $this->to])->count();
        $orders = OrderDetail::whereBetween('created_at', [$this->from, $this->to])->sum('quantity');
        $products = Product::where('confirmed', true)->whereBetween('created_at', [$this->from, $this->to])->count();
        $brands = Brand::where('published', true)->whereBetween('created_at', [$this->from, $this->to])->count();

        return view('admin.v2.statistics.partials.general', compact('users', 'orders', 'products', 'brands'));
    }
    
    //order-category
    private function order_category(){
        $data = Category::groupBy('categories.id', 'categories.name')
            ->select('categories.name', DB::raw('IFNULL(SUM(order_details.quantity), 0) as total'))
            ->leftJoin('products', 'products.category_id', '=', 'categories.id') 
            ->leftJoin('order_details', function ($join) {
                $join->on('products.id', '=', 'order_details.product_id')
                    ->whereBetween('order_details.created_at', [$this->from, $this->to]);
            })
            ->orderby('total', 'DESC')
        ->get();
        return view('admin.v2.statistics.partials.table', compact('data'));
    }

    //order-brands
    private function order_brand(){
        $data = Brand::groupBy('brands.id', 'brands.name')
            ->select('brands.name', DB::raw('IFNULL(SUM(order_details.quantity), 0) as total'))
            ->leftJoin('products', 'products.brand_id', '=', 'brands.id') 
            ->leftJoin('order_details', function ($join) {
                $join->on('products.id', '=', 'order_details.product_id')
                    ->whereBetween('order_details.created_at', [$this->from, $this->to]);
            })
            ->orderby('total', 'DESC')
        ->get();

        return view('admin.v2.statistics.partials.table', compact('data'));
    }
    
    //stock-category
    private function stock_category(){
        $data = Product::groupBy('category_id', 'categories.name')
        ->where('confirmed', true)
        ->whereBetween('products.created_at', [$this->from, $this->to])
        ->select('categories.name', 'category_id', DB::raw('sum(qty) as total'), DB::raw('count(*) as product'))
        ->join('categories', 'categories.id', '=', 'products.category_id')
        ->get();

        return view('admin.v2.statistics.partials.table', compact('data'));
    }
    
    //stock-category
    private function stock_brand(){
        $data = Product::groupBy('brand_id', 'brands.name')
            ->where('confirmed', true)
            ->whereBetween('products.created_at', [$this->from, $this->to])
            ->select('brands.name', 'brand_id', DB::raw('sum(qty) as total'), DB::raw('count(*) as product'))
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->get();

        return view('admin.v2.statistics.partials.table', compact('data'));
    }
}
