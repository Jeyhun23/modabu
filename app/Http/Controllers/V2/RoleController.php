<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Product;
use App\Models\Refler;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::query()
            ->orderBy('created_at', 'desc');

        $roles = app(Pipeline::class)
            ->send($roles)
            ->through([
                \App\QueryFilters\Title::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request, Role $role)
    {
        $data = $request->except('_token');
        $role->fill($data);
        $role->save();
        return redirect(route('roles.index'))->with('success', 'Əməliyyat uğurla Insert Oldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $data = $request->except('_token');
        $role->fill($data);
        $role->update();
        return redirect(route('roles.index'))->with('success', 'Əməliyyat uğurla Update olundu.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            Role::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
