<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\SMSTempalteRequest;
use App\Models\SMSTemplates;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class SMSTemplateController extends Controller
{
    public function index()
    {
        $sms = SMSTemplates::query()
            ->orderBy('created_at', 'desc');

        $sms = app(Pipeline::class)
            ->send($sms)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.sms_template.index', compact('sms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.v2.sms_template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SMSTempalteRequest $request)
    {
        SMSTemplates::create([
            'name' => $request->name,
            'message' => $request->message,
            'visible' => request()->has('visible') ? (request('visible') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('smstemplates.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(SMSTemplates $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SMSTemplates $smstemplate)
    {
        return view('admin.v2.sms_template.edit', compact('smstemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SMSTempalteRequest $request, SMSTemplates $smstemplate)
    {
        $smstemplate->update([
            'name' => $request->name,
            'message' => $request->message,
            'visible' => request()->has('visible') ? (request('visible') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('smstemplates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SMSTemplates $smstemplate)
    {
        $smstemplate->delete();
        return redirect()->route('smstemplates.index');
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            SMSTemplates::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function updateVisible(SMSTemplates $smstemplate)
    {
        $smstemplate->update([
            'visible' => !$smstemplate->visible
        ]);
        return redirect()->route('smstemplates.index');
    }

    public function smsSendToClient()
    {
        return view('admin.v2.sms_template.send_sms');
    }

    public function smsSendToClientPost(Request $request)
    {
        if ($request->has("sendAll")) {
            if ($request->sendAll == "on") {
                $users = User::whereDoesntHave("roles")->get();
                foreach ($users as $user) {
                    $this->smsService->send($this->preprocessNumber($user->phone), $request->message);
                }
                return redirect()->route('send.sms.admin')->with(['success' => "SMS Ugurla gonderildi"]);
            }
        }
        if ($request->phone) {
            $phones = explode(',', $request->phone);
            foreach ($phones as $p) {
                $this->smsService->send($this->preprocessNumber($p), $request->message);
            }
            return redirect()->route('send.sms.admin')->with(['success' => "SMS Ugurla gonderildi"]);
        }
        return redirect()->route('send.sms.admin');
    }
}
