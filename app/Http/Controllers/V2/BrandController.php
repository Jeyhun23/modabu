<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\BrandRequest;
use App\Models\Brand;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::query()
            ->withCount(['products'])
            ->orderBy('created_at', 'desc');

        $brands = app(Pipeline::class)
            ->send($brands)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());
        return  view('admin.v2.brands.index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.v2.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request,Brand $brand)
    {
        $data = $request->except('_token','logo');
        $path = File::storeFile('public/brands', $request->file("logo"));
        $data['logo'] = $path;
        $data['meta'] = '';
        $brand->fill($data);
        $brand->save();
        return redirect(route('brands.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return  view('admin.v2.brands.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, Brand $brand)
    {
        $data = $request->except('_method'.'_token','logo');
        if ($request->hasFile('logo')){
            $path = File::storeFile('public/brands', $request->file("logo"));
            $data['logo'] = $path;
        }
        $data['meta'] = '';
        $brand->fill($data);
        $brand->update();
        return redirect(route('brands.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
       if ($brand->products->count()  ==  0) {
           $brand->delete();
           return request()->ajax() ?
               response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'],200) :
               back()->with('success', 'Əməliyyat uğurla tamamlandı.');
       }
    }
}
