<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\StoryRequest;
use App\Models\Banner;
use App\Models\File;
use App\Models\Story;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stories = Story::query()
            ->orderBy('created_at', 'desc');

        $stories = app(Pipeline::class)
            ->send($stories)
            ->through([
                \App\QueryFilters\Title::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.stories.index', compact('stories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryRequest $request, Story $story)
    {
        $path = File::resiveAndstoreFile($request->file('image'), '/stories', $request->file('image')->getClientOriginalExtension());
        $data = $request->except('_token', 'image');
        $data['thumb_nail'] = $path;
        $story->fill($data);
        $story->save();
        return redirect(route('stories.index'))->with('success', 'Əməliyyat uğurla Insert Oldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Story $story
     * @return \Illuminate\Http\Response
     */
    public function show(Story $story)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Story $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        return response()->json($story);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Story $story
     * @return \Illuminate\Http\Response
     */
    public function update(StoryRequest $request, Story $story)
    {
        $data = $request->except('_token', 'image');
        if ($request->hasFile('image')) {
            File::deleteFile(public_path('storage/stories/' . $story->thumb_nail));
            $path = File::resiveAndstoreFile($request->file('image'), '/stories', $request->file('image')->getClientOriginalExtension());
            $data['thumb_nail'] = $path;
        }
        $story->fill($data);
        $story->update();
        return redirect(route('stories.index'))->with('success', 'Əməliyyat uğurla Update olundu.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Story $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        File::deleteFile(public_path('storage/stories/' . $story->thumb_nail));
        $story->delete();
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            foreach ($array as $id) {
                $story = Story::where('id', $id)->first();
                File::deleteFile(public_path('storage/stories/' . $story->thumb_nail));
                $story->delete();
            }
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
