<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\BannerRequest;
use App\Models\Banner;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::query()
            ->orderBy('created_at', 'desc');

        $banners = app(Pipeline::class)
            ->send($banners)
            ->through([
                \App\QueryFilters\Title::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return  view('admin.v2.banners.index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.v2.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request,Banner $banner)
    {
        $data = $request->except('_token','image');
        $path = File::storeFile('public/banners', $request->file("image"));
        $data['image'] = $path;
        $data['meta'] = '';
        $banner->fill($data);
        $banner->save();
        return redirect(route('banners.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('admin.v2.banners.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, Banner $banner)
    {
        $data = $request->except('_token','image');
        if ($request->hasFile('image')){
            \App\Models\File::deleteFile(public_path('storage/banners/' . $banner->image));
            $path = File::storeFile('public/banners', $request->file("image"));
            $data['image'] = $path;
        }

        $data['meta'] = '';
        $banner->fill($data);
        $banner->update();
        return redirect(route('banners.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    public function update_banner_visibility(Banner $banner)
    {
        $banner->update([
            'published' => !$banner->published
        ]);
        return redirect()->route('banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        \App\Models\File::deleteFile(public_path('storage/banners/' . $banner->image));
        $banner->delete();
        return request()->ajax() ?
            response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'],200) :
            back()->with('success', 'Əməliyyat uğurla tamamlandı.');
    }
}
