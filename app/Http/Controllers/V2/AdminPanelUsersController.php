<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Hash;

class AdminPanelUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query()->has('roles')
            ->orderBy('created_at', 'desc');

        if ($request->has('role')) {
            if ($request->role) {
                $users = User::query()->orderBy('created_at', 'desc');
            }
        }
        $user = $users->with('roles');

        $users = app(Pipeline::class)
            ->send($users)
            ->through([
                \App\QueryFilters\Name::class,
                \App\QueryFilters\Role::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        $roles = Role::select([
            'id', 'title'
        ])->get();

        return view('admin.v2.adminpanel_users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\V2\AdminPanelUserRequest $request, User $user, RoleUser $roleUser)
    {
        $data = $request->except('_token');
        $data['password'] = Hash::make($data['password']);
        $user->fill($data);
        $user->save();
        $roleUser->insert([
            'role_id' => $data['roles'],
            'user_id' => $user->id
        ]);
        return redirect(route('admin_panel_users.index'))->with('success', 'Əməliyyat uğurla Insert Oldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $admin_panel_user
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\V2\AdminPanelUserRequest $request, User $admin_panel_user)
    {
        $data = $request->except('_token');
        if ($data['password']) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data['password'] = $admin_panel_user->password;
        }
        if ($data['roles']) {
            RoleUser::where('user_id', $admin_panel_user->id)->update([
                'role_id' => $data['roles']
            ]);
        }
        if (!$data['phone']) {
            $data['phone'] = $admin_panel_user->phone;
        }
        $admin_panel_user->fill($data);
        $admin_panel_user->save();
        return redirect(route('admin_panel_users.index'))->with('success', 'Əməliyyat uğurla Update Oldu.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin_panel_user)
    {
        RoleUser::where('user_id', $admin_panel_user->id)->delete();
        User::where('id', $admin_panel_user)->delete();
        return response()->json(['message' => 'Silinme uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            RoleUser::whereIn('user_id', $array)->delete();
            User::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
