<?php

namespace App\Http\Controllers\V2\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\WishlistCollection;
use App\Models\User;
use App\Services\Wishlist;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    use ApiResponser;

    public function userWishlists(Request $request)
    {
        $user = $request->user();
        $wishlist = new Wishlist();
        return new WishlistCollection($wishlist->getUserWishlist($user->id)->load('product'));
    }

    public function addWishlist($product_id, Request $request)
    {
        $user = $request->user();
        $wishlist = new Wishlist();
        $wishlist->add($product_id, $user->id);
        $wishlists = $wishlist->getUserWishlist($user->id);
        return new WishlistCollection($wishlist->getUserWishlist($user->id)->load('product'));

    }

    public function removeWishlist($product_id, Request $request)
    {
        $user = $request->user();
        $wishlist = new Wishlist();
        $wishlist->removeByProduct($product_id, $user->id);
        $wishlists = $wishlist->getUserWishlist($user->id);
        return new WishlistCollection($wishlist->getUserWishlist($user->id)->load('product'));
    }
}
