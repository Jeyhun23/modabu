<?php

namespace App\Http\Controllers\V2\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddressStoreRequest;
use App\Http\Requests\AddressUpdateRequest;
use App\Http\Resources\AddressCollection;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressStoreRequest $request)
    {
        $user = User::find($request->user()->id);
        try {
            $address = Address::create([
                'user_id' => $user->id,
                'title' => $request->input('title'),
                'fullname' => $request->input('firstname') . ' ' . $request->input('lastname'),
                'city' => $request->input('city'),
                'address' => $request->input('address'),
                'phone' => $request->input('phone'),
                'is_default' => false,
            ]);
            if ($user->addresses()->count() == 1) {
                $address->update(['is_default' => true]);
            }
            return new AddressResource($address);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    function list(Request $request) {
        $user = User::find($request->user()->id);
        //return $user->addresses();
        try {
            $addresses = Address::where(['user_id' => $user->id])->paginate(5);
            return new AddressCollection($addresses);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(AddressUpdateRequest $request, Address $address)
    {
        $user = User::find($request->user()->id);
        try {
            if ($address->user_id == $user->id) {
                $address->title = $request->input('title');
                $address->fullname = $request->input('firstname') . ' ' . $request->input('lastname');
                $address->phone = $request->input('phone');
                $address->city = $request->input('city');
                $address->address = $request->input('address');
                $address->phone = $request->input('phone');
                $address->save();
                return new AddressResource($address);
            } else {
                return $this->sendError(null, 'address not found', 404);
            }
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Address $address)
    {
        $user = User::find($request->user()->id);
        try {
            if ($address->user_id == $user->id) {
                if ($address->is_default) {
                    if ($user->addresses->count() > 1) {
                        $user->addresses()->where('id', '!=', $address->id)->first()->update(['is_default' => true]);
                    }
                }
                $address->delete();
                return $this->sendResponse(null, 'deleted');
            } else {
                return $this->sendError(null, 'address not found', 404);
            }
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    public function setDefault(Request $request, Address $address)
    {
        $user = User::find($request->user()->id);
        try {
            if ($address->user_id == $user->id) {
                $user->addresses()->update(['is_default' => false]);
                $address->update(['is_default' => true]);
                return new AddressResource($address);
            } else {
                return $this->sendError(null, 'address not found', 404);
            }
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }
}
