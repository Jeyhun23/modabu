<?php

namespace App\Http\Controllers\V2\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\PayDirectlyRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Color;
use App\Models\Order;
use App\Models\OrderForm;
use App\Models\Product;
use App\Models\User;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    use ApiResponser;

    public function getOrders(Request $request)
    {
        $limit = 5;
        $user = User::find($request->user()->id);
        $orders = $user->orders()->orderBy('created_at', 'desc')->with(['detail'])->paginate($limit);
        return new OrderCollection($orders);
    }

    public function getOrder(Order $order, Request $request)
    {
        $user = User::find($request->user()->id);
        abort_if($order->user_id != $user->id, Response::HTTP_FORBIDDEN, '403 Forbidden');
        $order->load(['user', 'detail']);
        return new OrderResource($order);
    }
}
