<?php

namespace App\Http\Controllers\V2\Api\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create user
     *
     * @param  [string] phone
     * @param  [string] password
     * @return [string] message
     */
    private $pac = "Modabu Personal Access Client";


    /**
     * Login user and create token
     *
     * @param  [string] phone
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'phone' => 'required|exists:users,phone',
            'password' => 'required|string|min:6|max:30',
            'remember_me' => 'boolean',
        ]);

        $credentials = request(['phone', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->sendError('Unauthorized', ['Daxil etdiyiniz məlumatlar düzgün deyil'], 422);
        }
        if (User::where('phone', $request->input('phone'))->whereNull('phone_verified_at')->exists()) {
            return $this->sendError('Account not verified', ['Please verify your account'], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken($this->pac);
        if ($request->remember_me) {
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
        }
        $res['access_token'] = $tokenResult->accessToken;
        $res['token_type'] = 'Bearer';
        $res['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
        return $this->sendResponse($res, 'Uğurlu giriş');
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse(null, 'Uğurlu çıxış');
    }
}
