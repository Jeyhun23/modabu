<?php

namespace App\Http\Controllers\V2\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\SMSTemplates;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    use ApiResponser;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    public function sendToken(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'exists:users,phone']
        ]);

        if ($request->filled('phone')) {
            $phone = $this->preprocessNumber($request->get('phone'));
            $request->merge(['phone' => $phone]);
        }

        $user = User::where(['phone' => $request->input('phone')])->firstOrFail();
        $user->verification_token = rand(1000, 9999);
        $user->save();

        $smstemp = SMSTemplates::where('id', 3)->where('visible', true)->first();
        if ($smstemp) {
            $message = $smstemp->message . $user->verification_token;
            $sms = $this->smsService->send($user->getAttribute('phone'), $message);
            $responseBody = $sms->getAttribute('response');
            $responseBody = simplexml_load_string($responseBody);
            if ($responseBody->head->responsecode == "000") {
                return $this->sendResponse(['phone' => $user->phone], 'Şifrənizi yeniləmək üçün kod daxil etdiyiniz nömrəyə göndərildi');
            } else {
                return $this->sendError('Kod göndərilmədi, yenidən cəhd edin', [], 500);
            }
        } else {
            return $this->sendError('SMS xidməti aktiv deyil, zəhmət olmasa biraz sonra yenidən cəhd edin', [], 500);
        }
    }
}
