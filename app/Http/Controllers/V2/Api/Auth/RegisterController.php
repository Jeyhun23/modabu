<?php

namespace App\Http\Controllers\V2\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\SMSTemplates;
use App\Models\User;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    use ApiResponser;

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */
    private $pac = "Modabu Personal Access Client";

    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function register(Request $request): JsonResponse
    {
        if ($request->filled('phone')) {
            $phone = $this->preprocessNumber($request->get('phone'));
            $request->merge(['phone' => $phone]);
        }

        $request->validate([
            'phone' => [
                'required', Rule::unique('users', 'phone')
                    ->whereNotNull('phone_verified_at')
            ]
        ], [
            'phone.unique' => 'Bu nömrə artıq qeydiyyatdan keçmişdir.',
        ], []);

        $user = User::query()->updateOrCreate(
            [
                'phone' => $request->input('phone')
            ],
            [
                'verification_token' => rand(1000, 9999),
            ]
        );
        $smstemp = SMSTemplates::where('id', 2)->where('visible', true)->first();
        if ($smstemp) {
            $message = $smstemp->message . $user->verification_token;
            $sms = $this->smsService->send($user->getAttribute('phone'), $message);

            $responseBody = simplexml_load_string($sms->getAttribute('response'));
            if ($responseBody->head->responsecode == "000") {
                return $this->sendResponse(['phone' => $user->phone], 'Hesabınızı təsdiqləmək üçün kod daxil etdiyiniz nömrəyə göndərildi');
            } else {
                return $this->sendError('Qeydiyyat tamamlanmadı, yenidən cəhd edin', [], 500);
            }
        } else {
            return $this->sendError('SMS xidməti aktiv deyil, zəhmət olmasa biraz sonra yenidən cəhd edin', [], 500);
        }
    }

    public function setPassword(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'exists:users,phone'],
            'code' => ['required', 'exists:users,verification_token'],
            'password' => ['required', 'min:5'],
        ]);

        $user = User::where([
            'phone' => $request->input('phone'),
            'verification_token' => $request->input('code'),
        ])->first();

        if ($user) {
            $user->password = Hash::make($request->input('password'));
            $user->phone_verified_at = Carbon::now();
            $user->save();

            $tokenResult = $user->createToken($this->pac);
            $res['access_token'] = $tokenResult->accessToken;
            $res['token_type'] = 'Bearer';
            $res['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
            return $this->sendResponse($res, 'Uğurlu giriş', 200);
        } else {
            return $this->sendError('Daxil edilən məlumatlar düzgün deyil', ['Daxil edilən məlumatlar düzgün deyil'], 422);
        }
    }
}
