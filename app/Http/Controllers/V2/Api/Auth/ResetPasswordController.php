<?php

namespace App\Http\Controllers\V2\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends Controller
{
    use ApiResponser;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    // use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request): JsonResponse
    {
        $request->validate([
            'phone' => ['required', 'exists:users,phone'],
            'code' => ['required', 'exists:users,verification_token'],
            'password' => ['required', 'min:5'],
        ]);

        $user = User::query()->where([
            'phone' => $request->input('phone'),
            'verification_token' => $request->input('code'),
        ])->first();

        if ($user) {
            $user->password = Hash::make($request->input('password'));
            $user->save();
            return $this->sendResponse([], 'Şifrəniz dəyişdirildi');
        } else {
            return $this->sendError('Daxil edilən məlumatlar düzgün deyil', ['Daxil edilən məlumatlar düzgün deyil'], 422);
        }
    }
}
