<?php

namespace App\Http\Controllers\V2\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class VerificationController extends Controller
{
    use ApiResponser;
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function check(Request $request): JsonResponse
    {
        $request->validate([
            'phone' => 'required',
            'code' => 'required|digits:4',
        ]);
        $user = User::where([
            'phone' => $request->input('phone'),
            'verification_token' => $request->input('code'),
        ])->exists();

        if ($user) {
            return $this->sendResponse([], 'Doğrulama kodu düzgündür', 200);
        }
        return $this->sendError('Doğrulama kodu düzgün deyil', ['Kod düzgün daxil edilməyib']);
    }

    public function verify(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'code' => 'required|digits:4',
        ]);
        $user = User::where([
            'phone' => $request->input('phone'),
            'verification_token' => $request->input('code')
        ])->first();

        if ($user) {
            $user->phone_verified_at = now();
            $user->save();
            return $this->sendResponse([], 'Hesab təsdiqləndi', 200);
        }
        return $this->sendError('Doğrulama kodu düzgün deyil', ['Kod düzgün daxil edilməyib']);
    }

    public function resend()
    {
        //
    }


}
