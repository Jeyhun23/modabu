<?php

namespace App\Http\Controllers\V2\Api\Payment;

use App\Http\Controllers\Controller;
use App\Models\Olympic;
use App\Models\OlympicContestant;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Plan;
use App\Models\Referral;
use App\Models\User;
use App\Models\UserPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class PaymentController extends Controller
{
    protected $serviceUrl = 'https://e-commerce.kapitalbank.az:5443/Exec';
    protected $cert = "/certificates/production/modabu_kapital.crt";
    protected $key = "/certificates/production/modabu_kapital.pem";
    protected $merchant_id = 'E1150004';
    protected $language = 'AZ';
    const PORT = 5443;

    //protected $approveUrl = "http://127.0.0.1:8000/payment";
    //protected $cancelUrl = "http://127.0.0.1:8000/payment/cancelled";
    //protected $declineUrl = "http://127.0.0.1:8000/payment/declined";

    protected $baseUrl = "https://modabu.az";
    protected $approveUrl = "https://api.modabu.az/api/v2/payment";
    protected $cancelUrl = "https://api.modabu.az/api/v2/payment/cancelled";
    protected $declineUrl = "https://api.modabu.az/api/v2/payment/declined";


    public function __construct()
    {
        if (!file_exists($this->cert)) {
            $this->cert = public_path($this->cert);
        } else {
            throw new \Exception("Certificate does not exists: $this->cert");
        }

        if (!file_exists($this->key)) {
            $this->key = public_path($this->key);
        } else {
            throw new \Exception("Key does not exists: $this->key");
        }

        if (!Storage::disk('local')->exists('public/' . 'xml/create_order.xml')) {
            throw new \Exception("File does not exists: xml/create_order.xml");
        }

        if (!Storage::disk('local')->exists('public/' . 'xml/get_status.xml')) {
            throw new \Exception("Certificate does not exists: xml/get_status.xml");
        }
    }

    public function curl($xml)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSLCERT, $this->cert);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, "Modabu.az");
        curl_setopt($ch, CURLOPT_SSLKEY, $this->key);
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, "Modabu.az");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_PORT, self::PORT);
        curl_setopt($ch, CURLOPT_URL, $this->serviceUrl);
        $data = curl_exec($ch);
        if ($data === false) {
            $result = curl_error($ch);
        } else {
            $result = $data;
        }
        curl_close($ch);
        return $result;
    }

    public function createOrder(Order $order, array $coupon)
    {
        //if coupon exists
        $price = $order->payed;
        if (count($coupon)) {
            if ($coupon['coupon']) {
                $discount = $coupon['coupon->discount'];
                $discountType = $coupon['coupon->discount_type'];
                if ($discountType === 0) {
                    $price -= $discount;
                }
                if ($discountType === 1) {
                    $price -= $price * $discount / 100;
                }
            }
        }
        $order_data = array(
            'merchant' => $this->merchant_id,
            'amount' => round($order->payed, 2),
            'currency' => 944,
            'description' => 'Modabu.az - Sifariş ödənişi',
            'lang' => $this->language,
            'order' => $order->id,
        );
        $xml = file_get_contents(storage_path('app/public/xml/create_order.xml'));
        $xmlObject = new SimpleXMLElement($xml);
        $xmlObject->Request->Language = $this->language;
        $xmlObject->Request->Order->Merchant = $this->merchant_id;
        $xmlObject->Request->Order->Amount = round($order->payed, 2) * 100;
        $xmlObject->Request->Order->Currency = $order_data['currency'];
        $xmlObject->Request->Order->Description = $order_data['description'];
        $xmlObject->Request->Order->ApproveURL = $this->approveUrl;
        $xmlObject->Request->Order->CancelURL = $this->cancelUrl;
        $xmlObject->Request->Order->DeclineURL = $this->declineUrl;
        $result = $this->curl($xmlObject->asXML());
        return $this->handleCurlResponse($order_data, $result, $coupon);
    }

    public function handleCurlResponse($inital_data, $data, array $coupon)
    {
        $oXML = new SimpleXMLElement($data);
        if ($oXML->Response->Status == "00") {
            $OrderID = $oXML->Response->Order->OrderID;
            $SessionID = $oXML->Response->Order->SessionID;
            $paymentBaseUrl = $oXML->Response->Order->URL;

            $payment = new Payment();
            $payment->user_id = request()->user('api') ? request()->user('api')->id : null;
            $payment->order_id = $OrderID;
            $payment->session_id = $SessionID;
            $payment->currency = $inital_data['currency'];
            $payment->order_description = $inital_data['description'];
            $payment->amount = $inital_data['amount'];
            $payment->order_check_status = 0;
            $payment->status_code = $oXML->Response->Status;
            $payment->detail = json_encode($coupon);
            $payment->save();

            DB::insert('insert into order_payments (order_id, payment_id) values (?, ?)', array($inital_data['order'], $payment->id));
            return $paymentBaseUrl . "?ORDERID=" . $OrderID . "&SESSIONID=" . $SessionID . "&";
        } else {
            return false;
        }
    }

    public function approveUrl(Request $request)
    {
        if ($request->filled('xmlmsg')) {
            $xmlmsg = new SimpleXMLElement($request->xmlmsg);
            Payment::where(['order_id' => $xmlmsg->OrderID, 'session_id' => $xmlmsg->SessionID])->update(['order_status' => end($xmlmsg->OrderStatus)]);
            return redirect()->to($this->baseUrl . '?payment=successfull');
        }
        abort(404);
    }

    public function cancelUrl(Request $request)
    {
        if ($request->filled('xmlmsg')) {
            $xmlmsg = new SimpleXMLElement($request->xmlmsg);
            Payment::where(['order_id' => $xmlmsg->OrderID, 'session_id' => $xmlmsg->SessionID])->update(['order_status' => end($xmlmsg->OrderStatus)]);
            return redirect()->to($this->baseUrl . '?payment=unsuccessfull');
        }
        abort(404);
    }

    public function declineUrl(Request $request)
    {
        if ($request->filled('xmlmsg')) {
            $xmlmsg = new SimpleXMLElement($request->xmlmsg);
            Payment::where(['order_id' => $xmlmsg->OrderID, 'session_id' => $xmlmsg->SessionID])->update(['order_status' => end($xmlmsg->OrderStatus)]);
            return redirect()->to($this->baseUrl . '?payment=declined');
        }
        abort(404);
    }

    public function getOrderStatus($order_id, $session_id)
    {
        $xml = file_get_contents(storage_path('app/public/xml/get_status.xml'));
        $xmlObject = new SimpleXMLElement($xml);
        $xmlObject->Request->Language = $this->language;
        $xmlObject->Request->Order->Merchant = $this->merchant_id;
        $xmlObject->Request->Order->OrderID = $order_id;
        $xmlObject->Request->SessionID = $session_id;

        $response = $this->curl($xmlObject->asXML());
        $xmlmsg = new SimpleXMLElement($response);
        dd($xmlmsg);

        //$payment = Payment::where(['order_id' => $data->order_id, 'session_id' => $data->session_id])->first();
        //if ($payment) {
        //    $payment->update([
        //        'order_check_status' => $xmlmsg->Response->Order->OrderStatus,
        //        //'status_code' => $xmlmsg->Response->Status,
        //    ]);
        //    if ($xmlmsg->Response->Order->OrderStatus == "APPROVED") {
        //        //
        //    }
        //    return $payment->order_status;
        //}

        return false;
    }
}
