<?php

namespace App\Http\Controllers\V2\Api\Cart;

use App\Models\Color;
use App\Models\OrderForm;
use App\Models\Product;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\V2\Api\Payment\PaymentController;
use App\Http\Requests\PayDirectlyRequest;
use App\Http\Requests\V2\PayDirectlyRequest as V2PayDirectlyRequest;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    use ApiResponser;


    /**
     * Buy now
     *
     * @return success
     */
    public function payDirectly(V2PayDirectlyRequest $request, $id)
    {
        $user = request()->user('api');
        $product = Product::with(['attributes.attribute'])->findOrFail($id);
        if ($product->has('attributes')) {
            if ($product->attributes == '') {
                return $this->sendError('Daxil edilən məlumatlar düzgün deyil.', ['Düzgün seçim edin!'], 422);
            }
            $attributes = json_decode($request->get('attributes'));
            $variation = $this->getProductVariation($product, $attributes);
            if ($variation && $variation->qty < 1) {
                return $this->sendError(null, ['max quantity'], 404);
            }
        }
        try {
            $total = $product->price;
            //if (isset($variation)) {
            //    $total = $variation->price;
            //}
            $discount = $this->getProductDiscount($product);
            $payable = $product->price - $discount;
            $order = Order::query()->create([
                'user_id' => null,
                'total' => $total,
                'discount' => $discount,
                'payed' => $payable,
                'payment_type' => strtolower($request->input('payment_type')),
                'address' => json_encode([
                    'fullname' => $request->input('first_name') . ' ' . $request->input('last_name'),
                    'phone' => $request->input('phone'),
                    'city' => $request->input('city'),
                    'address' => $request->input('address'),
                ]),
                'order_type_id' => 1,
                'payment_type' => $request->input('payment_type') == "cash"  ? "cash" :  "online"
            ]);
            if ($user) {
                $order->user_id = $user->id;
            }
            if (isset($variation)) {
                $variation->update(['qty' => DB::raw('qty - 1')]);
            } else {
                $product->update(['qty' => DB::raw('qty - 1')]);
            }
            OrderDetail::query()->create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'price' => $product->price,
                'discount' => $discount,
                'quantity' => 1,
                'attributes' => $request->get('attributes'),
                'stock' => false
            ]);
            if ($request->input('payment_type') == "cash") {
                return $this->sendResponse(null, 'success', 201);
            } else {
                $data = (new PaymentController())->createOrder($order, []);
                if ($data != false) {
                    return $this->sendResponse(['success' => true, 'url' => $data], '', 201);
                }
                return $this->sendResponse(['success' => false, 'url' => ''], 'Sifariş tamamlanmadı', 500);
            }
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    /**
     * Cart checkout
     *
     * @return success
     */
    public function makeOrder(Request $request)
    {
        $user = request()->user('api');
        if ($user) {
            $address = null;
            $request->validate([
                'address' => ['required_if:address_detail,null', 'integer', Rule::exists('addresses', 'id')->where('user_id', $user->id)],
                'address_detail' => ['required_if:address,null', 'array'],
                'address_detail.firstname' => ['required_if:address,null'],
                'address_detail.lastname' => ['required_if:address,null'],
                'address_detail.address' => ['required_if:address,null'],
                'address_detail.phone' => ['required_if:address,null'],
                'address_detail.city' => ['required_if:address,null', 'exists:cities,id'],
                'payment_type' => ['required', 'in:cash,online']
            ]);

            if (!$request->address && $request->address_detail['save']) {
                $address = Address::create([
                    'user_id' => $user->id,
                    'title' => "Ev",
                    'fullname' => ($request->address_detail['firstname'] . ' ' . $request->address_detail['lastname']),
                    'city' => $request->address_detail['city'],
                    'address' => $request->address_detail['address'],
                    'phone' => $request->address_detail['phone'],
                    'is_default' => (bool)$request->address_detail['save'],
                ]);
            } else {
                $address = Address::firstWhere(['id' => $request->address, 'user_id' => $user->id]);
            }


            //if (!$request->address) {
            //    return response()->json(['address_detail' => $request->address_detail], 400);
            //}

            $cart = Cart::with('items')->firstWhere(['user_id' => $user->id]);
            $errors = array();

            if ($cart) {
                foreach ($cart->items as $item) {
                    $product = Product::with(['attributes.attribute'])->where(['id' => $item->model_id, 'confirmed' => true])->firstOrFail();
                    if ($product->has('attributes')) {
                        $attributes = json_decode($item->attributes);
                        $variation = $this->getProductVariation($product, $attributes);
                        if ($variation && $variation->qty < $item->quantity) {
                            $a = ['item' => $item->id, 'stock' => $variation ? $variation->qty : 0];
                            array_push($errors, $a);
                        }
                    }
                }
                if (!empty($errors)) {
                    return $this->sendError('quantity exceed', $errors, 400);
                }
                $_total = 0;
                $_payable = 0;
                $_discounts = 0;
                foreach ($cart->items as $item) {
                    $product = Product::firstWhere(['id' => $item->model_id]);
                    $discount = (float)$this->getProductDiscount($product) *  $item->quantity;
                    $_variation = $this->getProductVariation($product, $attributes);
                    $_price = $product->price;
                    //if (isset($_variation)) $_price = $_variation->price;
                    $_discounts += $discount;
                    $_payable += ($_price * $item->quantity - $discount);
                    $_total += ($_price * $item->quantity);
                }
                if ($request->input('isUrgent') == true) {
                    $_payable += 3;
                }

                $order = Order::query()->create([
                    'user_id' => $user->id,
                    'total' => $_total,
                    'discount' => $_discounts,
                    'payed' => $_payable,
                    'address' => json_encode([
                        'fullname' => $address ? $address->fullname : ($request->address_detail['firstname'] . ' ' . $request->address_detail['lastname']),
                        'phone' => $address ? $address->phone : $request->address_detail['phone'],
                        'city' => $address ? $address->city : $request->address_detail['city'],
                        'address' => $address ? $address->address : $request->address_detail['address'],
                    ]),
                    'is_urgent' => (bool) $request->input('isUrgent'),
                    'order_type_id' => 2,
                    'payment_type' => $request->input('payment_type') == "cash"  ? "cash" :  "online"
                ]);

                foreach ($cart->items as $item) {
                    $product = Product::with(['attributes.attribute'])->firstWhere(['id' => $item->model_id, 'confirmed' => true]);
                    if ($product->has('attributes')) {
                        $attributes = json_decode($item->attributes);
                        $__variation = $this->getProductVariation($product, $attributes);
                    }
                    if (isset($__variation)) {
                        $__variation->update(['qty' => DB::raw('qty - 1')]);
                    } else {
                        $product->update(['qty' => DB::raw('qty - 1')]);
                    }
                    OrderDetail::query()->create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'price' => $product->price,
                        'discount' => $this->getProductDiscount($product),
                        'quantity' => $item->quantity,
                        'attributes' => $item->attributes,
                        'stock' => false,
                    ]);
                }
                $cart->items()->delete();
                $cart->delete();
                if ($request->input('payment_type') == "cash") {
                    return $this->sendResponse(null, 'Sifariş tamamlandı', 201);
                } else {
                    $data = (new PaymentController())->createOrder($order, []);
                    if ($data != false) {
                        return $this->sendResponse(['success' => true, 'url' => $data], '', 201);
                    }
                    return $this->sendResponse(['success' => false, 'url' => ''], 'Sifariş tamamlanmadı', 500);
                }
            }
            return $this->sendError('Empty Cart', ['Cart is Empty'], 404);
        } else {
            $request->validate([
                'products' => ['required'],
                'products.*.id' => ['required', 'integer', 'exists:products,id'],
                'products.*.qty' => ['required', 'integer'],
                'products.*.attributes' => ['required', 'string'],
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                'phone' => ['required', 'string'],
                'city' => ['required', 'string'],
                'address' => ['required', 'string'],
            ], [
                'products.required' => 'Səbətinizdə minimum 1 məhsul olmalıdır!'
            ]);

            $errors = [];
            $rP = $request->products;
            foreach ($rP as $item) {
                $product = Product::with(['attributes.attribute'])->firstWhere(['id' => $item['id'], 'confirmed' => true]);
                if ($product->has('attributes')) {
                    if ($product->attributes == '') {
                        return $this->sendError('Daxil edilən məlumatlar düzgün deyil.', ['Düzgün seçim edin!'], 422);
                    }
                    $attributes = json_decode($item['attributes']);
                    $variation = $this->getProductVariation($product, $attributes);
                    if ($variation && $variation->qty < $item['qty']) {
                        $a = ['item' => $item->id, 'stock' => $variation ? $variation->qty : 0];
                        array_push($errors, $a);
                    }
                }
            }
            if (!empty($errors)) {
                return $this->sendError('quantity exceed', $errors, 400);
            }
            $total = 0;
            $payable = 0;
            $discounts = 0;
            try {
                foreach ($rP as $item) {
                    $product = Product::firstWhere(['id' => $item['id'], 'confirmed' => true]);
                    $discount = $this->getProductDiscount($product);
                    $discounts += $discount;
                    $payable += $product->price * $item['qty'] - $discount;
                    $total += $product->price * $item['qty'];
                }
                if ($request->input('isUrgent') == true) {
                    $payable += 3;
                }
                $order = Order::query()->create([
                    'user_id' => null,
                    'total' => $total,
                    'discount' => $discounts,
                    'payed' => $payable,
                    'address' => json_encode([
                        'fullname' => $request->input('firstname') . ' ' . $request->input('lastname'),
                        'phone' => $request->input('phone'),
                        'city' => $request->input('city'),
                        'address' => $request->input('address'),
                    ]),
                    'is_urgent' => (bool) $request->input('isUrgent'),
                    'order_type_id' => 3,
                    'payment_type' => $request->input('payment_type') == "cash" ? "cash" : "online"
                ]);
                foreach ($rP as $prdct) {
                    $product = Product::with(['attributes.attribute'])->firstWhere(['id' => $prdct['id'], 'confirmed' => true]);
                    $discount = $this->getProductDiscount($product);
                    if ($product->has('attributes')) {
                        $attributes = json_decode($prdct['attributes']);
                        $variation = $this->getProductVariation($product, $attributes);
                    }
                    if (isset($variation)) {
                        $variation->update(['qty' => DB::raw('qty - 1')]);
                    } else {
                        $product->update(['qty' => DB::raw('qty - 1')]);
                    }
                    OrderDetail::query()->create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'price' => $product->price,
                        'discount' => $discount,
                        'quantity' => $prdct['qty'],
                        'attributes' => $prdct['attributes'],
                        'stock' => false
                    ]);
                }
                if ($request->input('payment_type') == "cash") {
                    return $this->sendResponse(null, 'Sifariş tamamlandı', 201);
                } else {
                    $data = (new PaymentController())->createOrder($order, []);
                    if ($data != false) {
                        return $this->sendResponse(['success' => true, 'url' => $data], '', 201);
                    }
                    return $this->sendResponse(['success' => false, 'url' => ''], 'Sifariş tamamlanmadı', 500);
                }
            } catch (\Exception $e) {
                return $this->sendError('error', [$e->getMessage()]);
            }
        }
    }

    public function getProductVariation(Product $product, $attributes)
    {
        $color = $attributes->color ? Color::firstWhere(['name' => $attributes->color]) : '';
        $variation = Variation::where(['product_id' => $product->id]);
        if ($color) {
            $variation->where(['color_id' => $color->id]);
        }
        if ($attributes->size) {
            $variation->where(['variation_parent' => $attributes->size]);
        }
        return $variation = $variation->first();
    }
}
