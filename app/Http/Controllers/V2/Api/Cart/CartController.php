<?php

namespace App\Http\Controllers\V2\Api\Cart;

use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Color;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CartController extends Controller
{
    use ApiResponser;

    public function __construct(Request $request)
    {
    }

    /**
     * Add to cart
     *
     * @return json
     */

    public function addToCart($id, Request $request)
    {
        //$start = microtime(true);
        $user = request()->user('api');
        abort_if(!$user, 401);
        $product = Product::with('brand')->findOrFail($id);
        $create = true;

        //VALIDATION
        $arr2 = json_decode($request->get('attributes'), true);
        $color = $arr2['color'] ? Color::firstWhere(['name' => $arr2['color']]) : null;
        if ($color == null) return $this->sendError(null, ['color not exists'], 404);

        $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $arr2['size']])->first();
        if (!$variation) {
            return $this->sendError(null, ['size not exists'], 404);
        }

        $_price = (float)$product->price;
        $discount = (float)$this->getProductDiscount($product);
        //if (isset($variation)) $_price = $variation->price;

        $cart = Cart::firstWhere(['user_id' => $user->id]);
        if (!$cart) $cart = Cart::query()->create(['user_id' => $user->id,]);

        $cartItem = CartItem::query()->firstWhere([
            'cart_id' => $cart->id,
            'model_id' => $product->id,
            'attributes' => $request->get('attributes'),
        ]);
        if ($cartItem) {
            if ($cartItem->quantity + 1 > $variation->qty) {
                return $this->sendError(null, ['max quantity'], 404);
            }
            $cartItem->update([
                'name' => $product->title,
                'price' => $_price,
                'quantity' => DB::raw("(1 + quantity)"),
            ]);
            $create = false;
        }

        if ($create) {
            CartItem::create([
                'cart_id' => $cart->id,
                'model_id' => $product->id,
                'model_type' => 'App\Models\Product',
                'name' => $product->title,
                'brand' => $product->brand->name,
                'price' => $product->price,
                'image' => $product->thumbnail,
                'quantity' => 1,
                'attributes' => $request->get('attributes'),
            ]);
        }

        $cart->discount = (float) $cart->discount + $discount;
        $cart->total = (float) $cart->total + $product->price;
        $cart->payable = (float) $cart->payable + ($_price - $discount);
        $cart->save();

        //return $time_elapsed_secs = microtime(true) - $start;
        return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
    }

    public function getCart()
    {
        $user = request()->user('api');
        $cart = Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        if ($cart) {
            return $cart;
        } else {
            return $this->sendError('cart is empty', [''], Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * Remove from cart
     *
     * @return json
     */
    public function removeFromCart(int $id, Request $request)
    {
        $user = request()->user('api');
        $discount = 0;

        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart) {
                if ($cart->items()->count() == 1) {
                    $cartItem->delete();
                    $cart->delete();
                } else {
                    $cart->discount = (float) $cart->discount - $discount * $cartItem->quantity;
                    $cart->total = (float) $cart->total - $product->price * $cartItem->quantity;
                    $cart->payable = (float) $cart->payable - $price * $cartItem->quantity;
                    $cart->save();
                    $cartItem->delete();
                }
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Increment cart item quantity
     *
     * @return json
     */
    public function incrementCartItem(int $id)
    {
        $user = request()->user('api');
        $discount = 0;
        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $attributes = json_decode($cartItem->attributes, true);
            $color = $attributes['color'] ? Color::firstWhere(['name' => $attributes['color']]) : null;
            $variation = Variation::where(['product_id' => $product->id, 'color_id' => $color->id, 'variation_parent' => $attributes['size']])->first();
            if (!$variation || $cartItem->quantity + 1 > $variation->qty) {
                return $this->sendError(null, ['max quantity'], 404);
            }
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart && $cartItem) {
                $cart->discount = (float) $cart->discount + $discount;
                $cart->total = (float) $cart->total + $product->price;
                $cart->payable = (float) $cart->payable + $price;
                $cart->save();
                $cartItem->quantity += 1;
                $cartItem->save();
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Decrement cart item quantity
     *
     * @return json
     */
    public function decrementCartItem(int $id)
    {
        $user = request()->user('api');
        $discount = 0;
        $cartItem = CartItem::with(['product', 'cart'])->where(['id' => $id])->firstOrFail();
        $cart = $cartItem->cart;
        if ($cart->user_id == $user->id) {
            $product = $cartItem->product;
            $price = $product->price;
            if ($product->discount_type == 1) {
                $discount += $product->discount;
                $price -= $product->discount;
            } else {
                $discount += $price * $product->discount / 100;
                $price -= $price * $product->discount / 100;
            }
            if ($cart && $cartItem && $cartItem->quantity > 1) {
                $cart->discount = (float) $cart->discount - $discount;
                $cart->total = (float) $cart->total - $product->price;
                $cart->payable = (float) $cart->payable - $price;
                $cart->save();
                $cartItem->quantity -= 1;
                $cartItem->save();
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, ['invalid cart id'], 500);
    }

    /**
     * Clear Cart
     *
     * @return json
     */
    public function clearCart()
    {
        $user = request()->user('api');
        return Cart::with('items')->firstWhere(['user_id' => $user->id])->delete();
    }

    public function addToCartAll(Request $request)
    {
        $user = request()->user('api');
        $products = $request->get('products');
        if (is_array($products) && !empty($products)) {
            foreach ($products as $item) {
                $product = Product::with('brand')->findOrFail($item['id']);
                $create = false;
                $discount = (float)(new OrderController())->getProductDiscount($product);
                $price = $product->price;
                //if (isset($variation)) $price = $variation->price;

                $cart = Cart::firstWhere(['user_id' => $user->id]);
                if (!$cart) $cart = Cart::query()->create(['user_id' => $user->id,]);
                $cartItem = CartItem::query()->firstWhere([
                    'cart_id' => $cart->id,
                    'model_id' => $product->id,
                ]);
                if ($cartItem) {
                    $arr1 = json_decode($cartItem->attributes, true);
                    $arr2 = json_decode($item['attributes'], true);
                    if (is_array($arr1) && is_array($arr2) && (bool) array_diff($arr1, $arr2) === false) {
                        $cartItem->update([
                            'name' => $product->title,
                            'price' => $product->price,
                            'quantity' => DB::raw("(" . $item['qty'] . " + quantity)"),
                        ]);
                    } else {
                        $create = true;
                    }
                } else {
                    $create = true;
                }
                if ($create) {
                    CartItem::create([
                        'cart_id' => $cart->id,
                        'model_id' => $product->id,
                        'model_type' => 'App\Models\Product',
                        'name' => $product->title,
                        'brand' => $product->brand->name,
                        'price' => $price,
                        'image' => $product->thumbnail,
                        'quantity' => $item['qty'],
                        'attributes' => $item['attributes'],
                    ]);
                }

                $cart->discount = (float) $cart->discount + $discount * $item['qty'];
                $cart->total = (float) $cart->total + $product->price * $item['qty'];
                $cart->payable = (float) $cart->payable + ($price - $discount) * $item['qty'];
                $cart->save();
            }
            return Cart::with(['items.product'])->firstWhere(['user_id' => $user->id]);
        }
        return $this->sendError(null, 'invalid request', 500);
    }
}
