<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\File;
use App\Models\ProductColor;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use \App\Http\Requests\V2\ColorRequest;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::query()
            ->orderBy('created_at', 'desc');

        $colors = app(Pipeline::class)
            ->send($colors)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.color.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColorRequest $request, Color $color)
    {
        $data = $request->except('_token');
        $color->fill($data);
        $color->save();
        return redirect(route('colors.index'))->with('success', 'Əməliyyat uğurla Insert Oldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param Color $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Color $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Color $color
     * @return \Illuminate\Http\Response
     */
    public function update(ColorRequest $request, Color $color)
    {
        $data = $request->except('_token');
        $color->fill($data);
        $color->update();
        return redirect(route('colors.index'))->with('success', 'Əməliyyat uğurla Update olundu.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Color $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        ProductColor::where('color_id', $color->id)->delete();
        $color->delete();
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            ProductColor::whereIn('color_id', $array)->delete();
            Color::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
