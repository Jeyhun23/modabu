<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\CategoryRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::where('parent_id', 0)->orderBy('parent_id')->with('children')->get();
        return view('admin.v2.category.index', compact('category'));
    }

    public function create()
    {
        $category = Category::where('parent_id', 0)->orderBy('parent_id')->with(['children2' => function ($query) {
            $query->select('id', 'parent_id', 'name');
        }])->select('id', 'parent_id', 'name')->get();

        return view('admin.v2.category.create', compact('category'));
    }

    public function store(CategoryRequest $request)
    {
        $file_name = null;
        if (request()->file('banner')) {
            $file_name = \App\Models\File::storeFile('public/categories', $request->file("banner"));
        }

        abort_if(request('parent_id') && !Category::where('id', request('parent_id'))->first(), 404);
        $slug = Str::slug(request('name'));
        $dub = Category::where('slug', $slug)->first();
        if ($dub) {
            return redirect()->back()->with(['danger' => "Bu ad artiq istifade olunub"], 403);
        }
        Category::create([
            'parent_id' => request('parent_id') ?? 0,
            'banner' => $file_name,
            'name' => request('name'),
            'slug' => $slug,
            'meta_title' => request('meta_title'),
            'meta_desc' => request('meta_desc'),
            'sort' => request('sort'),
            'published' => request()->has('published') ? (request('published') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('category.index');
    }

    public function update($category, CategoryRequest $request)
    {
        $category = Category::findOrFail($category);

        $file_name = $category->banner;
        if (request()->file('banner')) {
            \App\Models\File::deleteFile(public_path('storage/categories/' . $file_name));
            $file_name = \App\Models\File::storeFile('public/categories', $request->file("banner"));
        }

        abort_if(request('parent_id') && !Category::where('id', request('parent_id'))->first(), 404);

        $slug = Str::slug(request('name'));
        $dub = Category::where('slug', $slug)->where('id', '!=', $category->id)->first();
        if ($dub) {
            return redirect()->back()->with(['danger' => "Bu ad artiq istifade olunub"], 403);
        }
        $category->update([
            'parent_id' => request('parent_id') ?? 0,
            'banner' => $file_name,
            'name' => request('name'),
            'slug' => $slug,
            'meta_title' => request('meta_title'),
            'meta_desc' => request('meta_desc'),
            'sort' => request('sort'),
            'published' => request()->has('published') ? (request('published') == 'on' ? true : false) : false,
        ]);
        return redirect()->route('category.index');
    }

    public function edit(Category $category)
    {
        $categorys = Category::where('parent_id', 0)->orderBy('parent_id')->with(['children2' => function ($query) {
            $query->select('id', 'parent_id', 'name');
        }])->select('id', 'parent_id', 'name')->get();
        return view('admin.v2.category.edit')->with(['cat' => $category, 'category' => $categorys]);
    }

    public function destroy($id)
    {
        $cat = Category::where('id', $id)->orderBy('parent_id')->with(['children2' => function ($query) {
            $query->select('id', 'parent_id', 'name');
        }])->first();

        if ($cat) {
            \App\Models\File::deleteFile(public_path('storage/categories/' . $cat->banner));
        }

        if (count($cat->children)) {
            $this->foorCategoryDelete($cat->children);
        }
        Product::where('category_id', $cat->id)->update([
            'category_id' => 0
        ]);

        $cat->delete();
        return redirect()->route('category.index');
    }

    public function update_category_visibility(Category $category)
    {
        $category->update([
            'published' => !$category->published
        ]);
        return redirect()->route('category.index');
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            foreach ($array as $i) {
                $cat = Category::where('id', $i)->with(['children2' => function ($query) {
                    $query->select('id', 'parent_id', 'banner');
                }])->first();
                if ($cat) {
                    \App\Models\File::deleteFile(public_path('storage/categories/' . $cat->banner));
                }
                if (count($cat->children)) {
                    $this->foorCategoryDelete($cat->children);
                }
                $cat->delete();
            }
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    function foorCategoryDelete($items)
    {
        foreach ($items as $item) {
            \App\Models\File::deleteFile(public_path('storage/categories/' . $item->banner));
            Product::where('category_id', $item->id)->update([
                'category_id' => 0
            ]);
            if (count($item->children)) {
                $this->foorCategoryDelete($item->children);
            }
            $item->delete();
        }
    }
}
