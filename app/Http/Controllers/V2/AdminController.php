<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    private $pac = "Modabu Personal Access Client";

    public function dashboard()
    {
        return view('admin.v2.dashboard');
    }

    public function client_login(User $user)
    {
        $tokenResult = $user->createToken($this->pac);

        return Redirect::to('https://modabu.az/?token_client_access=' . $tokenResult->accessToken);
    }
}
