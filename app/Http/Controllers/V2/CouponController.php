<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\CouponsRequest;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::query()
            ->orderBy('created_at', 'desc');

        $coupons = app(Pipeline::class)
            ->send($coupons)
            ->through([
                \App\QueryFilters\Title::class,
                \App\QueryFilters\Code::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.v2.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CouponsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponsRequest $request)
    {
        $coupon = Coupon::create($request->all());
        return redirect()->route('coupons.index', ['success' => "Ugurla Əlava edildi"]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Coupon $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Coupon $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        return view('admin.v2.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Coupon $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(CouponsRequest $request, Coupon $coupon)
    {
        if ($request->coupon_type == 'expire_date') {
            $request->merge([
                'count' => null,
            ]);
        } else {
            $request->merge([
                'expire_date' => null,
            ]);
        }
        $coupon->update($request->all());
        return redirect()->route('coupons.index', ['success' => "Ugurla Update edildi"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Coupon $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            Coupon::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
