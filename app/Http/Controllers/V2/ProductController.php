<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Models\Attribute;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\File;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeVariation;
use App\Models\ProductColor;
use App\Models\ProductCombine;
use App\Models\Refler;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{

    use  ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['colors', 'colors.color', 'attributes', 'brand', 'images','category','shelf'])
            ->orderByDesc('created_at');

        $products = app(Pipeline::class)
            ->send($products)
            ->through([
                \App\QueryFilters\Title::class,
                \App\QueryFilters\Brand::class,
                \App\QueryFilters\Category::class,
                \App\QueryFilters\Shelf::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        $brands = Brand::select('id','name')->get();
        $categories = Category::select('id','name')->get();
        $shelfes = Refler::all();
        return  view('admin.v2.products.index',compact('products','brands','categories','shelfes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::select('id','name')->wherePublished(1)->get();
        $categories = Category::with(['children'])->withCount(['children'])->where(['parent_id' => 0])->get();
        $attributes = Attribute::all();
        $colors = Color::all();
        $shelfes = Refler::all();
        return  view('admin.v2.products.create',compact('brands','categories','attributes','colors','shelfes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $colors = $request->get('colors');
        $images = $request->get('images');
        $attributes = $request->get('attributes');
        $variations = $request->get('variations');
        $prices = $request->get('prices');
        $qtys = $request->get('qtys');

        $meta = $request->get('meta');
        $product = Product::query()->create(['user_id' => auth()->user()->id, 'confirmed' => false]);
        $path = null;

        try {
            if ($request->file("thumbnail")) {
                $path = File::storeFile('public/products/thumbnail', $request->file("thumbnail"));
            }
            $product->thumbnail = '/products/thumbnail/' . $path;
            $product->category_id = $request->get('category');
            $product->brand_id = $request->get('brand');
            $product->ref_id = $request->get('shelf');
            $product->title = $request->get('title');
            $product->description = $request->get('description');
            $product->meta = json_encode($meta);
            $product->price = $request->get('price');
            $product->qty = $request->get('qty');
            $product->discount = $request->get('discount');
            $product->discount_type = $request->get('discount_type');
            $product->featured = 0;
            $product->save();
            $product->slug = Str::slug($request->get('title')) . $product->id;
            $product->save();

            $products = $request->products ? json_decode($request->products,true) : false;
            if ($products) {
                array_push($products,['value' => $product->id]);
                $this->productCombine($products);
            }

            if ($images) {
                $images = explode(',', $images);
                File::whereIn('id', $images)->update([
                    'p_id' => $product->id,
                ]);
            }
            $this->resize($product);

            if (is_array($colors) && count($colors) > 0) {
                foreach ($colors as $color) {
                    ProductColor::query()->insert([
                        'product_id' => $product->id,
                        'color_id' => $color,
                    ]);
                }
            }
            if (is_array($attributes) && count($attributes) > 0) {
                foreach ($attributes as $attribute) {
                    $pa = ProductAttribute::query()->insertGetId([
                        'product_id' => $product->id,
                        'attribute_id' => $attribute,
                    ]);
                    $vars = json_decode($variations[$attribute]);
                    if (count($vars) > 0) {
                        foreach ($vars as $var) {
                            ProductAttributeVariation::query()->insert([
                                'product_attribute_id' => $pa,
                                'name' => strtoupper($var->value),
                            ]);
                        }
                    }
                }
            }
            $__v = array();
            if ($colors && count($colors) > 0) {
                foreach ($colors as $i => $color) {
                    $firstAttribute = $attributes[0] ?? null;
                    $fv = json_decode($variations[$firstAttribute]);
                    if (isset($firstAttribute) && count($fv) > 0) {
                        foreach ($fv as $key => $firstVariation) {
                            if ($attributes && count($attributes) > 1) {
                                $secondAttribute = $attributes[1];
                                $sv = json_decode($variations[$secondAttribute]);
                                foreach ($sv as $kk => $secondVariation) {
                                    array_push($__v, [
                                        'color' => (int) $color,
                                        'attribute' => (int) $firstAttribute,
                                        'parent' => $firstVariation->value,
                                        'child' => $secondVariation->value,
                                        'price' => (float) $prices[$firstAttribute][$key][$kk],
                                        'qty' => (int) $qtys[$firstAttribute][$key][$kk],
                                    ]);
                                }
                            } else {
                                array_push($__v, [
                                    'color' => (int) $color,
                                    'parent' => $firstVariation->value,
                                    'attribute' => (int) $firstAttribute,
                                    'child' => 0,
                                    'price' => (float) $prices[$firstAttribute][$key],
                                    'qty' => (int) $qtys[$firstAttribute][$key],
                                ]);
                            }
                        }
                    }
                }
            } else {
                //comment
            }
            if (count($__v) >= 1) {
                foreach ($__v as $v) {
                    Variation::query()->insert([
                        'product_id' => $product->id,
                        'color_id' => $v['color'],
                        'attribute_id' => $v['attribute'],
                        'variation_parent' => $v['parent'],
                        'variation_child' => $v['child'],
                        'sku' => $v['color'] . 'xx' . $v['attribute'] . 'xx' . $v['parent'] . 'xx' . $v['child'],
                        'price' => $v['price'] ?? $request->get('price'),
                        'qty' => $v['qty'],
                    ]);
                }
            }

            return $this->sendResponse(null, 'success', 201);
        }catch (\Exception $e){
            return $this->sendError(null, $e->getMessage());
        }
    }

    public function resize(Product $product)
    {
        $k = 0;

        $products = Product::get();
        //foreach ($products as $product) {
        $thumbnail = $product->thumbnail;
        $exists = Storage::disk('local')->exists('/public' . $thumbnail);
        if ($exists) {
            $k++;
            $fileName = explode("/", $thumbnail);
            $fileName = explode(".", end($fileName));

            $size = Storage::disk('local')->size('/public' . $thumbnail);
            $file = Storage::disk('local')->get('/public' . $thumbnail);
            $img = Image::make($file)->resize(264, 362, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save(storage_path('app/public/products/264x362/') . $fileName[0] . '.' . $fileName[1], 90);
//            echo "Before: " . $size . " Current: " . $img->filesize() . "<br>";
        }
        //}

        //$products = Product::where('id', '>', $down)->where('id', '<=', $up)->get();
        //foreach ($products as $product) {
        $images = $product->images;
        foreach ($images as $image) {
            $exists = Storage::disk('local')->exists('/public/' . $image->path);
            if ($exists) {
                $k++;
                $fileName = explode("/", $image->path);
                $fileName = explode(".", end($fileName));

                $size = Storage::disk('local')->size('/public/' . $image->path);
                $file = Storage::disk('local')->get('/public/' . $image->path);
                $img = Image::make($file)->resize(373, 560, function ($constraint) {
                    $constraint->aspectRatio();
                    //$constraint->upsize();
                });
                $img->save(storage_path('app/public/products/373x560/') . $fileName[0] . '.' . $fileName[1], 100);
//                echo "Before: " . $size . " Current: " . $img->filesize() . "<br>";
            }
        }
        //}
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load(['colors', 'attributes', 'images', 'variations']);
        $brands = Brand::select('id','name')->wherePublished(1)->get();
        $categories = Category::with(['children'])->withCount(['children'])->where(['parent_id' => 0])->get();
        $attributes = Attribute::all();
        $colors = Color::all();
        $shelfes = Refler::all();
        $productCombines = ProductCombine::where('source_product',$product->id)->orWhere('target_product',$product->id)->get();
        $productColors = $product->colors->pluck(['color_id'])->toArray();
        $productAttributes = $product->attributes->pluck(['attribute_id'])->toArray();
        return  view('admin.v2.products.edit',compact('product','brands','categories','attributes','colors','shelfes','productColors','productAttributes','productCombines'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $products = $request->combineProducts ? json_decode($request->combineProducts,true) : false;
        if ($products) {
            $this->productCombine($products);
        }
        return $this->sendResponse(null, 'success', 200);
    }

    public function productCombine($products)
    {
        $products = combineProducts($products);
        try {
            foreach ($products as $product) {
                ProductCombine::updateOrCreate(
                    ['source_product' => $product['source']['value'], 'target_product' => $product['target']['value']],
                    ['source_product' => $product['source']['value'], 'target_product' => $product['target']['value']]
                );
            }
        } catch (\Exception $e){

        }

    }

    public function removeProductFromCombine(Request $request)
    {

        ProductCombine::where('source_product',$request->productId)->orWhere('target_product',$request->productId)->delete();
        return response()->json(['data' => translate('success')], 200);
    }

    public function productsStatus($id)
    {
        $product = Product::findOrFail($id);
        $product->confirmed = $product->confirmed ? 0 : 1;
        $product->update();

        return response()->json(['data' => translate('success')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
