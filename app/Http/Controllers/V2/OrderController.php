<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Color;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderForm;
use App\Models\OrderStatus;
use App\Models\User;
use App\Models\Variation;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function __construct()
    {
        //        anbar_permission();
    }

    public function index()
    {
        $orders = Order::query()->orderBy('created_at', 'desc');
        if (auth()->user()->roles[0]->id == 3) {
            abort_if(!in_array(request('order_status'), [3, 4, 7]), 404);
            abort_if(!request()->has('order_status'), 404);
            $orders->with([
                'detail.parentStatus', 'detail.product',
                'detail.product.shelf', 'user',
                'detail' => function ($detail) {
                    if (request('order_status') != '') {
                        $detail->where('status', request('order_status'));
                    }
                    $detail->where('kuryer_id', \auth()->id());
                }, 'payment.detail'
            ])
                ->whereHas('detail', function ($q) {
                    if (request('order_status') != '') {
                        $q->where('status', request('order_status'));
                    }
                    $q->where('kuryer_id', \auth()->id());
                });
        } else {
            $orders = Order::query()
                ->orderBy('created_at', 'desc')
                ->with([
                    'detail.parentStatus', 'detail.product',
                    'detail.product.shelf', 'user', 'payment.detail',
                    'detail' => function ($detail) {
                        if (request('order_status') != '') {
                            $detail->where('status', request('order_status'));
                        }
                    }
                ])
                ->whereHas('detail', function ($query) {
                    if (request('order_status') != '') {
                        $query->where('status', request('order_status'));
                    }
                });
        }
        $orders = app(Pipeline::class)
            ->send($orders)
            ->through([
                \App\QueryFilters\OrderType::class,
                \App\QueryFilters\OrderNumber::class,
                \App\QueryFilters\User::class,
                \App\QueryFilters\OrderPaymentType::class,
                \App\QueryFilters\OrderStatus::class,
                \App\QueryFilters\CreateDate::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());
//        dd($orders);
        $orderStatuses = OrderStatus::orderBy('id')->get();
        $kuryers = User::whereHas('roles', function ($q) {
            $q->where('role_id', 3);
        })->get();
        return view('admin.v2.orders.index', compact('orders', 'orderStatuses', 'kuryers'));
    }

    public function showDetail($orderId, $type)
    {
        $city = null;
        if ($type == 1) {
            $order = Order::select('address')->findOrFail($orderId);
            $city = City::select('name')->find($order->address['city']);
        } else {
            $order = OrderDetail::select('attributes')->findOrFail($orderId);
        }
        return view('admin.v2.orders.orderDetailData', compact('order', 'city', 'type'));
    }

    public function updateOrderDetailStatus(Request $request)
    {
        if (Auth::user()->roles[0]->pivot->role_id == 1) {
            $request->validate([
                'order_detail_id' => 'required|integer',
                'status' => 'required|integer|in:0,1,2,3,4,5,6,7',
                'kuryer_id' => 'nullable|required_if:status,3|integer',
                'kuryer_amount' => 'nullable|required_if:status,3|between:0,99999.99',
                'note' => 'nullable'
            ]);

            if ($request->status == 3) {
                $kuryer = User::where('id', $request->kuryer_id)->whereHas('roles', function ($q) {
                    $q->where('id', 3);
                })->first();
                if (!$kuryer) {
                    return ['errors' => ['kuryer_id' => ["Bele bir Kuryer Yoxdur"]]];
                }
            }
            $orderDetail = OrderDetail::findOrFail($request->order_detail_id);
            $orderDetail->status = $request->status;
            switch ($request->status) {
                case 5:
                    if (!$request->has('reason_id1')) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!$request->reason_id1) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!array_key_exists($request->reason_id1, Order::ORDER_CANCEL_STATUSES)) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } else {
                        $orderDetail->reason_id = $request->reason_id1;
                    }
                    break;
                case 6:
                    if (!$request->has('reason_id2')) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!$request->reason_id2) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!array_key_exists($request->reason_id2, Order::ORDER_REJECT_STATUSES)) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } else {
                        $orderDetail->reason_id = $request->reason_id2;
                    }
                    break;
                case 7:
                    if (!$request->has('reason_id3')) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!$request->reason_id3) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } elseif (!array_key_exists($request->reason_id3, Order::ORDER_REFUND_STATUSES)) {
                        return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                    } else {
                        $orderDetail->reason_id = $request->reason_id3;
                    }
                    break;
            }
            if ($request->status == 3) {
                $orderDetail->kuryer_id = $request->kuryer_id;
                $orderDetail->kuryer_amount = $request->kuryer_amount;
//                $orderDetail->kuryer_note = $request->note;
//                $orderDetail->kuryer_status = $request->kuryer_status;
            }
//            elseif (!in_array($request->status, [4, 5,6,7])) {
//                $orderDetail->kuryer_id = null;
//                $orderDetail->kuryer_amount = null;
//                $orderDetail->kuryer_note = null;
//                $orderDetail->kuryer_status = null;
//                $orderDetail->reason_id = null;
//                $orderDetail->kuryer_reason_id = null;
//            }
            $orderDetail->update();
            return response()->json(['success' => 'Əməliyyat uğurla tamamlandı.']);
        } elseif (Auth::user()->roles[0]->pivot->role_id == 3) {
            $request->validate([
                'status' => 'required|integer',
                'order_detail_id' => 'required|integer',
                'kuryer_status' => 'required|integer|in:1,2,3,4',
                'note' => 'nullable'
            ]);
            $orderDetail = OrderDetail::findOrFail($request->order_detail_id);
            $orderDetail->status = $request->status;
            if ($request->status == 7) {
                if (!$request->has('reason_id3')) {
                    return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                } elseif (!$request->reason_id3) {
                    return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                } elseif (!array_key_exists($request->reason_id3, Order::ORDER_REFUND_STATUSES)) {
                    return ['errors' => ['reason_id' => ["Sebebi qedy edin"]]];
                } else {
                    $orderDetail->kuryer_reason_id = $request->reason_id3;
                }
            } else {
                $orderDetail->kuryer_reason_id = 0;
            }
            $orderDetail->kuryer_status = $request->kuryer_status;
            $orderDetail->kuryer_note = $request->note ? $request->note : $orderDetail->note;
            $orderDetail->update();
            return response()->json(['success' => 'Əməliyyat uğurla tamamlandı.']);
        } else {
            abort(404);
        }
    }

    public function updateStock(Request $request)
    {
        //        return $request->all();
        $type = $request->type;
        if ($type == "order") {
            $order = OrderDetail::find($request->id);
        } else {
            $order = OrderForm::find($request->id);
        }
        $stock = $order->stock;
        if ($type == "order") {
            $attrs = $order->attributes;
            if ($attrs) {
                $color = Color::firstWhere('name', $attrs['color']);
                $size = $attrs['size'];
                if ($color && $size) {
                    $sku = $color->id . 'xx' . 1 . 'xx' . $size;
                    $variation = Variation::where([
                        'product_id' => $order->product_id,
                        'color_id' => $color->id,
                    ])->where('sku', 'like', "%{$sku}%")->first();
                    if ($variation) {
                        $variation->update(['qty' => $stock ? ($variation->qty - $order->quantity) : ($variation->qty + $order->quantity)]);
                    }
                }
            }
        } else {
            $attrs = $order->attributes;
            if ($attrs) {
                $color = Color::firstWhere('name', $attrs['color']);
                $size = $attrs['size'];
                if ($color && $size) {
                    $sku = $color->id . 'xx' . 1 . 'xx' . $size;
                    $variation = Variation::where([
                        'product_id' => $order->product_id,
                        'color_id' => $color->id,
                    ])->where('sku', 'like', "%{$sku}%")->first();
                    if ($variation) {
                        $qty = $order->quantity ?: 1;
                        $variation->update(['qty' => $stock ? ($variation->qty - $qty) : ($variation->qty + $qty)]);
                    }
                }
            }
        }
        return $order->update(['stock' => DB::raw("(1 - stock)")]);
    }

    public function deleteOrderDetail($detailId)
    {
        OrderDetail::findOrFail($detailId)->delete();
        return back()->with('success', 'Əməliyyat uğurla tamamlandı.');
    }
}
