<?php

namespace App\Http\Controllers\V2\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\Auth\LoginRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /**
     * Show login form
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function login()
    {
        return view('admin.v2.account.login');
    }

    public function loginUser(LoginRequest $request)
    {
        if (auth()->attempt($request->only('email', 'password'))) {
            $user = User::find(auth()->user()->id);
            if ($user->getInAdminroles()) {
                $role = $user->roles[0]->id;

                if ($role == 1) {
                    return redirect(route('orders'));
                } elseif ($role == 5) {
                    return redirect(route('products.index'));
                } elseif ($role == 3) {
                    return redirect(route('orders') . '?order_status=3');
                } elseif ($role == 4) {
                    return redirect(route('orders'));
                } else {
                    return redirect()->back()->withErrors(['authFail' => trans('auth.failed')]);
                }
            } else {
                auth()->logout();
            }
        }
        return redirect()->back()->withErrors(['authFail' => trans('auth.failed')]);
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }

    public function forgot_password(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        $user = User::where('email', $request->email)->has('roles')->first();
        if (!$user) {
            return redirect()->back()->withErrors(['email' => "Bele bir email tapilmadi"]);
        }
        $token = encode_hash($user->email . ',' . time());

        $user->update([
            'forgot_token' => $token
        ]);

        Mail::send(['text' => 'admin.v2.mail.forgot'], ['token' => $token], function ($message) use ($user) {
            $message->to($user->email, 'Modabu Admin Panel')->subject('Forgot Password');
            $message->from(config('admin.mail.MAIL_USERNAME'), config('admin.mail.MAIL_FROM_NAME'));
        });

        return redirect()->route('login')->with(['mail_send' => true, "message" => "Mail Succesfully sended!"]);
    }

    public function get_forgot_password()
    {
        return view('admin.v2.account.forgot_password');
    }

    public function refresh_password(Request $request)
    {
        abort_if(!$request->has('token'), 404);
        abort_if(!$request->token, 404);
        $full_token = decode_hash($request->token);
        if ($full_token) {
            $array = explode(',', $full_token);
            if (array_key_exists(1, $array)) {
                $now = time();
                if (($array[1] + 3600) >= $now) {
                    $user = User::where('email', $array[0])->has('roles')->first();
                    if ($user) {
                        if ($user->forgot_token) {
                            if (decode_hash($user->forgot_token) == decode_hash($request->token)) {
                                return view('admin.v2.account.reset_password')->with(['token' => $request->token, 'email' => $array[0]]);
                            }
                        }
                    }
                }
            }
        }
        abort(404);
    }

    public function refresh_password_post(Request $request)
    {
        abort_if(!$request->has('token'), 404);
        abort_if(!$request->token, 404);
        $full_token = decode_hash($request->token);
        if ($full_token) {
            $array = explode(',', $full_token);
            if (array_key_exists(1, $array)) {
                $now = time();
                if (($array[1] + 3600) >= $now) {
                    $user = User::where('email', $array[0])->has('roles')->first();
                    if ($user) {
                        if ($user->forgot_token) {
                            if (decode_hash($user->forgot_token) == decode_hash($request->token)) {
                                if ($request->password == $request->confirm_password) {
                                    $user->update([
                                        'password' => Hash::make($request->password),
                                        'forgot_token' => ''
                                    ]);
                                    return redirect()->route('login')->with(['password_success' => true, 'message' => "Password Successfully Changed!"]);
                                } else {
                                    return redirect()->back()->withErrors(['error' => "Passwords not match"]);
                                }
                            }
                        }
                    }
                }
            }
        }
        abort(404);
    }
}
