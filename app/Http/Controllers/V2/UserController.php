<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\UserRequest;
use App\Models\User;
use Illuminate\Pipeline\Pipeline;
use phpseclib\Crypt\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::query()->whereDoesntHave("roles")
            ->orderBy('created_at', 'desc')
            ->withCount(['orders']);

        $users = app(Pipeline::class)
            ->send($users)
            ->through([
                \App\QueryFilters\Name::class,
                \App\QueryFilters\CreateDate::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.v2.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User;
        $request->merge([
            'password' => \Illuminate\Support\Facades\Hash::make($request->password)
        ]);
        $user->fill($request->all());
        $user->save();
        return redirect(route('users.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.v2.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->only('name');
        if (!is_null($request->email)) {
            $data['email'] = $request->email;
        }
        if (!is_null($request->phone)) {
            $data['phone'] = $request->phone;
        }
        if (!is_null($request->password)) {
            $data['password'] = \Illuminate\Support\Facades\Hash::make($request->password);
        }
        $user->fill($data);
        $user->update();
        return redirect(route('users.index'))->with('success', 'Əməliyyat uğurla tamamlandı.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return request()->ajax() ?
            response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200) :
            back()->with('success', 'Əməliyyat uğurla tamamlandı.');
    }
}
