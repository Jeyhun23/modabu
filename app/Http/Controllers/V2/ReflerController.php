<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\ReflerRequest;
use App\Models\Product;
use App\Models\Refler;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class ReflerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reflers = Refler::query()
            ->orderBy('created_at', 'desc');

        $reflers = app(Pipeline::class)
            ->send($reflers)
            ->through([
                \App\QueryFilters\Name::class,
            ])
            ->thenReturn()
            ->paginate(getPaginationLimit());

        return view('admin.v2.refler.index', compact('reflers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReflerRequest $request, Refler $refler)
    {
        $data = $request->except('_token');
        $refler->fill($data);
        $refler->save();
        return redirect(route('refler.index'))->with('success', 'Əməliyyat uğurla Insert Oldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Refler $refler
     * @return \Illuminate\Http\Response
     */
    public function show(Refler $refler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Refler $refler
     * @return \Illuminate\Http\Response
     */
    public function edit(Refler $refler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Refler $refler
     * @return \Illuminate\Http\Response
     */
    public function update(ReflerRequest $request, Refler $refler)
    {
        $data = $request->except('_token');
        $refler->fill($data);
        $refler->update();
        return redirect(route('refler.index'))->with('success', 'Əməliyyat uğurla Update olundu.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Refler $refler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Refler $refler)
    {
        Product::where('ref_id', $refler->id)->update([
            'ref_id' => null
        ]);
        $refler->delete();
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }

    public function destroyAllSelections(Request $request)
    {
        $request->validate(['keys' => 'present']);
        if ($request->keys) {
            $array = explode(',', $request->keys);
            Product::whereIn('ref_id', $array)->update([
                'ref_id' => null
            ]);
            Refler::whereIn('id', $array)->delete();
        }
        return response()->json(['message' => 'Əməliyyat uğurla tamamlandı.'], 200);
    }
}
