<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    public function index()
    {
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            if ($user->getIsAdminAttribute()) {
                return Redirect::route('admin.dashboard');
            } else {
                Auth::logout();
            }
        }
        return view('admin.pages.account.login');
    }

    public function login(Request $request)
    {
        $email = $request->username;
        $password = $request->password;
        $user = User::where(['email' => $email])->first();
        if ($user && $user->getIsAdminAttribute()) {
            if (Hash::check($password, $user->password)) {
                Auth::login($user, true);
                return redirect()->route('admin.dashboard');
            }
        }

        $return = array(
            'message' => __('wrong crendentials'),
        );
        return back()->withErrors($return);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return Redirect::route('admin.login');
    }

}