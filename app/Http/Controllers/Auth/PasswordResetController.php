<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\Auth\PasswordResetRequest;
use App\Notifications\Auth\PasswordResetSuccess;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    use ApiResponser;
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|max:255|exists:users,email',
        ]);
        $user = User::where('email', $request->email)->first();
        try {
            $passwordReset = PasswordReset::updateOrCreate(
                [
                    'email' => $user->email,
                ],
                [
                    'email' => $user->email,
                    'token' => rand(100000, 999999),
                ]
            );
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
            return $this->sendResponse(null, 'We have e-mailed your password reset link!', 201);
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset || Carbon::parse($passwordReset->updated_at)->addMinutes(180)->isPast()) {
            $passwordReset->delete();
            return $this->sendError(null, 'This password reset token is invalid.', 404);
        }
        return $this->sendResponse($passwordReset, '');
    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|exists:users,email',
            'password' => ['required', 'string', 'min:7', 'regex:/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/'],
            'token' => 'required|string',
        ]);
        $user = User::where('email', $request->email)->first();
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['user_id', $user->id],
        ])->first();

        if (!$passwordReset) {
            return $this->sendError(null, 'This password reset token is invalid.', 404);
        }
        try {
            $user->password = bcrypt($request->password);
            $user->save();
            $passwordReset->delete();
            $user->notify(new PasswordResetSuccess($passwordReset));
            return $this->sendResponse($user, '');
        } catch (\Exception $e) {
            return $this->sendError(null, $e->getMessage(), 500);
        }

    }
}