<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuthController extends Controller
{

    use ApiResponser;

    /**
     * Create user
     *
     * @param  [string] phone
     * @param  [string] password
     * @return [string] message
     */
    private $pac = "Modabu Personal Access Client";

    public function register(Request $request)
    {
        $this->validateSignup($request);
        $user = new User([
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $user->save();
        $tokenResult = $user->createToken($this->pac);
        $success['access_token'] = $tokenResult->accessToken;
        $success['token_type'] = 'Bearer';
        $success['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
        return $this->sendResponse($success, 'User successfully created!', 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] phone
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->sendError('Unauthorized', ['Crendentials do not match'], 401);
        }

        $user = $request->user();

        /* $request->request->add([
        "grant_type" => "password",
        "client_id" => "1", //Get from oauth_clients table
        "client_secret" => "JZ9dHwKcUVke6yrKmJ37rPaHZVDZfjmVCb0RP9dA", //Get from oauth_clients table
        "username" => $request->email,
        "password" => $request->password,
        ]);

        $tokenRequest = $request->create(
        route('passport.token'), //'YOUR_APP_URL/oauth/token'
        'post'
        );

        $instance = Route::dispatch($tokenRequest);

        return ($instance->getContent()); */

        $tokenResult = $user->createToken($this->pac);
        if ($request->remember_me) {
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
        }

        $success['access_token'] = $tokenResult->accessToken;
        $success['token_type'] = 'Bearer';
        $success['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
        return $this->sendResponse($success, 'User login successfully.');
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse(null, 'Successfully logged out');

    }

    public function validateLogin($request)
    {
        return $request->validate([
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255',
            'remember_me' => 'boolean',
        ]);
    }

    public function validateSignup($request)
    {
        return $request->validate([
            'email' => 'required|string|max:255|unique:users,email',
            'password' => ['required', 'string', 'min:5'], //'regex:/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/'
            'cnd' => 'required',
        ]);
    }
}
