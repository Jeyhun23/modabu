<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use ApiResponser;

    function list(Request $request) {
        $brands = Brand::where(['published' => true])->get();
        return $this->sendResponse($brands);
    }

    public function show(string $slug)
    {
        $product = '';
        return $this->sendResponse($product, '');
    }
}