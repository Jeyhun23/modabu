<?php

namespace App\Http\Controllers;

use App\Http\Resources\ColorCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductDetailResource;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\Variation;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ApiResponser;

    function list(Request $request)
    {
        //return $request->price;

        $products = Product::with(['colors', 'attributes', 'brand', 'images'])->where(['confirmed' => true])->orderByDesc('created_at');

        if ($request->get('categories')) {
            $cts = $request->get('categories');
            $new = Category::whereIn('parent_id', $cts)->get()->pluck('id')->toArray();
            $cts = array_merge($cts, $new);
            $products->whereIn('category_id', (array) $cts);
        }

        if ($request->get('brands')) {
            $products->whereIn('brand_id', (array) $request->get('brands'));
        }

        if ($request->get('colors')) {
            $products->whereHas('colors', function ($query) use ($request) {
                $query->whereIn('color_id', (array) $request->get('colors'));
            });
        }

        if ($request->get('price')) {
            $price = explode('-', $request->get('price'));
            $minPrice = min($price) ?: 0;
            $maxPrice = max($price) ?: 1000;
            $products->where(function ($query) use ($minPrice, $maxPrice) {
                //$query->whereHas('attributes.variations', function ($query) use ($minPrice, $maxPrice) {
                //    $query->where('price', '>=', $minPrice)
                //        ->where('price', '<=', $maxPrice);
                //})->orWhereDoesntHave('attributes.variations', function ($query) use ($minPrice, $maxPrice) {
                //    $query->where('price', '>=', $minPrice)
                //        ->where('price', '<=', $maxPrice);
                //});
                $query->where('price', '>=', $minPrice)
                    ->where('price', '<=', $maxPrice);
            });
        }

        $products->when($request->get('discount'), function ($query) use ($request) {
            $query->where(function ($query) {
                $query->where('discount', '!=', 0)->whereNotNull('discount');
            });
        });
        
        if ($request->get('sizes')) {
            $products->whereHas('attributes', function ($query) {
                $query->where('attribute_id', 1);
            })->whereHas('attributes.variations', function ($query) use ($request) {
                //$sizes = explode(',', $request->get('sizes'));
                $query->whereIn('name', (array) $request->get('sizes'));
            });
        }

        if ($request->get('textQuery')) {
            $products->where(function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->get('textQuery')}%");
                $query->orWhere('description', 'like', "%{$request->get('textQuery')}%");
            });
        }

        $products = $products->paginate(6);//->cacheFor(now()->addWeek(1))->cachePrefix('products_')->paginate(6);

        /*  $attributes = $products->flatMap(function ($item) {
        return $item->attributes;
        })->groupBy('id');
        $attributes = $attributes->map(function ($item) {
        return [
        'id' => $item[0]->id,
        'name' => $item[0]->attribute->name,
        'values' => $item[0]->variations->pluck(['name']),
        ];
        });

        $filters = array();
        $attributes = (array) $attributes->first;
        $attributes = array_shift($attributes);
        foreach ($attributes as $key => $item) {
        $name = $item['name'];
        $ss = (array) $item['values'];
        $values = array_shift($ss);
        if (array_key_exists($name, $filters)) {
        foreach ($values as $val) {
        if (in_array($val, (array) $filters[$name], true)) {
        //
        } else {
        array_push($filters[$name], $val);
        }
        }
        } else {
        $filters[$name] = ($values);
        }
        }
        return $filters; */

        return new ProductCollection($products);
    }

    public function show(string $slug)
    {
        $product = Product:: //cacheFor(now()->addDay(1))
            //->cachePrefix('product_')
            where(['slug' => $slug, 'confirmed' => true])
            ->with(['brand', 'category.products.brand'])
            ->firstOrFail();
        return new ProductDetailResource($product);
    }

    public function colors()
    {
        return new ColorCollection(Color::all());
    }

    public function getVariations(Request $request)
    {
        //return $request->all();
        $request->validate([
            'product_id' => ['required', 'integer', 'exists:products,id'],
            'color_id' => ['required', 'integer', 'exists:colors,id'],
        ]);
        $product = $request->get('product_id');
        $color = $request->get('color_id');
        $parent = $request->get('parent');
        $sku = $color . 'xx1xx';
        if ($parent) {
            $sku .= $parent . 'xx';
        }
        return Variation::where([
            'product_id' => $product,
            'color_id' => $color,
        ])->get()->transform(function ($v) {
            return [
                "variation" => (string) $v->variation_parent,
                "sub_variation" => (string) $v->variation_child,
                "price" => (float) $v->price,
                "qty" => (int) $v->qty,
            ];
        });
    }
}
