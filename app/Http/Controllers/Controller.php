<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\AtlSms;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected AtlSms $smsService;

    public function __construct()
    {
        $this->smsService = new AtlSms();

        $this->middleware(function ($request, $next) {
            return $next($request);
        });
    }

    protected function preprocessNumber($phone): string
    {
        $phoneNumeric = str_replace(['-', '+', ' '], '', $phone);
        $phoneCleaned = Str::substr($phoneNumeric, -9);
        return "994{$phoneCleaned}";
    }

    public function getProductDiscount(Product $product, $qty = 1)
    {
        $discount = 0;
        if ((float) $product->discount != 0) {
            if ($product->discount_type == 1) {
                $discount = $product->discount;
            } else {
                $discount = $product->price * $qty * $product->discount / 100;
            }
        }
        return $discount;
    }
}
