<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'mimes:jpeg,bmp,png,svg,gif,jpg'],
            'url' => 'required',
            'position' => 'required|integer|in:1,2',
            'rank' => 'nullable|integer|gte:0',
        ];
    }
}