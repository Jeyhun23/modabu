<?php

namespace App\Http\Requests\V2;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class BrandRequest extends FormRequest
{
    private $requiredOrNot;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->merge([
            'published' => $this->published == 'on'? 1 : 0,
            'slug' => Str::slug($this->name)
        ]);
        $this->requiredOrNot = $this->method() == 'PUT' ? 'sometimes|nullable' : 'required';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => $this->requiredOrNot.'|mimes:jpeg,bmp,png,svg,gif,jpg',
            'name' => 'required|max:191'
        ];
    }
}
