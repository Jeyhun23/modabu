<?php

namespace App\Http\Requests\V2;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    private $requiredOrNot;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->merge([
            'phone' => !is_null($this->phone) ? $this->phone_prefix . $this->phone : null,
            'name' => $this->name . ' ' . $this->surname
        ]);
        $this->requiredOrNot = $this->method() == 'PUT' ? 'sometimes|nullable' : 'required';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'email' => $this->requiredOrNot . '|email|unique:users,email,' . $this->uid,
            'phone' => $this->requiredOrNot . '|numeric|unique:users,phone',
            'password' => $this->requiredOrNot . '|min:6|max:191'
        ];
    }
}
