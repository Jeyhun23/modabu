<?php

namespace App\Http\Requests\V2;

use Illuminate\Foundation\Http\FormRequest;

class ReflerRequest extends FormRequest
{
    private $uniqueOrNot;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->uniqueOrNot = $this->method() == 'PUT' ? 'required|unique:reflers,name,' . $this->id : 'required|unique:reflers,name';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => $this->uniqueOrNot,
            'barcode' => 'nullable'
        ];
    }
}
