<?php

namespace App\Http\Requests\V2;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class AdminPanelUserRequest extends FormRequest
{
    private $requiredOrNot;
    private $phoner;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->merge([
            'phone' => !is_null($this->phone) ? $this->phone_prefix . $this->phone : null
        ]);
        $this->phoner = $this->phone ? 'required_if:phone,!=,null' : 'nullable';
//        $this->phone=!is_null($this->phone) ? $this->phone_prefix.$this->phone : null
        $this->requiredOrNot = $this->method() == 'PUT' ? 'nullable' : 'required';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'phone_prefix' => $this->phoner,
            'phone' => $this->requiredOrNot . '|numeric|unique:users,phone',
            'roles' => $this->requiredOrNot,
            'password' => $this->requiredOrNot . '|min:6|max:191'
        ];
    }

    public function withValidator($validator)
    {
        if ($this->roles) {
            $validator->after(function ($validator) {
                $role = Role::where('id', $this->roles)->first();
                if (!$role) {
                    $validator->errors()->add('role', __('Bele bir Role yoxdur'));
                }
            });
        }
        return;
    }
}
