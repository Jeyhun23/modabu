<?php

namespace App\Http\Requests\V2;

use Illuminate\Foundation\Http\FormRequest;

class StoryRequest extends FormRequest
{
    private $requiredOrNot;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->merge([
            'published' => $this->published == 'on' ? 1 : 0,
        ]);
        $this->requiredOrNot = $this->method() == 'PUT' ? 'sometimes|nullable' : 'required';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => $this->requiredOrNot . '|mimes:jpeg,bmp,png,svg,gif,jpg|max:2048',
            'title' => 'required|max:191',
            'rank' => 'required',
            'url' => 'required'
        ];
    }
}
