<?php

namespace App\Http\Requests\V2;

use App\Models\Coupon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CouponsRequest extends FormRequest
{
    private $uniqueOrNot;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->uniqueOrNot = $this->method() == 'PUT' ? 'unique:coupons,code,' . $this->cid : 'unique:coupons';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
            'code' => [
                'required',
                $this->uniqueOrNot
            ],
            'discount' => [
                'numeric',
                'required',
                'min:1',
                'max:100000',
            ],
            'discount_type' => [
                'required',
            ],
            'coupon_type' => [
                'required',
                Rule::in(array_keys(Coupon::COUPON_TYPE_SELECT))
            ],
            'expire_date' => [
                'nullable',
                'sometimes',
                'date',
                'after:today'
            ],
            'count' => [
                'nullable',
                'sometimes',
                'integer',
                'min:1'
            ]
        ];
    }
}
