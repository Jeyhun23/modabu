<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserUpdateSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => ['required'],
            'lastname' => ['required'],
            'email' => ['required',
                Rule::unique('users')->ignore($this->user()),
            ],
            'current_password' => ['nullable'],
            'password' => ['nullable', 'string', 'min:6'],
            'password_confirmation' => ['nullable', 'required_with:password', 'same:password'],
        ];
    }

    public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        $validator->after(function ($validator) {
            if ($this->password != '' && !Hash::check($this->current_password, $this->user()->password)) {
                $validator->errors()->add('current_password', __('Your current password is incorrect.'));
            }
        });
        return;
    }
}