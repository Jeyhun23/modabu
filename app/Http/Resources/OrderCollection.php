<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'current_page' => $resource->currentPage(),
            'last_page' => $resource->lastPage(),
            'total' => $resource->total(),
            'per_page' => $resource->perPage(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $this->collection->transform(function ($order) {
            //return (new OrderResource($order));
            return [
                'id' => $order->id,
                'orderno' => $order->orderno,
                'total' => $order->total,
                'discount' => $order->discount,
                'payed' => $order->payed,
                'status' => $order->parentStatus,
                'detail' => new OrderDetailCollection($order->detail),
                'createdAt' => $order->created_at->format('d.m.Y, H:i'),
            ];
        });
        return [
            'data' => $this->collection,
            'pagination' => $this->pagination,
        ];
    }
}
