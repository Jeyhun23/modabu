<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderFormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'id' => null,
                'name' => $this->first_name . ' ' . $this->last_name,
                'phone' => $this->phone,
                'email' => $this->email,
                'city' => $this->city,
                'address' => $this->address,
                'email_verified_at' => null,
                'created_at' => null,
                'updated_at' => null,
            ],
            'orderno' => $this->orderno,
            'orderDate' => $this->created_at->format("d/m/Y H:i"),
            'total' => $this->total,
            'discount' => $this->discount,
            'payed' => $this->payed,
            'productCount' => 1,
            'detail' => [
                [
                    'id' => $this->id,
                    'productId' => $this->product_id,
                    'productSlug' => $this->product ? $this->product->slug : '',
                    'price' => $this->total,
                    'discount' => $this->discount,
                    'quantity' => $this->quantity ?: 1,
                    'attributes' => $this->attributes,
                    'status' => $this->parentStatus,
                    'stock' => $this->stock,
                    'type' => 'form',
                ],
            ],
            'type' => 'form',
        ];
    }
}