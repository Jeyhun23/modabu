<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category->name,
            'brand_id' => $this->brand_id,
            'title' => truncate($this->title, 70, true),
            'thumbnail' => $this->thumbnail,
            'description' => truncate($this->title, 70, true),
            'slug' => $this->slug,
            'price' => $this->price . '₼',
            'qty' => $this->qty,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'featured' => $this->featured,
            'confirmed' => $this->confirmed,
            'created_at' => $this->created_at->format('d/m/Y'),
        ];
    }
}