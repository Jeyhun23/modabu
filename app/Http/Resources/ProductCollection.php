<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    private $pagination;

    public $collects = Product::class;

    public function __construct($resource)
    {
        $this->pagination = [
            'current_page' => $resource->currentPage(),
            'last_page' => $resource->lastPage(),
            'total' => $resource->total(),
            'per_page' => $resource->perPage(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $this->collection->transform(function (Product $product) {
            return (new ProductResource($product));
        });
        return [
            'data' => $this->collection,
            'pagination' => $this->pagination,
        ];
    }
}