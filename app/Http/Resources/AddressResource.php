<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'fullName' => $this->fullname,
            'city' => $this->city,
            'address' => $this->address,
            'phone' => $this->phone,
            'isDefault' => $this->is_default,
            'createdAt' => $this->created_at->format('d/m/Y'),
        ];
    }
}