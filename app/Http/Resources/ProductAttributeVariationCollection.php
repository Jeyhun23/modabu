<?php

namespace App\Http\Resources;

use App\Models\ProductAttributeVariation;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductAttributeVariationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($attrV) {
            //return (new ProductAttributeVariationResource($attrV));
            return [
                'id' => $attrV->id,
                'productAttributeId' => $attrV->product_attribute_id,
                'name' => (string) $attrV->name,
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
