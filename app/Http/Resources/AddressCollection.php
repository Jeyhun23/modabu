<?php

namespace App\Http\Resources;

use App\Models\Address;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AddressCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function __construct($resource)
    {
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        //collect($this->collection)->map(function (Address $adrs) {
        return $this->collection->transform(function ($adrs) {
            //return (new AddressResource($adrs));
            return [
                'id' => $adrs->id,
                'title' => $adrs->title,
                'fullName' => $adrs->fullname,
                'city' => $adrs->city,
                'address' => $adrs->address,
                'phone' => $adrs->phone,
                'isDefault' => $adrs->is_default,
                'createdAt' => $adrs->created_at->format('d/m/Y'),
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
