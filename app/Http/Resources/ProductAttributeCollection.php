<?php

namespace App\Http\Resources;

use App\Models\ProductAttribute;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductAttributeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return $this->collection->transform(function ($attr) {
            //return (new ProductAttributeResource($attr));
            return [
                'id' => $attr->id,
                'name' => $attr->attribute ? $attr->attribute->name : '',
                'variations' => new ProductAttributeVariationCollection($attr->variations),
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
