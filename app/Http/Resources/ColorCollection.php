<?php

namespace App\Http\Resources;

use App\Models\Color;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ColorCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($color) {
            //return (new ColorResource($color));
            return [
                'id' => $color->id,
                'name' => $color->name,
                'code' => $color->code,
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
