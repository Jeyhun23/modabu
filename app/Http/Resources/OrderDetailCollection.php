<?php

namespace App\Http\Resources;

use App\Models\OrderDetail;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderDetailCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($order) {
            //return (new OrderDetailResource($order));
            return [
                'id' => $order->id,
                'orderno' => $order->orderno,
                'productId' => $order->product_id,
                'productSlug' => $order->product ? $order->product->slug : '',
                'productTitle' => $order->product ? $order->product->title : '',
                'productBrand' => $order->product ? $order->product->brand->title : '',
                'productThumbnail' => $order->product ? $order->product->thumbnail : '',
                'price' => $order->price,
                'discount' => $order->discount,
                'quantity' => $order->quantity,
                'attributes' => $order->attributes,
                'stock' => $order->stock,
                'status' => $order->parentStatus,
                'type' => 'cart',
            ];
        })->toarray();
        return $this->collection;
    }
}
