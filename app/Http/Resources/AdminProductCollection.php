<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AdminProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'page' => $resource->currentPage(),
            'pages' => $resource->lastPage(),
            'perpage' => $resource->perPage(),
            'total' => $resource->total(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $this->collection->transform(function ($product) {
            //return (new AdminProductResource($product));
            return $this->utf8ize([
                'id' => $product->id,
                'category' => $product->category?strip_tags($product->category->name):'',
                'brand_id' => $product->brand_id,
                'title' => truncate(strip_tags($product->title), 70, true),
                'thumbnail' => $product->thumbnail,
                'description' => truncate(strip_tags($product->description), 70, true),
                'slug' => strip_tags($product->slug),
                'price' => $product->price . '₼',
                'qty' => $product->qty,
                'discount' => $product->discount,
                'discount_type' => $product->discount_type,
                'featured' => $product->featured,
                'confirmed' => (int) $product->confirmed,
                'created_at' => $product->created_at->format('d/m/Y'),
            ]);
        })->toArray();
        return [
            'data' => $this->collection,
            'meta' => $this->pagination,
        ];
    }

    function utf8ize( $mixed ) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = $this->utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }
}
