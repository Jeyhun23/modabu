<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'categoryId' => $this->category_id,
            'brand' => new BrandResource($this->brand),
            'title' => $this->title,
            'thumbnail' => $this->thumbnail, //str_replace('thumbnail', '264x362', str_replace('compressed/', '', $this->thumbnail)),
            'slug' => $this->slug,
            'price' => $this->price,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'featured' => $this->featured,
            'isInCombine' => false,
            'hasMultipleColor' => (bool) $this->leftCombines->count() || $this->rightCombines->count(),
            //'description' => $this->description,
            //'meta' => json_decode($this->meta),
            //'qty' => $this->qty,
            //'colors' => $this->colors,
            //'images' => $this->images,
            //'attributes' => new ProductAttributeCollection($this->attributes),
        ];
    }
}
