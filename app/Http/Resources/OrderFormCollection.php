<?php

namespace App\Http\Resources;

use App\Models\OrderForm;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderFormCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'field' => "ID",
            'page' => $resource->currentPage(),
            'pages' => $resource->lastPage(),
            'perpage' => $resource->perPage(),
            'sort' => "asc",
            'total' => $resource->total(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $this->collection->transform(function ($order) {
            //return (new OrderFormResource($order));
            return [
                'id' => $order->id,
                'user' => [
                    'id' => null,
                    'name' => $order->first_name . ' ' . $order->last_name,
                    'phone' => $order->phone,
                    'email' => $order->email,
                    'city' => $order->city,
                    'address' => $order->address,
                    'email_verified_at' => null,
                    'created_at' => null,
                    'updated_at' => null,
                ],
                'orderno' => $order->orderno,
                'orderDate' => $order->created_at->format("d/m/Y H:i"),
                'total' => $order->total,
                'discount' => $order->discount,
                'payed' => $order->payed,
                'productCount' => 1,
                'detail' => [
                    [
                        'id' => $order->id,
                        'productId' => $order->product_id,
                        'productSlug' => $order->product ? $order->product->slug : '',
                        'price' => $order->total,
                        'discount' => $order->discount,
                        'quantity' => $order->quantity ?: 1,
                        'attributes' => $order->attributes,
                        'status' => $order->parentStatus,
                        'stock' => $order->stock,
                        'type' => 'form',
                    ],
                ],
                'type' => 'form',
            ];
        });
        return [
            'data' => $this->collection,
            'meta' => $this->pagination,
        ];
    }
}
