<?php

namespace App\Http\Resources;

use App\Models\Wishlist;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WishlistCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($wishlist) {
            //return (new WishlistResource($wishlist));
            return [
                'id' => $wishlist->id,
                'product' => new ProductResource($wishlist->product),
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
