<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductRelatedCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($product) {
            //return (new ProductRelatedResource($product));
            return [
                'id' => $product->id,
                'brand' => new BrandResource($product->brand),
                'title' => $product->title,
                'thumbnail' => str_replace('thumbnail', '264x362', str_replace('compressed/', '', $product->thumbnail)),
                'slug' => $product->slug,
                'price' => $product->price,
                //'qty' => $product->qty,
                'discount' => $product->discount,
                'discount_type' => $product->discount_type,
                'featured' => $product->featured,
            ];
        })->toArray();
        return parent::toArray($request);
    }
}
