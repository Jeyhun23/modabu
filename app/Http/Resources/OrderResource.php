<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'orderno' => $this->orderno,
            'total' => $this->total,
            'discount' => $this->discount,
            'payed' => $this->payed,
            'status' => $this->parentStatus,
            'isUrgent' => (bool) $this->is_urgent,
            'paymentType' => (bool) $this->payment_type,
            'detail' => new OrderDetailCollection($this->detail),
            'address' => $this->address,
            'createdAt' => $this->created_at->format('d.m.Y, H:i'),
        ];
    }
}
