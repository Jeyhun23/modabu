<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'fullname' => $this->address['fullname'],
            'orderno' => $this->orderno,
            'orderDate' => $this->created_at->format('d/m/Y H:i'),
            'total' => (float) $this->total,
            'discount' => (float) $this->discount,
            'payed' => (float) $this->payed,
            'productCount' => $this->productCount,
            'detail' => new OrderDetailCollection($this->detail),
            'type' => 'order',
        ];
    }
}