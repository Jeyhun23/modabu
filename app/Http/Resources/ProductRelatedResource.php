<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductRelatedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'brand' => new BrandResource($this->brand),
            'title' => $this->title,
            'thumbnail' => $this->thumbnail,
            'slug' => $this->slug,
            'price' => $this->price,
            'qty' => $this->qty,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'featured' => $this->featured,
        ];
    }
}