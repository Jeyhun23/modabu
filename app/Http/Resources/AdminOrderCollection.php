<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AdminOrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'page' => $resource->currentPage(),
            'pages' => $resource->lastPage(),
            'perpage' => $resource->perPage(),
            'total' => $resource->total(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $this->collection->transform(function ($order) {
            //return (new AdminOrderResource($order));
            return [
                'id' => $order->id,
                'user' => $order->user,
                'fullname' => $order->address['fullname'],
                'orderno' => $order->orderno,
                'orderDate' => $order->created_at->format('d/m/Y H:i'),
                'total' => (float) $order->total,
                'discount' => (float) $order->discount,
                'payed' => (float) $order->payed,
                'productCount' => $order->productCount,
                'detail' => new OrderDetailCollection($order->detail),
                'type' => 'order',
            ];
        });
        return [
            'data' => $this->collection,
            'meta' => $this->pagination,
        ];
    }
}
