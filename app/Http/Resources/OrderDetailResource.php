<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'orderno' => $this->orderno,
            'productId' => $this->product_id,
            'productSlug' => $this->product ? $this->product->slug : '',
            'productTitle' => $this->product ? $this->product->title : '',
            'productBrand' => $this->product ? $this->product->brand->title : '',
            'productThumbnail' => $this->product ? $this->product->thumbnail : '',
            'price' => $this->price,
            'discount' => $this->discount,
            'quantity' => $this->quantity,
            'attributes' => $this->attributes,
            'stock' => $this->stock,
            'status' => $this->parentStatus,
            'type' => 'cart',
        ];
    }
}