<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //$images = $this->images->each(function($img){
        //    return $img->path = str_replace("products/", "products/373x560/", str_replace("products/thumbnail", "products/", $img->path));
        //});
        return [
            'id' => $this->id,
            'categoryId' => $this->category_id,
            'brand' => new BrandResource($this->brand),
            'title' => $this->title,
            'thumbnail' => $this->thumbnail,
            'description' => $this->description,
            'slug' => $this->slug,
            'meta' => json_decode($this->meta),
            'price' => $this->price,
            'qty' => $this->qty,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'featured' => $this->featured,
            'colors' => $this->colors,
            'images' => $this->images,
            'attributes' => new ProductAttributeCollection($this->attributes),
            'related' => new ProductRelatedCollection($this->category->products),
            'combines' => $this->combines(),
        ];
    }
}
