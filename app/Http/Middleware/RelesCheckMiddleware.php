<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RelesCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if (in_array(auth()->user()->roles[0]->id, explode('|', $role))) {
            return $next($request);
        }
        abort(404);
    }
}
