<?php

namespace App\Services;

use App\Models\Sms;
use App\Models\User;
use App\Services\IServices\ISmsService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\Types\Null_;
use SebastianBergmann\Type\NullType;
use SimpleXMLElement;

class AtlSms implements ISmsService
{
    /**
     * @param string|null $phone
     * @param string $message
     * @return Model|Builder|null
     */
    public function send(string $phone, string $message)
    {
        if ($phone == null)
            return null;

        $sms = Sms::query()->create([
            'phone' => $phone,
            'message' => $message
        ]);

        $xml = file_get_contents(storage_path('app/public/xml/atl.xml'));
        $xmlObject = new SimpleXMLElement($xml);
        $xmlObject->head->login = Sms::ATL_USER;
        $xmlObject->head->password = Sms::ATL_PASS;
        $xmlObject->head->title = Sms::ATL_TITLE;
        $xmlObject->head->controlid = str_pad($sms->getAttribute('id'), 6, 0, STR_PAD_LEFT);
        $xmlObject->body->msisdn = $phone;
        $xmlObject->body->message = $message;

        $response = Http::withHeaders([
            'Content-Type' => 'text/xml; charset=UTF8'
        ])->withBody($xmlObject->asXML(), 'text/xml; charset=UTF8')
            ->post(Sms::ATL_URL)->body();
        $sms->setAttribute('response', $response);
        $sms->save();

        return $sms;
    }

    /**
     * @param User $user
     * @param $text
     * @return Builder|Model
     */
    public function sendSmsToUser(User $user, $text)
    {
        $sms = $this->send($user->getAttribute('phone'), $text);
        $sms->setAttribute('user_id', $user->getAttribute('id'));
        $sms->save();
        return $sms;
    }
}
