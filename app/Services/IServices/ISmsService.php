<?php


namespace App\Services\IServices;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\Type\NullType;

interface ISmsService
{
    public function send(string $phone, string $message);
}
